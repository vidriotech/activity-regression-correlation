.. _Configuration:
 
Configuration
=============

The :option:`falcor config-file` follows lua_ 5.3 syntax, and is actually
a lua script.  This means you can use comments and much more; lua is a
full-fledged programing language.  However, most of the time the config file
will look very :ref:`simple <example-config>`.

.. _lua: http://www.lua.org/manual/5.3/

When :program:`falcor` loads the file, it will
run the configuration file through the Lua evaluator [#LuaEval]_, check
that the appropriate values are set, and perform some
validation.  When syntax and validation errors, an error message
will be produced to help you find the problem.

.. [#LuaEval] You can play with lua using an `online interpreter <https://www.lua.org/cgi-bin/demo>`_

Version
-------

Each :option:`falcor config-file` should identify the version of the configuration file format.
This will be used for backwards compatibility if the configuration format
evolves over time.  Currently, there's only one supported configuration format
and :program:`falcor` does not check this value, but in the future that might
change.

Just set:

.. code-block:: lua

    version=0

.. _config-input:


Input
-----

.. table:: Summary of input parameters

    ========== =================================================================
    Parameter  Description
    ========== =================================================================
    timepoint  A filename pattern used to locate the data that makes up the
               time-series.
    first      Integer. The first timepoint in the time series.
    last       Integer. The last timepoint in the time series.
    chunk_size Integer. Optional .The number of timepoints to process in the GPU
               at once. If you're running out of GPU memory, reduce this number.
               If not specified, the chunk size will be set to the length
               of the full timeseries.
    stimulus   A filename.  The file should be a text file containing
               integers on seperate lines.  Each integer represents the frame
               where a stimulus occured.
    ========== =================================================================

Together, the ``timepoint``, ``first`` and ``last`` fields are used to load the
sequence of stacks that make up the timeseries.  To load a particular timepoint,
an integer is subsituted into the timepoint's filename pattern up to four times.
The filename pattern that uses printf-style_ formating.

For example:

.. code-block:: lua

    input={
        timepoint="E:/Data/%d/sweet_sweet_data_00003_%05d.tif",
        first=1,
        last=4,
    }

Would read:

.. code-block:: powershell

    E:/Data/1/sweet_sweet_data_00003_00001.tif
    E:/Data/2/sweet_sweet_data_00003_00002.tif
    E:/Data/3/sweet_sweet_data_00003_00003.tif
    E:/Data/4/sweet_sweet_data_00003_00004.tif

Loading a 4D stack
..................

If the timeseries is to be loaded from a single file, set ``options.use_single_file`` to ``true``.
Use ``input.first`` and ``input.last`` to specify the number of timepoints in the data set.

Alternatively to load the whole timeseries from one file:

.. code-block:: lua

    input={
        timepoint="E:/Data/all_the_sweet_sweet_data_00003.tif",
        first=1,
        last=4,
    }
    options={
        use_single_file=true
        ...
    }

This loads data from the indicated file.  The tiff format is limited to describing 3D
data (as a sequence of images).  To load a timeseries of 3D data from a single file
we have to know how to divide the loaded images between z and time.  The loader
assumes the sequence looks like:

.. math::

  (z_0,t_0), (z_1,t_0), (z_2,t_0), ..., (z_n,t_0), (z_0,t_1), ...

To assign a particular `(z,t)` to an image in the sequence, the loader needs to know
the number of timepoints to expect.  If `T` is the number of timepoints and `N` is 
the number of images in the sequence, than the number of z planes is `N/T`. `T` is 
calculated using the ``input.first`` and ``input.last`` fields:

.. math::

    T = last - first + 1

So using the example above, :math:`T=4-1+1=4`.  If the loaded file has 400 images in it,
it will load as a sequence of 4 volumes each with 100 planes.

.. _printf-style: https://en.wikipedia.org/wiki/Printf_format_string#1970s:_C.2C_Lisp

.. note:: Path seperators in filenames

    On Windows, The ``/`` or ``\\`` symbol can be used to seperate folder names
    when specifying a filename. Using a single ``\`` symbol by itself can
    prevent proper parsing of the configuration file.  If you'd like to be able
    to copy-and-paste paths into your configuration file on windows then enclose
    the paths in ``[[`` and ``]]`` instead of quotes; these will prevent the
    parser from treating ``\`` as a special character.  For example:

    .. code-block:: lua

        -- problem ;_;
        timepoint="C:\Data\timeseries\file_%d.tif"

        -- works just fine!
        timepoint="C:\\Data\\timeseries\\file_%d.tif"
        timepoint="C:/Data/timeseries/file_%d.tif"
        timepoint=[[C:\Data\timeseries\file_%d.tif]]

    For more details, refer to this overview of
    `string handling in lua <https://www.lua.org/pil/2.4.html>`_.

.. _config-output:

Output
------

.. table:: Summary of output parameters

    ============================== ==================================================================================================================
    Parameter                      Description
    ============================== ==================================================================================================================
    folder                         The path to the folder where all results will be stored.  All
                                   output files will be stored in this folder as well as a copy of
                                   the configuration file.
    correlation                    Filename.  :math:`R^2` results will be stored in this (Big)Tiff [#f1]_
                                   file.  Don't specify a path.
                                   It will be stored in the results folder.
    log                            Filename. Any generated status or error messages in this file.
    stimdt                         Filename. A raw binary file containing signed 32-bit integers,
                                   one for each timepoint.  This represents the raw input to the
                                   correlation.  Use this to verify that stimulus times were
                                   extracted properly.
    projection                     Filename. Optional. If specified, a motion-corrected
                                   z-projection of the timeseries will be saved to this file in the
                                   output folder.  No median filter is applied.
                                   The output is a Tiff or BigTiff depending on the file size.
    mean                           Filename. Optional. If specified, a motion-corrected mean
                                   volume will be saved to this file in the output folder.
                                   No median filter is applied.
                                   The output is a Tiff or BigTiff depending on the file size.
    ============================== ==================================================================================================================

.. [#f1] BigTiff_ and Tiff_ are distinct image/volume data formats, but most
         applications with BigTiff support also support Tiff.  The Tiff format
         is limited to 4GB file sizes.  BigTiff fixes that.  :program:`falcor`
         outputs Tiff files if the data is small enough, otherwise BigTiff.

.. _BigTiff: http://bigtiff.org/
.. _Tiff: https://en.wikipedia.org/wiki/Tagged_Image_File_Format

.. _config-params:

Parameters
----------

.. table:: Summary of parameters

    ============================== ==================================================================================================================
    Parameter                      Description
    ============================== ==================================================================================================================
    window                         Integer. A regression to estimate the stimulus response is
                                   performed over a window of time points.  The algorithm
                                   assumes the stimulus response has decayed back to the
                                   baseline after *window* timepoints, and therefore
                                   different stimulation events should be spaced by more than
                                   *window*; the responses should not overlap.
    stimulus_threshold             Stimulus times can be potentially be extracted from
                                   image data.  If the average intensity of the last plane
                                   in an image stack for a timepoint is above this
                                   threshold, this is taken as an indication of a stimulation
                                   event.
            
                                   Only required if the `extract_stim_times_from_images`
                                   option is `true`.
    median_radius                  Integer. Radius in pixels of the box-window to use for
                                   2D median filtering.  If set to 0, the median filter
                                   will be disabled.  If the radius is :math:`r`, the
                                   width and height of the box used for the filter is
                                   :math:`2r+1`.
    gpuid                          Integer. Optional. Specifies the id of the GPU to use.
                                   Normally devices are numbered 0,1,... By default device
                                   0 is chosen.
    ============================== ==================================================================================================================

.. _config-options:

Options
-------

.. table:: Summary of options

    ============================== ==================================================================================================================
    Option                         Description
    ============================== ==================================================================================================================
    extract_stim_times_from_images true or false.  Use crude image analysis to
                                   identify stimulation events.
    write_median                   true or false. Write a file with the data after
                                   median filtering.
    write_stimdt                   true or false.  Write the stimulation
                                   :math:`\Delta t` vector to a raw binary file as
                                   a stream of 32-bit signed integers.
                                   Stored in the output folder.
    write_motion_vectors           true or false.  Write a raw binary file with
                                   the estimated motion vectors.  The file will
                                   have a stream of 32-bit floats, 3 for each
                                   timepoint.  The 3 values for a time point are
                                   :math:`\Delta x`, :math:`\Delta y`, :math:`\Delta z`
                                   in that order.
    write_kernel_estimate          true or false.  Optional.  Default false.  If
                                   true, the estimated response function at each
                                   voxel will be written to as a file `K.tif` in the
                                   output directory.
    disable_motion_correction      true or false.  Optional.  Default false. If true,
                                   the motion estimation and resampling step will be
                                   skipped.
    use_single_file                true or false. Optional.  Default false. If true,
                                   attempt to load the timeseries from a single file.
    ============================== ==================================================================================================================

Examples
--------

Full configuration file
.......................

.. _example-config:

.. code-block:: lua
    :linenos:

    -- a comment!
    version=0
    input={
        timepoint="C:/Data/timeseries_00003_%05d.tif",
        stimulus="stim-times.txt",
        first=1,
        last=350,
        chunk_size=100,
    }
    output={
        folder=[[C:\Results\run6\"]],
        correlation="r2.tif",
        log="log.txt",
    	stimdt='stimdt.i32'
    }
    parameters={
        window=12,
    	stimulus_threshold=6,
        median_radius=2,
        gpuid=0
    }
    options={
    	extract_stim_times_from_images=false,
        write_kernel_estimate=true,
    	write_stimdt=true,
    	write_motion_vectors=true,
    }


Set the output folder to a unique value for every run
.....................................................

.. code-block:: lua
    :linenos:

    ...
    output={
        -- will become something like "C:/Results/run-1466717632"
        folder="C:/Results/run-" .. os.time(),
        ...
    }
    print(output.folder) -- will print the output folder to
     stdout.
    ...

.. _config-args:

Pass command line arguments to the configuration script
.......................................................

Calling falcor with additional arguments will pass those to the config file.

For example:

.. code-block:: none
    :linenos:

    falcor config.lua hi there everyone

Provides three extra arguments: `hi`, `there` and `everyone`.

To access these in the config script, you'd write:

.. code-block:: lua
    :linenos:

    -- add these lines to your config.lua file somewhere
    local a,b,c=... -- the ... is special: it gives you access to the arguments
    print(a)        -- the "local" keyword is used here, but it's not necessary
    print(b)
    print(c)        -- use the lua print function to output values

The "..." in the script access the list of arguments.  These are unpacked into
local variables `a`,`b`, and `c`.  The example will print the values to console:

.. code-block:: none
    :linenos:

    hi
    there
    everyone

.. warning::

    When no arguments are passed to the configuration file, it acts as a
    complete record of the input parameters used to generate the results.

    A copy of this file is copied to the results folder so one can easily
    find how a particular result was generated later on.

    No record of any extra arguments is made; only the configuration file is
    copied.  As a result using extra arguments can impact your ability to track
    the provinance of your data.
