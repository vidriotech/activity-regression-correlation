.. Falcor documentation master file, created by
   sphinx-quickstart on Thu Jun 23 13:03:42 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
  :hidden:
  :glob:

  usage
  pipeline
  config
  input/index
  output/index
  glossary

Falcor
======

:program:`Falcor` performs fast local correlations of stimulus-induced activity.

.. todolist::
