Usage
=====

:program:`falcor` runs from the command line.  You control it by manipulating
the values in a :option:`config-file`.  There really isn't a lot to the command
line interface, but there's quite a lot to the :doc:`config`.

.. code-block:: powershell

    falcor <config-file> [<extra-args>]

.. program:: falcor

.. option:: config-file

    A file name. The configuration file is a text file that specifies where
    input files are, where the output should be stored, and various parameters
    and options.

.. option:: extra-args

    Additional arguments that come after the :option:`config-file` are passed
    as string arguments that can be used to alter the configuration.
    See :ref:`here <config-args>` for an example.

Piping
------

Alternative the configuration file can be piped in::

    cat config.lua | falcor

`falcor` will read the configuration file from ``stdin`` and record the contents
of the configuration with the result data.  This should be useful for workflows
that generate configurations programmatically.

In this case additional arguments cannot be supplied to the configuration script.

Getting started
---------------

1. Copy the :ref:`example configuration file <example-config>`.
2. Update the `input` section to point to your data.
3. Update the `output.folder` to point to where results should be stored.
4. Review `parameters` and `options` to make sure they're what you want.
5. Run :program:`falcor`
