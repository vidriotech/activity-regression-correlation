The *stimdt* file
-----------------

.. figure:: _static/stimdt.png
    :width: 350px
    :align: right
    :alt: Plot of the values in a *stimdt* file.

    Plot of the values in a *stimdt* file.

The :ref:`stimdt <config-output>` and :ref:`output folder <config-output>`
determine where this file is written

Structure
`````````

It is a raw binary file containing signed 32-bit integers, one for each
timepoint.  This represents the raw input to the correlation.
It may be useful for verifying that stimulus times were extracted properly.

It's straightforward to load an plot this data using Python_ [#f1]_.

.. code-block:: python

    dt=fromfile('stimdt.i32',dtype=int32)
    plot(dt)

.. [#f1] When I say Python, I actually mean Python plus all the packages usually
         involved in numerical computing: ipython, numpy, scipy, and matplotlib.
         If you install Anaconda_, you'll have all those.

.. _Python: https://www.python.org/
.. _Anaconda: https://www.continuum.io/downloads

Interpretation
``````````````

The values in the file follow a few rules:

1. Before the first stimulus, values are -1.
2. On the frame where a stimulus occurs, values are 0.
3. Otherwise, the value is the time since the last stimulus, :math:`\Delta t`.

Time is always measured in integral units: the number of samples.  In this case
time is sampled by quickly scanning a volume.  Each volume is a :term:`timepoint`,
sometimes also called a :term:`frame`.

This file is useful because it has the values that are used to learn the
:term:`stimulus response`.
