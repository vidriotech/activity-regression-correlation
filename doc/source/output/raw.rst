.. _how-to-read-raw:

Reading raw output files
------------------------

:program:`falcor` often saves data to raw binary files.  Once you know how to read
them it's simple to load them into Matlab or Python and view the data.

For example, the file ``dr.3x350.f32`` indicates the data type and dimensions
of the data in the file name.  Here the file is a table with 3 columns and
350 rows.  Some examples:

Python:

.. code-block:: python

    >>> from numpy import *
    >>> dr=fromfile("dr.3x350.f32",dtype=float32).reshape(350,3)

Matlab:

.. code-block:: matlab

    >> dr=reshape(fread(fopen('dr.3x350.f32','r'),Inf,'single'),[3 350]);
