Output Data
===========

:program:`falcor` generates a number of files with different formats.

.. toctree::

  raw
  stimdt
  volumes
