Storing volume data
-------------------

*Volume* data (data that is represents pixels or voxels) is saved as a Tiff_
or BigTiff_ file.  Both filetypes use the same extension ``.tif``.  BigTiff is
used when the output file size would exceed the 2 GB limit of a normal
Tiff file.  These files can be easily viewed and manipulated using Fiji_.

Baseline Tiff is a 2D image format, but multiple images can be stored in the
same file.  That capability is used to store multidimensional data.  3D data
can be stored by adding each z-plane to the image in sequence.

Higher dimensional data can be stored in the tiff as well, but this gets more
complicated.  The tiff format can be used to remember the width, height, and
number of planes, but the size of higher (>3) dimensions is lost.  The higher
dimensions all get concatenated together.

Fortunately, it's simple to recover the original dimensionality of the data if
you remember the shape.  In Fiji_, after opening the Tiff stack, use the
`stack to hyperstack <hyperstack>`_ tool to reshape the volume.  In Python, you
would use `numpy.array.reshape <pyreshape>`_.  In Matlab, reshape_.

.. figure:: _static/StackToHyperstack.png
    :figwidth: 50%
    :align: right

    Using the stack to hyperstack tool in Fiji_.


.. _BigTiff: http://bigtiff.org/
.. _Tiff: https://en.wikipedia.org/wiki/Tagged_Image_File_Format
.. _Fiji: http://fiji.sc/
.. _hyperstack: https://imagej.nih.gov/ij/docs/guide/146-8.html
.. _pyreshape: http://docs.scipy.org/doc/numpy/reference/generated/numpy.reshape.html
.. _reshape: http://www.mathworks.com/help/matlab/ref/reshape.html
