File series
-----------

:program:`falcor` assumes that input data is organized as a :term:`file series`
with one volume (a Tiff_ or BigTiff_) per :term:`timepoint`.  Data is loaded
for a range of times: from :ref:`first <config-input>` to :ref:`last <config-input>`, inclusive.

A filename pattern is used to resolve which file to load for each timepoint.
The pattern should use printf-style_
formatting to specify how to form the filename for an input :term:`frame`.
Up to four usages of the timepoint can be made.  For example, for timepoint 30::

    path/%d/%05d.tif

will become::

    path/30/00030.tif

It doesn't really matter whether the referenced tif's represent 2d images or
3d volumes; the downstream processing is the same.

.. note::

    I've been testing on 3d data.  Trying to use this with 2d data will likely
    run into problems, but it should work in principle.

.. _printf-style: https://en.wikipedia.org/wiki/Printf_format_string
.. _BigTiff: http://bigtiff.org/
.. _Tiff: https://en.wikipedia.org/wiki/Tagged_Image_File_Format
