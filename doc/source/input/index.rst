Input Files
============

:program:`falcor` expects certain things from it's input files.

.. toctree::

  fileseries
  stimulus
