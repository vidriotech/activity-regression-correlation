Stimulus
--------

A text file containing integers on seperate lines.  Each integer represents the
:term:`frame` where a :term:`stimulus` occured.  They should be listed in order
of increasing time.

For example, this file:

.. code-block:: txt

    27
    55
    83
    111

describes four stimulation events where the first occured on frame 27.

.. note::

    The first frame of the time series is time 0 here.

.. versionchanged:: 0.3

    This file used to be encoded as a binary file with 1 byte for each timepoint
    in the timeseries. If a stimulus occured at time t, byte t was
    non-zero.  Otherwise, the byte was zero.  It was changed to the text format
    to accomidate existing workflows and because it's a little simpler to create.
