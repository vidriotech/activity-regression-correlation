Pipeline
========

This process data in a series of steps that we like to call **the pipeline**.

GPU Requirements
----------------

:program:`falcor` requries the use of a CUDA_ capable GPU with `compute
capability`_ of 3.0 or better (roughly speaking, newer than a GTX 650).

.. _CUDA: http://www.nvidia.com/object/cuda_home_new.html
.. _compute capability: https://developer.nvidia.com/cuda-gpus

Memory
''''''

Currently, the entire data set is uploaded to the GPU.  So the GPU needs to be
able to hold all the raw data for the timeseries at the very least.
Input data must consist of signed 16-bit voxels [#InputType]_.

In addition to space for the raw data, the correlation computation requires
some additional space for accumulators and kernel estimates. If :math:`N` is the
number of voxels in a single timepoint and :math:`W` is the
:ref:`window <config-params>` size, then to process :math:`T` timepoints, at least
:math:`[4(4+W)+2T]N` bytes are required [#Cropping]_.

A little more memory is requried in other places, so the above represents a
lower bound.

Loading data
------------

Input timeseries data is assumed to consist of a sequence of Tiff [#InputFiles]_
files. One file holds a 3D volume's worth of data (using a multi-page tiff) with
signed 16-bit voxels [#InputType]_.

The files in the timeseries are specified in the :ref:`config-input` section of
the :ref:`Configuration` file.

For example:

.. code-block:: lua

    input={
        timepoint="E:/Data/%d/sweet_sweet_data_00003_%05d.tif",
        first=1,
        last=4,
    }

Would load::

    E:/Data/1/sweet_sweet_data_00003_00001.tif
    E:/Data/2/sweet_sweet_data_00003_00002.tif
    E:/Data/3/sweet_sweet_data_00003_00003.tif
    E:/Data/4/sweet_sweet_data_00003_00004.tif

Loading stimulus times from a text file
'''''''''''''''''''''''''''''''''''''''

Use the ``stimulus`` field of the :ref:`config-input` section in the
:ref:`Configuration` file to specify a text file from which stimulus times
will be extracted.

The text file should be a whitespace_-delimeted list of integers.  Each integer
should correspond to a timepoint when a stimulus occured.  The stimulus times
must be in ascending order (low to high) and expressed in units of frame indexes.
The first frame is timepoint 0, the second is 1, and so on.

Example::

    34
    49
    64
    79

.. note::

    If ``options.extract_stim_times_from_images`` is set to true, this text file
    will be ignored.

.. _whitespace: http://www.cplusplus.com/reference/cctype/isspace/

Extracting stimulus times from the image
''''''''''''''''''''''''''''''''''''''''

The last plane of each timepoint can be used to detect the stimulus by
comparing the average intensity to a threshold.

To enable, there are a few changes requied in the :ref:`Configuration` file.

.. code-block:: lua

    options.extract_stim_times_from_images=true
    parameters.stimulus_threshold=6

.. figure:: _static/stimdt.png
    :figwidth: 50%
    :align: right

    Example plot of the stimdt file.

Write extracted stimulus times
''''''''''''''''''''''''''''''

To check extracted stimulus times, enable ``write_stimdt`` and specify
the ``stimdt`` output file in :ref:`config-output`.  The stimdt file is a raw
binary file consisting of one 32-bit signed integer for each timepoint.  The
value at each timepoint is the time since the last stimulus.  The timepoints
where a stimulus occur will have a value of 0.  The timepoints before the first
stimulus have a value of -1.

It looks a little strange, but this way of looking at stimulus times is valuable
because it's a raw view of how stimulus times are presented to the correlation
procedure.

2D Median filter
----------------

Median filters are expensive [#MedianAlternatives]_.  It's easily the most expensive step in the
pipeline.  The current implementation executes on the GPU and is reasonable
for small kernel sizes, but gets much more expensive as the kernel radius
increases [#MedianComplexity]_.

Only a 2D filtering is performed; each plane is treated independantly.  This occurs
just before motion correction.  It's mainly used here for denoising.

To disable median filtering, set the :ref:`median filter radius <config-params>`
to zero in the :ref:`Configuration`.  The result from median filtering will be
saved to a file if the :ref:`write_median <config-options>` option is set to
true.

.. raw:: html

    <div>
    <p style="font-weight:bold;font-size:18;text-align:center;">
        Maximum intensity z-projections before and after filtering
    </p>
    <table>
    <tr style="width:100%">
        <td><video style="width:100%; height:auto;" controls>
            <source src="_static/MAX_orig-1.mp4" type="video/mp4">
        </video></td>
        <td><video style="width:100%; height:auto;" controls>
            <source src="_static/MAX_median_rad2-1.mp4" type="video/mp4">
        </video></td>
    </tr>
    <tr>
        <td align="center">
            Raw
        </td>
        <td align="center">
            2D Median Filter: Radius 2
        </td>
    </tr>
    </table>
    </div>

Median filtering is very effective at reducing salt-and-pepper noise in the
original dataset.

.. [#MedianComplexity] I'm using the Cuda 7.5 NPP implementation.  I'm not sure
    of the exact algorithm, but it's probably :math:`O(r^2 log r)` in the
    kernel radius, :math:`r`.  There are :math:`O(1)` algorithms, but they
    are ill-suited to acceleration with GPUs.

Motion correction
-----------------

Motion correction is performed by measuring 3D voxel-precision translations
over time.  The procedure looks like:

1. Max projection along x, y, and z respectively.
2. Take the first timepoint as the reference frame.  Measured translations are
   elative to this timepoint.
3. For each projection, compute 2d `phase correlation`_ between each timepoint
   in max projections and the reference frame.
4. Extract 2d displacement vectors by finding the maxima in the phase
   correlation result.  This gives a :math:`(\Delta x,\Delta y)`,
   :math:`(\Delta x,\Delta z)`, and :math:`(\Delta y,\Delta z)` estimate for
   each timepoint.
5. Combine the translations's from each projection into a final set of
   displacements.  For the final :math:`(\Delta x,\Delta y)`, the z-projection
   is used; the other projections are ignored because they appear to give
   noisier estimates. The final :math:`\Delta z` is a rounded average of the
   translations estimated from the x and y projections.

.. _phase correlation: https://en.wikipedia.org/wiki/Phase_correlation

.. note::

    To output the measured motion vectors, enable ``write_motion_vectors``
    in :ref:`config-options`.  A file called ``dr.*.f32`` will be generated
    in the :ref:`output folder <config-output>`.  The ``*`` in the filename
    will be replaced with the number of columns and rows in the data.  In
    this case, there are always 3 columsns for `dx`,`dy` and `dz`.  The
    rows will correspond to the timepoints in the input.  The data in the file
    is stored as raw 32-bit floating point numbers in row-major order.  For
    more information see :ref:`how-to-read-raw`.

.. raw:: html

    <table class="center">
    <tr>
    <td colspan="2">
        <p style="font-weight:bold;font-size:18;text-align:center;">
            Phase-correlation of max projections
        </p>
    </td>
    </tr>
    <tr>
        <td align="center">
            YZ
        </td>
        <td align="center">
            XY
        </td>
    </tr>
    <tr style="width:100%">
        <td><video style="width:100%; height:auto;" controls>
            <source src="_static/shifted.xcorr.1-1.mp4" type="video/mp4">
        </video></td>
        <td><video style="width:100%; height:auto;" controls>
            <source src="_static/shifted.xcorr.2-1.mp4" type="video/mp4">
        </video></td>
    </tr>
    <tr>
        <td></td>
        <td><video style="width:100%; height:auto;" controls>
            <source src="_static/shifted.xcorr.0-1.mp4" type="video/mp4">
        </video></td>
    </tr>
    <tr>
        <td></td>
        <td align="center">
            XZ
        </td>
    </tr>
    </table>

The videos (above) show how the phase-correlation changes over time for each
projection.  There isn't much displacement in these videos: maybe 1-2 pixels in
Y and little to none in X or Z.  The results have a very nice single peak for
this data.  Notice that in the first frame only the (0,0) pixel has any signal.
This is because the first frame is the reference frame, and so there is no
translation.

.. raw:: html

    <table class="center">
    <tr>
    <td colspan="3">
        <p style="font-weight:bold;font-size:18;text-align:center;">
            3D motion correction example
        </p>
    </td>
    </tr>
    <tr style="width:100%">
        <td><video style="width:100%; height:auto;" controls>
            <source src="_static/MAX_motion_correction_test_in-1.mp4" type="video/mp4">
        </video></td>
        <td><video style="width:100%; height:auto;" controls>
            <source src="_static/phase_correlation-1.mp4" type="video/mp4">
        </video></td>
        <td><video style="width:100%; height:auto;" controls>
            <source src="_static/MAX_motion_correction_test_out-2.mp4" type="video/mp4">
        </video></td>
    </tr>
    <tr>
        <td align="center">
            Raw (z max-projection)
        </td>
        <td align="center">
            Phase correlation from z max-projections
        </td>
        <td align="center">
            Motion corrected (Z max-projection)
        </td>
    </tr>
    </table>

Once the translations are measured, the input data can be resampled in order to
produced a motion-stabilized output.  A simple example is shown above.  In this
computationally generated dataset only integer-translations were used resulting
in a very sharp phase-correlation peak.  The motion is perfectly corrected.
Mostly this just proves that we got the signs right during the correction step.

.. raw:: html

    <div>
    <p style="font-weight:bold;font-size:18;text-align:center;">
        Z max-projection after motion correction.
    </p>
    <table class="center">
    <tr style="width:100%">
        <td><video style="width:100%; height:auto;" controls>
            <source src="_static/xy_postmotion.mp4" type="video/mp4">
        </video></td>
    </tr>
    <tr style="width:100%">
        <td><p>
        Some residual jitter is apparent.  This appears to result from limiting
        the translations and resampling to pixel precision.
        </p></td>
    </tr>
    </table>
    </div>

Stimulus correlation
--------------------

.. raw:: html

    <div>
    <p style="font-weight:bold;font-size:18;text-align:center;">
        \(R^2\) and Mean.  Rotated projections.
    </p>
    <table class="center">
    <tr style="width:100%">
        <td><video style="width:100%; height:auto;" controls>
            <source src="_static/r2_overlay_rot.mp4" type="video/mp4">
        </video></td>
    </tr>
    <tr style="width:100%">
        <td><p>
        \(R^2\) in red.  Mean intensity after median filtering (radius 2) is in
        grey.  Time course involved 350 timepoints and 10 stimulation events.
        </p></td>
    </tr>
    </table>
    </div>

Voxel standardization
'''''''''''''''''''''

Ultimately, the goal here is to compare the quality of different linear models
developed at each voxel.  A linear fit is done at each voxel, and we'd like to
be able to compare the resulting correlation coefficients across the volume.
To do this, we scale the observed responses to a uniform statistical space in a
process known as standardization.

Namely the raw input values, :math:`y` get transformed to a standardized value:

    :math:`z(t) = \frac{y(t)-\mu}{\sigma}`

where :math:`\mu` and :math:`\sigma` are the mean and standard deviation
estimated at that voxel.

Stimulus response and correlation
'''''''''''''''''''''''''''''''''

The correlation coefficient is computed as

    :math:`R^2=1-\frac{\sum{e^2} }{SS_{tot} }`

where

    :math:`SS_{tot}=\sum{(z(t)-\bar{z})^2}`

and

    :math:`e=z(t)-z_{prediction}(t)`.

The predicted response, :math:`z_{prediction}(t)` is determined from a learned
model.  In this case a simple stimulus response function is learned at each
voxel be averaging observed responses as a function of time since the last
stimulus.


.. _limitations:

Limitations
-----------

* The correlation calculation is limited to response functions that occur within
  a specific window (see :ref:`config-params`).  Outside the window, it's assumed
  that the response is zero.

* The window size must be less than the interval between stimulus so that each
  stimulus is more-or-less independant.

* It's also assumed that each stimulus is identical.  For example, the amplitude
  and duration of the stimulus should always be the same.

Using these assumptions makes the computation of the response functions
relatively simple and fast.

.. note::

    It might be worth revisiting this restriction in the future so that
    response functions that straddle multiple stimuli can be learned.

.. rubric:: Footnotes

.. [#InputFiles] Files can be Tiff_ or BigTiff_ files.  The library used to read the
    tiff files is the ScanImageTiffReader_.

.. [#InputType] It should be very simple to generalize the code to handle other input
    types if necessary.

.. [#Cropping] One of the best ways to make things fit into memory might be
    cropping the input.  It would be simple to add this just after loading the
    data.  Since the input data is 3D, even a little bit of cropping in (x,y,z)
    can make a pretty big difference.

.. [#MedianAlternatives] In some sense this is doing a combination of noise
    removal and smoothing.  Really just want a way of denoising that doesn't
    blur too much.  Would be nice if it could be 3d too.

.. _Tiff: https://en.wikipedia.org/wiki/Tagged_Image_File_Format
.. _BigTiff: http://bigtiff.org/
.. _ScanImageTiffReader: http://scanimage.gitlab.io/ScanImageTiffReaderDocs/
