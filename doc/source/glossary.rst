Terms
=====

.. glossary::

    frame
        One volume acquired to sample activity at a specific point in time.
        It's worth pointing out that in this context, it's distinct from an
        image.  A volume will be built of several images acquired at different
        planes.  Ideally, this happens quickly enough so that it approximates
        a single sample in time.

    timepoint
        A single sample of a time-varying measurement.

    stimulus response
        The time-dependent response of a system to an impulse.
