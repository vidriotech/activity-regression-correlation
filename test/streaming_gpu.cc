#include <nd.h>
#include <tiff.reader.api.h>
#include <stdlib.h>
#include <stdio.h>
#include <correlation.h>
#include <motion.h>
#include <maximum.h>
#include "tictoc.h"
#include <cuda_runtime.h>
#include <vops.h>
#include <stdarg.h>


// #define ROOT "E:/Allan"
//#define ROOT "D:/data/Allan"
//#define ROOT "E:/Data/Allan"
//#define ROOT "E:/Data/AllanWong"
#define ROOT "G:/Data/Allan"

#define DATASET (datasets[0])

static struct fileseries {
  int first, last, step;
  int thresh;
  int window;
  const char* filename_template;
} datasets[]={
    // LOW MEM - SMALL
#if 0
  {1,350,10 ,3,4,50,ROOT "/20151012_r3_ss01147_nsyblexa_flyb_00001/20151012_r3_ss01147_nsyblexa_flya_00001_00001 (%d).tif"},
  {1,350,10 ,6,4,50,ROOT "/20150716_r3_ss1189da1_frup65_flyb_003/20150716_r3_ss1189da1_frup65_flyb_5.1um_00003_%05d.tif"},
#endif
    // FULL Data sets
  {1,350,1,3,12,ROOT "/20151012_r3_ss01147_nsyblexa_flyb_00001/20151012_r3_ss01147_nsyblexa_flya_00001_00001 (%d).tif"},
  {1,350,1,6,12,ROOT "/20150716_r3_ss1189da1_frup65_flyb_003/20150716_r3_ss1189da1_frup65_flyb_5.1um_00003_%05d.tif"},
};



#define LOG(str) puts(str);
#define CHECK(e) do{if(!((e))) {LOG(#e); abort(); goto Error;}}while(0)
#define SICHECK(...) __VA_ARGS__; do{if(h.log) {LOG(h.log); goto Error;}}while(0)
#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        printf("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        abort(); \
        goto Error; \
        }\
}while(0)

static size_t nbytes(const struct nd shape) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return shape.strides[shape.ndim]*bpp[shape.type];
}

struct vol* load(const struct fileseries t) {
    char fname[1024]={0};
    struct ScanImageTiffReader h={0};
    struct nd shape={0};
    struct vol *vol=0;

    sprintf_s(fname,sizeof(fname),t.filename_template,t.first);
    SICHECK(h=ScanImageTiffReader_Open(fname));
    SICHECK(shape=ScanImageTiffReader_GetShape(h));
    shape.dims[shape.ndim]=(t.last-t.first+1)/t.step;
    shape.strides[shape.ndim+1]=shape.dims[shape.ndim]*shape.strides[shape.ndim];
    ++shape.ndim;
    CHECK(vol=(struct vol*)malloc(sizeof(*vol)+nbytes(shape)));
    vol->shape=shape;
    CHECK(shape.ndim==4);

    SICHECK(ScanImageTiffReader_GetData(h,vol->data,nbytes(shape)));
    SICHECK(ScanImageTiffReader_Close(h));

    shape.ndim=3;
    const size_t st=nbytes(shape);
    shape.ndim=4;
    char *cur=vol->data+st;
    for(int i=t.first+t.step;i<=t.last;i+=t.step) {
        sprintf_s(fname,sizeof(fname),t.filename_template,i);
        SICHECK(h=ScanImageTiffReader_Open(fname));
        SICHECK(ScanImageTiffReader_GetData(h,cur,st));
        SICHECK(ScanImageTiffReader_Close(h));
        cur+=st;
    }
    return vol;
Error:
    printf("\t%s\n",t.filename_template);
    return 0;
}

static size_t size(const struct vol* v,int idim) {
    return v->shape.dims[idim];
}

static size_t bytestride(const struct vol* v, int idim) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return v->shape.strides[idim]*bpp[v->shape.type];
}

static struct nd subvol(const struct vol* v, int idim) {
    struct nd shape=v->shape;
    shape.ndim=idim;
    return shape;
}

static float mean(const struct nd* shape,char* data) {
    CHECK(shape->type==nd_i16);
    int16_t
        *v  =(int16_t*)data,
        *end=v+shape->strides[2];
    float acc=0.0f;
    while(v<end) acc+=*v++;
    return acc/(float)shape->strides[2];
Error:
    return 0.0f;
}

static uint32_t vget_i32(struct vol* v,size_t i) {
    CHECK(v->shape.type=nd_i32);
    uint32_t* d=(uint32_t*)v->data;
    return d[i];
Error:
    return 0;
}

/**
Allocates and returns the stimdt array by analyzing volumes in the timeseries.
Caller is responsible for free'ing the returned pointer.

    Assumes

        The last plane of each timepoint can be used to detect the stimulus by
        comparing the average intensity to a threshold.

        Input volume has uint16 values.

    Output

        `stimdt` counts the number of frames since the last stimulus.
        Before the first stimulus, timepoints get a dt of -1.
*/
static struct vol* extract_stim_dt(struct vol *timeseries,int thresh) {
    const size_t n=timeseries->shape.dims[3];

    struct vol *stimdt=(struct vol*)malloc(n*sizeof(int32_t)+sizeof(*stimdt));
    stimdt->shape.ndim=1;
    stimdt->shape.type=nd_i32;
    stimdt->shape.dims[0]=n;
    stimdt->shape.strides[0]=1;
    stimdt->shape.strides[0]=n;
    int32_t *dt=(int32_t*)stimdt->data;

    const int iplane=size(timeseries,2)-1;
    const size_t lastplane_offset=iplane*bytestride(timeseries,2);
    const size_t timepoint_offset=bytestride(timeseries,3);
    char* cur=timeseries->data+lastplane_offset;
    struct nd shape=subvol(timeseries,3);
    float counter=-1;
    for(size_t i=0;i<timeseries->shape.dims[3];++i,++dt) {
        float avg=mean(&shape,cur+i*timepoint_offset);
        if(avg>thresh)
            counter=0;
        *dt=counter;
        if(counter>=0)
            ++counter;
    }
    {
        FILE *fp=fopen("stimdt.i32","wb");
        fwrite(stimdt->data,sizeof(int32_t),n,fp);
        fclose(fp);
    }
    return stimdt;
}

static float * alloc_correlation(const accumulator* a) {
    return (float*)malloc(a->bytesof_correlation());
}

#include "tiff.write.h"
static void write(const char* fmt,int i,const struct nd shape,const float* data) {
    char filename[1024]={0};
    sprintf_s(filename,sizeof(filename),fmt,i);
    printf("Writing %s\n",filename);
    write_tiff_f32(filename,shape.ndim,shape.dims,data);
}

static void* move_to_gpu(void* buf,struct nd shape) {
    void *p=0;
    CUDACHK(cudaMalloc(&p,nbytes(shape)));
    CUDACHK(cudaMemcpy(p,buf,nbytes(shape),cudaMemcpyHostToDevice));
    return p;
Error:
    return 0;
}

static int save_gpu(void *p,struct nd shape,const char* filename_fmt,...) {
    void *out=0;
    CHECK(out=malloc(nbytes(shape)));
    CUDACHK(cudaMemcpy(out,p,nbytes(shape),cudaMemcpyDeviceToHost));
    {
        char fname[1024]={0};
        va_list args;
        va_start(args,filename_fmt);
        vsprintf_s(fname,sizeof(fname),filename_fmt,args);
        va_end(args);
		switch(shape.type)
		{
			case nd_i16: write_tiff_i16(fname,shape.ndim,shape.dims,(int16_t*)out); break;
			case nd_f32: write_tiff_f32(fname,shape.ndim,shape.dims,(float*)out); break;
			default:
				throw "TODO";
		}


    }
    free(out);
    return 1;
Error:
    return 0;
}

static int dot_int_i64_f32(size_t n,int64_t *a,float* b) {
    int out=0;
    for(size_t i=0;i<n;++i)
        out+=a[i]*b[i];
    return out;
}

static void restride(struct nd* shape) {
    shape->strides[0]=1;
    for(int i=1;i<=shape->ndim;++i)
        shape->strides[i]=shape->strides[i-1]*shape->dims[i-1];
}

#include <nppi.h>

static size_t prod_sz(size_t n,size_t *v) {
	size_t out=1;
	for(size_t i=0;i<n;++i) out*=v[i];
	return out;
}

int main(int argc,char *argv[]) {
    struct vol *vol=0;
    struct vol *stimdt=0;
    struct vol *dr=0; // motion
    float *r2;
    struct dev { void *src,*max,*min,*dst; } dev={0};

	TicTocTimer clock=tic();
    {
        TicTocTimer t=tic();
        CHECK(vol=load(DATASET));
        double dt=toc(&t);
        printf("LOAD: Elapsed %4.2f s\n"
                "\t%6.3g B/s\n"
                "\t%4.2f timepoints/s\n",
                dt,nbytes(vol->shape)/dt,vol->shape.dims[3]/dt);
    }

    CHECK(stimdt=extract_stim_dt(vol,DATASET.thresh));
	CHECK(dev.src=move_to_gpu(vol->data,vol->shape));

	// Median filter
	// Npp's median filter is bad...doesn't work right
	if(0){
		int nplanes=prod_sz(vol->shape.ndim-2,vol->shape.dims+2);
		NppiSize
			roi={vol->shape.dims[0],vol->shape.dims[1]},
			mask={5,5};
		NppiPoint anchor={mask.width/2,mask.height/2};
		Npp8u* scratch=0;
		Npp32u sz;
		CHECK(vol->shape.type==nd_i16);
		NppStatus ecode=nppiFilterMedianGetBufferSize_16s_C1R(roi,mask,&sz);
		if(sz)
			CUDACHK(cudaMalloc(&scratch,sz));
		TicTocTimer t=tic();
		for(int i=0;i<nplanes;++i){
			Npp16s *im=(Npp16s*)dev.src+i*vol->shape.strides[2];
			NppStatus ecode=nppiFilterMedian_16s_C1R(im,2*vol->shape.strides[1],im,2*vol->shape.strides[1],roi,mask,anchor,scratch);
		}
		double dt=toc(&t);
		printf("MEDIAN: Elapsed %4.2f s\n"
			   "\t%6.3g B/s\n"
			   "\t%4.2f timepoints/s\n",
			   dt,nbytes(vol->shape)/dt,vol->shape.dims[3]/dt);
		if(scratch)
			CUDACHK(cudaFree(scratch));
		{
			size_t n=vol->shape.dims[3];
			vol->shape.dims[3]=1;
			save_gpu(dev.src,vol->shape,"median.tif");
			vol->shape.dims[3]=n;
		}
	}

	// Motion estimation
    {
        TicTocTimer t=tic();
        CHECK(dr=motion(dev.src,vol->shape));
        double dt=toc(&t);
        printf("MOTION: Elapsed %4.2f s\n"
               "\t%6.3g B/s\n"
               "\t%4.2f timepoints/s\n",
               dt,nbytes(vol->shape)/dt,vol->shape.dims[3]/dt);
		if(0){
			// save final deltas
			char fname[1024]={0};
			sprintf_s(fname,sizeof(fname),
					  "dr.%dx%d.f32",
					  dr->shape.dims[0],
					  dr->shape.dims[1]);
			FILE *fp=fopen(fname,"wb");
			fwrite(dr->data,nbytes(dr->shape),1,fp);
			fclose(fp);
		}
    }

    // normalize (looks bad? needs motion correction)
    #if 0
    {
        struct nd proj_shape=get_output_shape__maximum(vol->shape,3);
        proj_shape.type=nd_f32;
        CUDACHK(cudaMalloc(&dev.max,nbytes(proj_shape)));
        CUDACHK(cudaMalloc(&dev.min,nbytes(proj_shape)));
        CHECK(maximum(dev.max,proj_shape,dev.src,vol->shape,3));
        CHECK(minimum(dev.min,proj_shape,dev.src,vol->shape,3));
        CUDACHK(cudaDeviceSynchronize());
        for(unsigned i=0;i<vol->shape.dims[3];++i) {
            vnorm_to_range_ip_i16_f32_f32_gpu(
                vol->shape.strides[3],
                (int16_t*)dev.src+i*vol->shape.strides[3],
                (float*)dev.min,
                (float*)dev.max);
            CUDACHK(cudaDeviceSynchronize());
        }
        //save_gpu(dev.src,vol->shape,"norm.tif"); // she's a big one
    }
    #endif

    // TODO: accumulator use shifts
    // TODO: accumulator uses gpu ptrs as inputs
    {
      accumulator accumulator(vol->shape,DATASET.window);
      CHECK(r2=alloc_correlation(&accumulator));
      CHECK(dr->shape.type==nd_f32);
      for(int i=0;i<vol->shape.dims[3];++i) {
		  if(vget_i32(stimdt,i)>=0) {
			  int offset=dot_int_i64_f32(3,vol->shape.strides,(float*)dr->data+i*dr->shape.strides[1]);
			  accumulator.update_kernel(vget_i32(stimdt,i),(int16_t*)dev.src+i*vol->shape.strides[3],offset);
		  }
      }
	  if(0){
		  // save kernel estimate
		  struct nd shape;
		  shape.type=nd_f32;
		  shape.ndim=4;
		  memcpy(shape.dims,vol->shape.dims,sizeof(shape.dims[0])*3);
		  shape.dims[3]=accumulator.window;
		  restride(&shape);
		  save_gpu(accumulator.K,shape,"K.tif");
	  }
	  if(0){
		  // save accumulators
		  struct nd shape;
		  shape.type=nd_f32;
		  shape.ndim=3;
		  memcpy(shape.dims,vol->shape.dims,sizeof(shape.dims[0])*3);
		  restride(&shape);
		  save_gpu(accumulator.y,shape ,"pre_y.tif");
		  save_gpu(accumulator.y2,shape,"pre_y2.tif");
		  save_gpu(accumulator.e,shape ,"pre_e.tif");
		  save_gpu(accumulator.e2,shape,"pre_e2.tif");
	  }
      for(int i=0;i<vol->shape.dims[3];++i) {
          int offset=dot_int_i64_f32(3,vol->shape.strides,(float*)dr->data+i*dr->shape.strides[1]);
          accumulator.update_error(vget_i32(stimdt,i),(int16_t*)dev.src+i*vol->shape.strides[3],offset);
      }
	  CUDACHK( cudaPeekAtLastError() );
	  CUDACHK( cudaDeviceSynchronize() );
	  if(0){
		  // save accumulators
		  struct nd shape;
		  shape.type=nd_f32;
		  shape.ndim=3;
		  memcpy(shape.dims,vol->shape.dims,sizeof(shape.dims[0])*3);
		  restride(&shape);
		  save_gpu(accumulator.y,shape ,"y.tif");
		  save_gpu(accumulator.y2,shape,"y2.tif");
		  save_gpu(accumulator.e,shape ,"e.tif");
		  save_gpu(accumulator.e2,shape,"e2.tif");
	  }
      accumulator.correlation(r2);
      {
        write("r2_%05d.tif",vol->shape.dims[3],subvol(vol,3),r2);
      }

    }
	printf("DONE: Elapsed %f s\n",toc(&clock));
    return 0;
Error:
    return 1;
}

/* NOTES

TODO: add motion compensation to accumulator
TODO: remove the notion of history

-- OLD --

TODO: check out doing fully async...though I suppose thats not really the point.
      It might reduce memory consumption for this example.
      Hide gpu transfer during disk io.

TODO: ? should coellesce accumulator variables that index by voxel
      maybe would be better for cache coherency and is closer to how things should get done on the gpu

FIXME: As the Kernel estimate changes, the error needs to get updated for past
       observations.

       TODO: Keep (dt,vol) history.  Perhaps cache.
             Don't actually care about time; just dt.
       TODO: Separate error accumulation from other stuff
       TODO: ability to reset history?
*/
