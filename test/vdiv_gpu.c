#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <cuda_runtime.h>

extern void vdiv_vf32_vf32_f32_gpu(size_t n,float *z,const float *x,float y);

#define N (1<<20)

void breakme(){};
#define CHECK(e) do{if(!(e)) {printf(#e  "\n"); breakme(); goto Error;}}while(0)
#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        printf("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        breakme(); \
        goto Error; \
    }\
}while(0)


int main(int argc,char*argv[]) {
    float *a=malloc(N*sizeof(*a));
    float *b=malloc(N*sizeof(*b));
    for(int i=0;i<N;++i) {
        a[i]=(float)i;
        b[i]=(float)i;
    }

    float *ga;
    float *gb;
    CUDACHK(cudaMalloc(&ga,N*sizeof(*ga)));
    CUDACHK(cudaMalloc(&gb,N*sizeof(*gb)));
    CUDACHK(cudaMemcpy(ga,a,N*sizeof(*a),cudaMemcpyHostToDevice));
    CUDACHK(cudaMemcpy(gb,b,N*sizeof(*b),cudaMemcpyHostToDevice));
    vdiv_vf32_vf32_f32_gpu(N,ga,gb,17.0f);
    CUDACHK( cudaPeekAtLastError() );
    CUDACHK( cudaDeviceSynchronize() );
    CHECK(cudaSuccess==cudaGetLastError());
    CUDACHK(cudaMemcpy(a,ga,N*sizeof(*a),cudaMemcpyDeviceToHost));
    cudaFree(ga);
    cudaFree(gb);

    for(int i=0;i<N;++i) {
        float e=a[i]-(b[i]/17.0f);
        CHECK(e*e<1e-3);
    }

    return 0;
Error:
    return 1;
}
