#include <nd.h>
#include <tiff.reader.api.h>
#include <stdlib.h>
#include <stdio.h>
#include "../src/tiff.write.h"

// #define ROOT "E:/Allan"
//#define ROOT "D:/data/Allan"
#define ROOT "E:/Data/AllanWong"

static struct fileseries {
  int first, last;
  int thresh;
  int window;
  const char* filename_template;
} datasets[]={
    // LOW MEM - SMALL
#if 0
  {1,35 ,3,12,ROOT "/20151012_r3_ss01147_nsyblexa_flyb_00001/20151012_r3_ss01147_nsyblexa_flya_00001_00001 (%d).tif"},
  {1,35 ,6,12,ROOT "/20150716_r3_ss1189da1_frup65_flyb_003/20150716_r3_ss1189da1_frup65_flyb_5.1um_00003_%05d.tif"},
#endif
    // FULL Data sets
  {1,350,3,12,ROOT "/20151012_r3_ss01147_nsyblexa_flyb_00001/20151012_r3_ss01147_nsyblexa_flya_00001_00001 (%d).tif"},
  {1,350,6,12,ROOT "/20150716_r3_ss1189da1_frup65_flyb_003/20150716_r3_ss1189da1_frup65_flyb_5.1um_00003_%05d.tif"},
};

#define LOG(str) puts(str);
#define CHECK(e) do{if(!((e))) {LOG(#e); abort(); goto Error;}}while(0)
#define SICHECK(...) __VA_ARGS__; do{if(h.log) {LOG(h.log); goto Error;}}while(0)

static size_t nbytes(const struct nd* shape,int ndim) {
  const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
  return shape->strides[ndim]*bpp[shape->type];
}

struct vol* load(const struct fileseries t) {
  char fname[1024]={0};
  struct ScanImageTiffReader h={0};
  struct nd shape={0};
  struct vol *vol=0;

  sprintf_s(fname,sizeof(fname),t.filename_template,t.first);
  SICHECK(h=ScanImageTiffReader_Open(fname));
  SICHECK(shape=ScanImageTiffReader_GetShape(h));
  shape.dims[shape.ndim]=t.last-t.first+1;
  shape.strides[shape.ndim+1]=shape.dims[shape.ndim]*shape.strides[shape.ndim];
  ++shape.ndim;
  CHECK(vol=(struct vol*)malloc(sizeof(*vol)+nbytes(&shape,shape.ndim)));
  vol->shape=shape;
  CHECK(shape.ndim==4);

  SICHECK(ScanImageTiffReader_GetData(h,vol->data,nbytes(&shape,shape.ndim)));
  SICHECK(ScanImageTiffReader_Close(h));

  const size_t st=nbytes(&shape,3);
  char *cur=vol->data+st;
  for(int i=t.first+1;i<=t.last;++i) {
      sprintf_s(fname,sizeof(fname),t.filename_template,i);
      SICHECK(h=ScanImageTiffReader_Open(fname));
      SICHECK(ScanImageTiffReader_GetData(h,cur,st));
      SICHECK(ScanImageTiffReader_Close(h));
      cur+=st;
  }
  return vol;
Error:
  return 0;
}

static size_t size(const struct vol* v,int idim) {
    return v->shape.dims[idim];
}

static size_t bytestride(const struct vol* v, int idim) {
    const size_t bpp[]={1,2,4,8,1,2,5,8,4,8};
    return v->shape.strides[idim]*bpp[v->shape.type];
}

static struct nd subvol(const struct vol* v, int idim) {
    struct nd shape=v->shape;
    shape.ndim=idim;
    return shape;
}

static float mean(const struct nd* shape,char* data) {
    CHECK(shape->type==nd_i16);
    int16_t
        *v  =(int16_t*)data,
        *end=v+shape->strides[2];
    float acc=0.0f;
    while(v<end) acc+=*v++;
    return acc/(float)shape->strides[2];
Error:
    return 0.0f;
}

static uint32_t vget_i32(struct vol* v,size_t i) {
    CHECK(v->shape.type=nd_i32);
    uint32_t* d=(uint32_t*)v->data;
    return d[i];
Error:
    return 0;
}

/**
Allocates and returns the stimdt array by analyzing volumes in the timeseries.
Caller is responsible for free'ing the returned pointer.

    Assumes

        The last plane of each timepoint can be used to detect the stimulus by
        comparing the average intensity to a threshold.

        Input volume has uint16 values.

    Output

        `stimdt` counts the number of frames since the last stimulus.
        Before the first stimulus, timepoints get a dt of -1.
*/
static struct vol* extract_stim_dt(struct vol *timeseries,int thresh) {
    const size_t n=timeseries->shape.dims[3];

    struct vol *stimdt=malloc(n*sizeof(int32_t)+sizeof(*stimdt));
    stimdt->shape.ndim=1;
    stimdt->shape.type=nd_i32;
    stimdt->shape.dims[0]=n;
    stimdt->shape.strides[0]=1;
    stimdt->shape.strides[0]=n;
    int32_t *dt=(int32_t*)stimdt->data;

    const int iplane=size(timeseries,2)-1;
    const size_t lastplane_offset=iplane*bytestride(timeseries,2);
    const size_t timepoint_offset=bytestride(timeseries,3);
    char* cur=timeseries->data+lastplane_offset;
    struct nd shape=subvol(timeseries,3);
    float counter=-1;
    for(size_t i=0;i<timeseries->shape.dims[3];++i,++dt) {
        float avg=mean(&shape,cur+i*timepoint_offset);
        if(avg>thresh)
            counter=0;
        *dt=counter;
        if(counter>=0)
            ++counter;
    }
    {
        FILE *fp=fopen("stimdt.i32","wb");
        fwrite(stimdt->data,sizeof(int32_t),n,fp);
        fclose(fp);
    }
    return stimdt;
}

/* circular buffer of history items*/
struct history {
    int head,tail,cur;
    struct nd shape;
    struct history_item {
        int dt;
        const void *data;
    } items[200];
};

struct accumulator {
    float *y,*y2,*e,*e2,*N,*K; // e is kind of just temporary storage.  probably could be eliminated.
    size_t count;
    size_t window;
    struct nd shape;
    int is_ok;

    struct history history;
};

static struct accumulator make_accumulator(struct vol* v, int windowsize) {
    struct accumulator a={0};
    CHECK(v->shape.ndim>=3);
    size_t nelem=v->shape.strides[3];
    CHECK(a.y =calloc(nelem,sizeof(float)));
    CHECK(a.y2=calloc(nelem,sizeof(float)));
    CHECK(a.e =calloc(nelem,sizeof(float)));
    CHECK(a.e2=calloc(nelem,sizeof(float)));
    CHECK(a.N =calloc(windowsize,sizeof(float)));
    CHECK(a.K =calloc(nelem*windowsize,sizeof(float)));
    a.count=0;
    a.window=windowsize;
    a.shape=v->shape;
    a.is_ok=1;
Error:
    return a;
}

static float * alloc_correlation(struct accumulator* a) {
    const size_t nelem=a->shape.strides[3];
    return malloc(sizeof(float)*nelem);
}

#define countof(e) (sizeof(e)/sizeof(*(e)))

static void hist_push(struct history* h,int dt,const void *data) {
    const int n=countof(h->items);
    int i=h->tail%n;
    h->items[i].dt=dt;
    h->items[i].data=data;
    h->tail++;
    h->head+=(h->tail%n)==(h->head%n);
}

static int hist_iter_beg(struct history* h) {
    h->cur=h->head;
    return h->cur%countof(h->items);
}
static int hist_iter_done(struct history *h) {
    const int  n=countof(h->items);
    return (h->cur%n)==(h->tail%n);
}
static int hist_iter_inc(struct history *h) {
    return (h->cur++)%countof(h->items);
}

/* a+=b */
static void add_vf32_vi16_ip(size_t n,float *a,size_t astride,const int16_t* b,size_t bstride) {
    const float * const end=a+astride*n;
    #pragma omp parallel for
    for(;a<end;a+=astride,b+=bstride)
        *a+=(float)*b;
}

/* a-=b */
static void sub_vf32_vi16_ip(size_t n,float *a,size_t astride,const int16_t* b,size_t bstride) {
    const float * const end=a+astride*n;
    #pragma omp parallel for
    for(;a<end;a+=astride,b+=bstride)
        *a-=(float)*b;
}

/* a=b */
static void cpy_vf32_vi16(size_t n,float *a,size_t astride,const int16_t* b,size_t bstride) {
    const float * const end=a+astride*n;
    #pragma omp parallel for
    for(;a<end;a+=astride,b+=bstride)
        *a=(float)*b;
}

/* a+=b*b */
static void addsq_vf32_vf32_ip(size_t n,float *a,size_t astride,const float* b,size_t bstride) {
    const float * const end=a+astride*n;
    #pragma omp parallel for
    for(;a<end;a+=astride,b+=bstride)
        *a+=*b**b;
}

/* a+=b*b */
static void addsq_vf32_vi16_ip(size_t n,float *a,size_t astride,const int16_t* b,size_t bstride) {
    const float * const end=a+astride*n;
    #pragma omp parallel for
    for(;a<end;a+=astride,b+=bstride) {
        const float t=(float)*b;
        *a+=t*t;
    }
}

/* z=x./y */
static void div_vf32_vf32_f32(size_t n,
                            float* z,size_t z_stride,
                            const float* x,size_t x_stride,
                            const float y) {
    const float * const end=z+z_stride*n;
    #pragma omp parallel for
    for(;z<end;z+=z_stride,x+=x_stride)
        *z=*x/y;
}

static void accumulate(struct accumulator* a,int dt,struct nd shape,int16_t* data) {
    const size_t nelem=a->shape.strides[3];
    hist_push(&a->history,dt,data);
    if(0<=dt && dt<a->window){
        float yhat;
        a->N[dt]++;                                     // number of samples at bin dt
        add_vf32_vi16_ip(nelem,a->K+dt*nelem,1,data,1); // sum of observations at dt
    }

    // update accumulators    
    add_vf32_vi16_ip(nelem,a->y,1,data,1);
    addsq_vf32_vi16_ip(nelem,a->y2,1,data,1);
    a->count++;
}

static void correlation(const struct accumulator *a,float *r2) {
    const size_t nelem=a->shape.strides[3];
    float n=0.0f;
    // recompute error
    for(int i=hist_iter_beg(&a->history);
             !hist_iter_done(&a->history);
            i=hist_iter_inc(&a->history)){
        int dt=a->history.items[i].dt;        
        uint16_t *data=(uint16_t*)a->history.items[i].data;
        if(0<=dt && dt<a->window){
            // prediction error
            // 1. e = prediction;
            // 2. e -= observer; <-- result is observed-predicted
            div_vf32_vf32_f32(nelem,
                              a->e,1,
                              a->K+dt*nelem,1,
                              a->N[dt]);
            sub_vf32_vi16_ip(nelem,a->e,1,data,1);
        } else{
            cpy_vf32_vi16(nelem,a->e,1,data,1);
        }
        // update sq. error accumulator
        addsq_vf32_vf32_ip(nelem,a->e2,1,a->e,1);
        n++;
    }
    // r2
    for(size_t i=0;i<nelem;++i) {
        const float sst=(a->y2[i]-a->y[i]*a->y[i]/a->count)/a->count;
        r2[i]=1-(a->e2[i]/n)/sst;
    }
}

static void write(const char* fmt,int i,const struct nd* shape,const float* data) {
    char filename[1024]={0};
    sprintf_s(filename,sizeof(filename),fmt,i);
    printf("Writing %s\n",filename);
    write_tiff_f32(filename,shape->ndim,shape->dims,data);
}

#define DATASET (datasets[1])

int main(int argc,char *argv[]) {
  struct vol *vol=load(DATASET);
  struct vol *stimdt=extract_stim_dt(vol,DATASET.thresh);
  float *r2;
  struct accumulator accumulator=make_accumulator(vol,DATASET.window);
  CHECK(accumulator.is_ok);
  CHECK(r2=alloc_correlation(&accumulator));
  {
      struct nd shape=subvol(vol,3);
      CHECK(vol->shape.type==nd_i16);
      int16_t *data=(int16_t*)vol->data;
      for(int i=0;i<vol->shape.dims[3];++i){
          printf("%d\n",i);
          int16_t *cur=data+vol->shape.strides[3]*i;
          accumulate(&accumulator,vget_i32(stimdt,i),shape,cur);
          if(i%35==34) {
              correlation(&accumulator,r2);
              write("r2_%05d.tif",i,&shape,r2);
          }
      }
  }
  return 0;
Error:
  return 1;
}

/* NOTES

TODO: check out doing fully async...though I suppose thats not really the point.
      It might reduce memory consumption for this example.

TODO: ? should coellesce accumulator variables that index by voxel
      maybe would be better for cache coherency and is closer to how things should get done on the gpu

FIXME: As the Kernel estimate changes, the error needs to get updated for past
       observations.

       TODO: Keep (dt,vol) history.  Perhaps cache.
             Don't actually care about time; just dt.
       TODO: Separate error accumulation from other stuff
       TODO: ability to reset history?
*/
