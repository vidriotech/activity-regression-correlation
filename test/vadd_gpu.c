#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <cuda_runtime.h>

extern void vadd_vf32_vi16_ip_gpu(size_t n, float *a,const int16_t* b);

#define N (1<<20)

void breakme(){};
#define CHECK(e) do{if(!(e)) {printf(#e  "\n"); breakme(); goto Error;}}while(0)
#define CHECK(e) do{if(!(e)) {printf(#e ); breakme(); goto Error;}}while(0)
#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        printf("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        breakme(); \
        goto Error; \
    }\
}while(0)


int main(int argc,char*argv[]) {
    float *a=malloc(N*sizeof(*a));
    int16_t *b=malloc(N*sizeof(*b));
    for(int i=0;i<N;++i) {
        a[i]=i;
        b[i]=i;
    }

    float *ga;
    int16_t *gb;
    CUDACHK(cudaMalloc(&ga,N*sizeof(*ga)));
    CUDACHK(cudaMalloc(&gb,N*sizeof(*gb)));
    CUDACHK(cudaMemcpy(ga,a,N*sizeof(*a),cudaMemcpyHostToDevice));
    CUDACHK(cudaMemcpy(gb,b,N*sizeof(*b),cudaMemcpyHostToDevice));
    vadd_vf32_vi16_ip_gpu(N,ga,gb);
    CUDACHK( cudaPeekAtLastError() );
    CUDACHK( cudaDeviceSynchronize() );
    CHECK(cudaSuccess==cudaGetLastError());
    CUDACHK(cudaMemcpy(a,ga,N*sizeof(*a),cudaMemcpyDeviceToHost));
    cudaFree(ga);
    cudaFree(gb);

    for(int i=0;i<N;++i) {
        float e=a[i]-(i+b[i]);
        CHECK(e*e<1e-3);
    }

    return 0;
Error:
    return 1;
}
