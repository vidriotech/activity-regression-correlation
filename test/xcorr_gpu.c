#include <nd.h>
#include <tiff.reader.api.h>
#include <stdio.h>
#include <stdlib.h>
#include <motion.h>
#include "tictoc.h"
#include <driver_types.h>
#include <cuda_runtime.h>



// #define ROOT "E:/Allan"
//#define ROOT "D:/data/Allan"
//#define ROOT "E:/Data/Allan"
//#define ROOT "E:/Data/AllanWong"
#define ROOT "G:/Data/Allan"

#define DATASET (datasets[0])


static struct fileseries {
	int first, last, step;
	int thresh;
	int window;
	int history;
	const char* filename_template;
} datasets[]={
	// LOW MEM - SMALL
#if 0
	{1,350,10 ,3,12,35,ROOT "/20151012_r3_ss01147_nsyblexa_flyb_00001/20151012_r3_ss01147_nsyblexa_flya_00001_00001 (%d).tif"},
	{1,350,10 ,6,12,35,ROOT "/20150716_r3_ss1189da1_frup65_flyb_003/20150716_r3_ss1189da1_frup65_flyb_5.1um_00003_%05d.tif"},
#endif
	// FULL Data sets
	{1,350,1,3,12,115,ROOT "/20151012_r3_ss01147_nsyblexa_flyb_00001/20151012_r3_ss01147_nsyblexa_flya_00001_00001 (%d).tif"},
	{1,350,1,6,12,115,ROOT "/20150716_r3_ss1189da1_frup65_flyb_003/20150716_r3_ss1189da1_frup65_flyb_5.1um_00003_%05d.tif"},
};


#define LOG(str) puts(str);
#define CHECK(e) do{if(!((e))) {LOG(#e); abort(); goto Error;}}while(0)
#define SICHECK(...) __VA_ARGS__; do{if(h.log) {LOG(h.log); goto Error;}}while(0)
#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        printf("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        abort(); \
        goto Error; \
        }\
}while(0)

static size_t nbytes(const struct nd shape) {
	const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
	return shape.strides[shape.ndim]*bpp[shape.type];
}

struct vol* load(const struct fileseries t) {
	char fname[1024]={0};
	struct ScanImageTiffReader h={0};
	struct nd shape={0};
	struct vol *vol=0;

	sprintf_s(fname,sizeof(fname),t.filename_template,t.first);
	SICHECK(h=ScanImageTiffReader_Open(fname));
	SICHECK(shape=ScanImageTiffReader_GetShape(h));
	shape.dims[shape.ndim]=(t.last-t.first+1)/t.step;
	shape.strides[shape.ndim+1]=shape.dims[shape.ndim]*shape.strides[shape.ndim];
	++shape.ndim;
	CHECK(vol=(struct vol*)malloc(sizeof(*vol)+nbytes(shape)));
	vol->shape=shape;
	CHECK(shape.ndim==4);

	SICHECK(ScanImageTiffReader_GetData(h,vol->data,nbytes(shape)));
	SICHECK(ScanImageTiffReader_Close(h));

	shape.ndim=3;
	const size_t st=nbytes(shape);
	shape.ndim=4;
	char *cur=vol->data+st;
	for(int i=t.first+t.step;i<=t.last;i+=t.step) {
		sprintf_s(fname,sizeof(fname),t.filename_template,i);
		SICHECK(h=ScanImageTiffReader_Open(fname));
		SICHECK(ScanImageTiffReader_GetData(h,cur,st));
		SICHECK(ScanImageTiffReader_Close(h));
		cur+=st;
	}
	return vol;
Error:
	return 0;
}

static void* move_to_gpu(void* buf,struct nd shape) {
    void *p=0;
    CUDACHK(cudaMalloc(&p,nbytes(shape)));
    CUDACHK(cudaMemcpy(p,buf,nbytes(shape),cudaMemcpyHostToDevice));
    return p;
Error:
    return 0;
}

int main(int argc,char* argv[]){
	struct vol* vol;
	struct dev{ void *src; } dev={0};
    struct vol* dr=0;

	{
		TicTocTimer t=tic();
		CHECK(vol=load(DATASET));
		double dt=toc(&t);
		printf("LOAD: Elapsed %4.2f s\n"
			   "\t%6.3g B/s\n"
			   "\t%4.2f timepoints/s\n",
			   dt,nbytes(vol->shape)/dt,vol->shape.dims[3]/dt);
	}
    CHECK(dev.src=move_to_gpu(vol->data,vol->shape));
    {
        TicTocTimer t=tic();
        CHECK(dr=motion(dev.src,vol->shape));
        double dt=toc(&t);
        printf("MOTION: Elapsed %4.2f s\n"
               "\t%6.3g B/s\n"
               "\t%4.2f timepoints/s\n",
               dt,nbytes(vol->shape)/dt,vol->shape.dims[3]/dt);
    }

	{
		// save final deltas
		const char fname[1024]={0};
		sprintf_s(fname,sizeof(fname),
				  "dr.final.%dx%d.f32",
				  dr->shape.dims[0],
				  dr->shape.dims[1]);
		FILE *fp=fopen(fname,"wb");
		fwrite(dr->data,nbytes(dr->shape),1,fp);
		fclose(fp);
	}

	return 0;
Error:
	return 1;
}
