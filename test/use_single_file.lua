version=0
input={
    timepoint=[[E:\Data\AllanWong\20151012_r3_ss01147_nsyblexa_flyb_00001/20151012_r3_ss01147_nsyblexa_flya_00001_00001 (1).tif]],
    stimulus=[[E:\Data\AllanWong\Test\stimFrames.txt]],
    first=1,
    last=350,
	chunk_size=100;
}
output={
    folder=[[E:/Data/Allan/Results/run]] .. os.time(),
    correlation="r2.tif",
    log="log.txt",
    stimdt='stimdt.i32',
	mean="mean.tif",
	projection="xy.tif"
}
parameters={
    gpuid=0, -- select the gpu to use.  For multi-gpu systems.  Optional. Default value is zero.
    window=12,
    stimulus_threshold=6, -- used for extracting stimulation events from image data
    median_radius=1 -- set to zero to skip the median filter
}
options={
    extract_stim_times_from_images=false,
	write_median                  =false,
    write_stimdt                  =true,
    write_motion_vectors          =true,
	write_kernel_estimate		  =false,
    use_single_file               =true,
    disable_motion_correction     =false
}