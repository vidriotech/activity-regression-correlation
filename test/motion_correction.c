#include <motion.h>
#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <math.h>
#include <tiff.write.h>
#include <config.h>
#include <string.h>

#define LOG(str) puts(str);
#define CHECK(e) do{if(!((e))) {LOG(#e); abort(); goto Error;}}while(0)
#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        printf("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        abort(); \
        goto Error; \
    }\
}while(0)

static size_t nbytes(const struct nd shape) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return shape.strides[shape.ndim]*bpp[shape.type];
}


static void restride(struct nd* shape) {
    shape->strides[0]=1;
    for(unsigned i=1;i<=shape->ndim;++i)
        shape->strides[i]=shape->strides[i-1]*shape->dims[i-1];
}

static int16_t f(int x, int y, int z, int t) {
    float
        n=1000.0f,
        s=3.0f,
        x0=32.f+10.0f*sin(4.f*t*(6.28f/64.0f)),
        y0=32.f+10.0f*cos(4.f*t*(6.28f/64.0f)),
        z0=32.f+5.0f*sin(4.f*t*(6.28f/64.0f)),
        dx=(x-(int)x0)/s,
        dy=(y-(int)y0)/s,
        dz=(z-(int)z0)/s;
    return n*expf(-dx*dx-dy*dy-dz*dz);
}

static struct vol* cons() {
    struct nd shape={
        .type=nd_i16,
        .ndim=4,
        .dims={64,64,64,64},
    };
    struct vol *v=0;
    restride(&shape);
    CHECK(v=malloc(sizeof(struct vol)+nbytes(shape)));
    v->shape=shape;
    return v;
Error:
    return 0;
}

static struct vol* make() {
    struct vol* v;
    CHECK(v=cons());
    int16_t *d=(int16_t*)v->data;
    for(int t=0;t<64;++t){
        for(int z=0;z<64;++z){
            for(int y=0;y<64;++y){
                for(int x=0;x<64;++x){
                    *d++=f(x,y,z,t);
                }
            }
        }
    }
    return v;
Error:
    return 0;
}

static void save(const struct vol* v,const char* fname) {
    switch(v->shape.type)
    {
        case nd_i16: write_tiff_i16(fname,v->shape.ndim,v->shape.dims,(int16_t*)v->data); break;
        case nd_f32: write_tiff_f32(fname,v->shape.ndim,v->shape.dims,(float*)v->data); break;
        default:
            goto Error;
    }
    return;
Error:
    LOG("save: unsupported type\n");
}

#if 0
struct float3 {
    float x,y,z;
};
#endif

static void resample(struct vol* dst,struct vol* src,struct vol* dr) {
    CHECK(src->shape.ndim==4);
    CHECK(src->shape.type=nd_i16);
    CHECK(dst->shape.ndim==4);
    CHECK(dst->shape.type=nd_i16);    
    CHECK(dr->shape.ndim=2);
    CHECK(dr->shape.type=nd_f32);
    CHECK(dr->shape.dims[0]==3);

    int16_t *s=(int16_t*)src->data;
    int16_t *d=(int16_t*)dst->data;
    int64_t *st=src->shape.strides;
    float3 zero={0.f,0.f,0.f};    
    float3 *r=(float3*)dr->data;

#if 0 // method 1: the obvious one
    /*
    NOTES (emperical)

     - Subtracting r, as done below, is the "right thing to do".
       That is, this is the correct sign for correcting the 
       motion for this resampling.

     - When the motion is integer translations, the reconstruction
       is perfect.

    */

    for(int t=0;t<64;++t){    
        for(int z=0;z<64;++z){
            int zz=z-r->z;
            if(0>zz||zz>=64) zz=0;
            for(int y=0;y<64;++y){
                int yy=y-r->y;
                if(0>yy||yy>=64) yy=0;
                for(int x=0;x<64;++x){
                    int xx=x-r->x;
                    if(0>xx||xx>=64) xx=0;
                    *d++=s[xx*st[0]+yy*st[1]+zz*st[2]+t*st[3]];              
                }
            }
        }
        r++;
    }
#else // method 2: static offset.  not entirely proper handling of boundary 
    /*
    NOTES

        - this also works.  It'll wrap the y boundary a bit.
    */

    for(int t=0;t<64;++t){    
        int o=r->x*st[0]+r->y*st[1]+r->z*st[2];
        const int16_t 
            *beg=s+t*st[3],
            *end=s+(t+1)*st[3];
        int16_t *c=beg-o;
        for(int z=0;z<64;++z){
            for(int y=0;y<64;++y){
                for(int x=0;x<64;++x){
                    *d++=(beg<=c && c<end)?*c:0;
                    ++c;
                }
            }
        }
        r++;
    }
#endif
Error:;
}

int main(int argc,char* argv) {
    struct vol* src,*dst;
    struct vol* dr;
    struct dev {
        void *src;
    } dev;
    strcpy(Configuration.output.folder,".");
    src=make();
    dst=cons();
    save(src,"motion_correction_test_in.tif");
    CUDACHK(cudaMalloc(&dev.src,nbytes(src->shape)));
    CUDACHK(cudaMemcpy(dev.src,src->data,nbytes(src->shape),cudaMemcpyHostToDevice));
    CHECK(dr=motion(dev.src,src->shape));

    {
        // save final deltas
        char fname[1024]={0};
        sprintf_s(fname,sizeof(fname),
                  "%s/dr.%dx%d.f32",
                  Configuration.output.folder,
                  dr->shape.dims[0],
                  dr->shape.dims[1]);
        FILE *fp=fopen(fname,"wb");
        fwrite(dr->data,nbytes(dr->shape),1,fp);
        fclose(fp);
    }

    resample(dst,src,dr);
    save(dst,"motion_correction_test_out.tif");
    return 0;
Error:
    return 1;
}
