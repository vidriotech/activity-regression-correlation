#include <nd.h>
#include <tiff.reader.api.h>
#include <stdio.h>
#include <stdlib.h>
#include "tiff.write.h"

// #define ROOT "E:/Allan"
#define ROOT "D:/data/Allan"
//#define ROOT "E:/Data/AllanWong"

#define DATASET (datasets[1])

static struct fileseries {
	int timepoint;
	const char* filename_template;
} datasets[]={
	// FULL Data sets
	{100,ROOT "/20151012_r3_ss01147_nsyblexa_flyb_00001/20151012_r3_ss01147_nsyblexa_flya_00001_00001 (%d).tif"},
	{100,ROOT "/20150716_r3_ss1189da1_frup65_flyb_003/20150716_r3_ss1189da1_frup65_flyb_5.1um_00003_%05d.tif"},
};

#define LOG(str) puts(str);
#define CHECK(e) do{if(!((e))) {LOG(#e); abort(); goto Error;}}while(0)
#define SICHECK(...) __VA_ARGS__; do{if(h.log) {LOG(h.log); goto Error;}}while(0)

static size_t nbytes(const struct nd* shape) {
	const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
	return shape->strides[shape->ndim]*bpp[shape->type];
}

/* Load a single time point */
struct vol* load(const struct fileseries t) {
  char fname[1024]={0};
  struct ScanImageTiffReader h={0};
  struct nd shape={0};
  struct vol *vol=0;

  sprintf_s(fname,sizeof(fname),t.filename_template,t.timepoint);
  SICHECK(h=ScanImageTiffReader_Open(fname));
  SICHECK(shape=ScanImageTiffReader_GetShape(&h));  
  CHECK(vol=(struct vol*)malloc(sizeof(*vol)+nbytes(&shape)));
  vol->shape=shape;
  CHECK(shape.ndim==3);

  SICHECK(ScanImageTiffReader_GetData(&h,vol->data,nbytes(&shape)));
  SICHECK(ScanImageTiffReader_Close(&h));
  return vol;
Error:
  return 0;
}

extern struct nd get_output_shape__maximum(struct nd src_shape,unsigned idim);
extern int maximum(void *dst,const struct nd dst_shape,
				   const void *src,const struct nd src_shape,
				   unsigned idim);

int main(int argc,char* argv[]) {
	struct vol* vol;
	CHECK(vol=load(DATASET));
	for(unsigned i=0;i<vol->shape.ndim;++i)
	{
		struct nd dst_shape=get_output_shape__maximum(vol->shape,i);
		void *dst;
		CHECK(dst=malloc(nbytes(&dst_shape)));
		CHECK(maximum(dst,dst_shape,vol->data,vol->shape,i));
		{
			char fname[1024]={0};
			sprintf_s(fname,sizeof(fname),"max.%d.tif",i);
			write_tiff_i16(fname,dst_shape.ndim,dst_shape.dims,dst);
		}
		free(dst);
	}
	
	return 0;
Error:
	return 1;
}