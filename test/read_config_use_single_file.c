#include <config.h>
#include <stdio.h>

#define _STR(e) #e
#define STR(e) _STR(e)

int main(int argc,char *argv[]) {
	int isok=1;
	isok &= LoadConfiguration(STR(CONFIGPATH),0,0,printf)==1;
	isok &= Configuration.options.use_single_file==1;
	return isok==0;
}
