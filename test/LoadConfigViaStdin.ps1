param(
    [string]$falcor
)
$content = [IO.File]::ReadAllText((join-path $PSScriptRoot "shortrun.lua"))

$psi = New-Object System.Diagnostics.ProcessStartInfo;
$psi.FileName = $falcor;
$psi.UseShellExecute = $false;
$psi.RedirectStandardInput = $true;

$p = [System.Diagnostics.Process]::Start($psi); #start falcor
$p.StandardInput.WriteLine($content);           #feed config string to stdin
