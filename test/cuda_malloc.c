#include <stdio.h>
#include <cuda_runtime.h>

#define log printf

#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        log("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        goto Error; \
        }\
}while(0)

void pdev(int i) {
    struct cudaDeviceProp prop;
    CUDACHK(cudaGetDeviceProperties(&prop,i));
    printf("%s\n\tGlobal %f GB\n",
        prop.name,
        prop.totalGlobalMem*1e-9);
    Error:;
}

int main(int argc,char*argv[]) {
    void *p=0;
    CUDACHK(cudaSetDevice(0));
    pdev(0);
    size_t nbytes=(size_t) (6.0*(1ULL<<30));
    printf("Trying to allocate %f GB\n",1e-9*nbytes);
    CUDACHK(cudaMalloc(&p,nbytes));
    CUDACHK(cudaFree(p));
    printf("Success\n");
    return 0;
Error:
    printf("Failure\n");
    return 1;
}