#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <cuda_runtime.h>

extern void vaddsq_vf32_vf32_ip_gpu(size_t n, float *a,const float* b);
extern void vaddsq_vf32_vi16_ip_gpu(size_t n, float *a,const int16_t* b);

#define N (1<<20)

void breakme(){};
#define CHECK(e) do{if(!(e)) {printf(#e  "\n"); breakme(); goto Error;}}while(0)
#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        printf("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        breakme(); \
        goto Error; \
    }\
}while(0)

int test_float() {
    float *a=malloc(N*sizeof(*a));
    float *b=malloc(N*sizeof(*b));
    for(int i=0;i<N;++i) {
        a[i]=i;
        b[i]=i;
    }

    float *ga;
    float *gb;
    CUDACHK(cudaMalloc(&ga,N*sizeof(*ga)));
    CUDACHK(cudaMalloc(&gb,N*sizeof(*gb)));
    CUDACHK(cudaMemcpy(ga,a,N*sizeof(*a),cudaMemcpyHostToDevice));
    CUDACHK(cudaMemcpy(gb,b,N*sizeof(*b),cudaMemcpyHostToDevice));
    vaddsq_vf32_vf32_ip_gpu(N,ga,gb);
    CUDACHK( cudaPeekAtLastError() );
    CUDACHK( cudaDeviceSynchronize() );
    CHECK(cudaSuccess==cudaGetLastError());
    CUDACHK(cudaMemcpy(a,ga,N*sizeof(*a),cudaMemcpyDeviceToHost));
    cudaFree(ga);
    cudaFree(gb);

    for(int i=0;i<N;++i) {
        const float v=b[i]; // this line here in case I'm fiddling with type of b.
        float e=a[i]-(i+v*v);
        CHECK(e*e<1e-3);
    }
    free(a);
    free(b);

    return 0;
Error:
    return 1;
}

int test_int16() {
    float *a=malloc(N*sizeof(*a));
    int16_t *b=malloc(N*sizeof(*b));
    for(int i=0;i<N;++i) {
        a[i]=i;
        b[i]=i;
    }

    float *ga;
    int16_t *gb;
    CUDACHK(cudaMalloc(&ga,N*sizeof(*ga)));
    CUDACHK(cudaMalloc(&gb,N*sizeof(*gb)));
    CUDACHK(cudaMemcpy(ga,a,N*sizeof(*a),cudaMemcpyHostToDevice));
    CUDACHK(cudaMemcpy(gb,b,N*sizeof(*b),cudaMemcpyHostToDevice));
    vaddsq_vf32_vi16_ip_gpu(N,ga,gb);
    CUDACHK( cudaPeekAtLastError() );
    CUDACHK( cudaDeviceSynchronize() );
    CHECK(cudaSuccess==cudaGetLastError());
    CUDACHK(cudaMemcpy(a,ga,N*sizeof(*a),cudaMemcpyDeviceToHost));
    cudaFree(ga);
    cudaFree(gb);

    for(int i=0;i<N;++i) {
        const float v=b[i]; // this line here in case I'm fiddling with type of b.
        float e=a[i]-(i+v*v);
        CHECK(e*e<1e-3);
    }

    free(a);
    free(b);
    return 0;
Error:
    return 1;    
}


int main(int argc,char*argv[]) {
    return test_float()
        && test_int16();
}


