#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <cuda_runtime.h>

extern void vcmul_vf32_vf32_vf32_gpu(size_t n,float *z,const float *x,const float *y);

#define N (1<<20)

void breakme(){};
#define CHECK(e) do{if(!(e)) {printf(#e  "\n"); breakme(); goto Error;}}while(0)
#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        printf("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        breakme(); \
        goto Error; \
    }\
}while(0)


int main(int argc,char*argv[]) {
    float *a=malloc(2*N*sizeof(*a));
    float *b=malloc(2*N*sizeof(*b));
	float *c=malloc(2*N*sizeof(*c));
    for(int i=0;i<N;++i) {
        b[i]=(float)i;
		c[i]=(float)i;
    }

	float *ga,*gb,*gc;
    CUDACHK(cudaMalloc(&ga,2*N*sizeof(*ga)));
    CUDACHK(cudaMalloc(&gb,2*N*sizeof(*gb)));
	CUDACHK(cudaMalloc(&gc,2*N*sizeof(*gc)));	
    CUDACHK(cudaMemcpy(gb,b,2*N*sizeof(*a),cudaMemcpyHostToDevice));
    CUDACHK(cudaMemcpy(gc,c,2*N*sizeof(*b),cudaMemcpyHostToDevice));
	vcmul_vf32_vf32_vf32_gpu(N,ga,gb,gc);
    CUDACHK( cudaPeekAtLastError() );
    CUDACHK( cudaDeviceSynchronize() );
    CHECK(cudaSuccess==cudaGetLastError());
    CUDACHK(cudaMemcpy(a,ga,2*N*sizeof(*a),cudaMemcpyDeviceToHost));
    cudaFree(ga);
    cudaFree(gb);
	cudaFree(gc);

    for(int i=0;i<N;++i) {
        float e
			=a[2*i  ]-(b[2*i]*c[2*i]-b[2*i+1]*c[2*i+1])
			+a[2*i+1]-(b[2*i]*c[2*i+1]+b[2*i+1]*c[2*i]);
        CHECK(e*e<1e-3);
    }

    return 0;
Error:
    return 1;
}
