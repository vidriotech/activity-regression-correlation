#include <config.h>
#include <stdio.h>

#define _STR(e) #e
#define STR(e) _STR(e)

int main(int argc,char *argv[]) {
	return LoadConfiguration(STR(CONFIGPATH),0,0,printf)!=1;
}
