#include <host_defines.h>
#include <cstdint>
#include <limits>

// vsub_ip

template<class Ta,class Tb>
__global__ void vadd_ip_k(
    int n,
    Ta * __restrict__ const a, 
    const Tb * __restrict__ const b) 
{
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
            k<n;
            k+=gridDim.x*blockDim.x) {
        a[k]+=b[k];
    }
}

template<class Ta,class Tb>
__host__ void vadd_ip(size_t n, 
                            Ta * __restrict__ const a, 
                      const Tb * __restrict__ const b) {
    vadd_ip_k<<<(n+1023)/1024,1024>>>(n,a,b);
}

extern "C" void vadd_vf32_vi16_ip_gpu(size_t n, float *a,const int16_t* b) {
    vadd_ip<float,int16_t>(n,a,b);
}
extern "C" void vadd_vf32_vf32_ip_gpu(size_t n, float *a,const float* b) {
    vadd_ip<float,float>(n,a,b);
}

// vsub_ip

template<class Ta,class Tb>
__global__ void vsub_ip_k(
    int n,
    Ta * __restrict__ const a,
    const Tb * __restrict__ const b) 
{
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
        k<n;
        k+=gridDim.x*blockDim.x) 
    {
        a[k]-=Ta(b[k]);
    }
}

template<class Ta,class Tb>
__host__ void vsub_ip(size_t n,
                            Ta * __restrict__ const a, 
                      const Tb * __restrict__ const b) 
{
    vsub_ip_k<<<(n+1023)/1024,1024>>>(n,a,b);
}

extern "C" void vsub_vf32_vi16_ip_gpu(size_t n, float *a,const int16_t* b) {
    vsub_ip(n,a,b);
}
extern "C" void vsub_vf32_vf32_ip_gpu(size_t n, float *a,const float* b) {
    vsub_ip(n,a,b);
}

// vcpy_ip (really a reinterpret cast)

template<class Ta,class Tb>
__global__ void vcpy_ip_k(
    int n,
    Ta * __restrict__ const a,
    const Tb * __restrict__ const b) {
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
        k<n;
        k+=gridDim.x*blockDim.x) 
    {
        a[k]=b[k];
    }
}

template<class Ta,class Tb>
__host__ void vcpy_ip(size_t n,
                      Ta * __restrict__ const a,
                      const Tb * __restrict__ const b) {
    vcpy_ip_k<<<(n+1023)/1024,1024>>>(n,a,b);
}

extern "C" void vcpy_vf32_vi16_ip_gpu(size_t n,float *a,const int16_t* b) {
    vcpy_ip(n,a,b);
}
extern "C" void vcpy_vf32_vf32_ip_gpu(size_t n,float *a,const float* b) {
    vcpy_ip(n,a,b);   
}

// vaddsq_ip

template<class Ta,class Tb>
__global__ void vaddsq_ip_k(
    int n,
    Ta * const __restrict__ a,
    const Tb * const __restrict__ b) 
{
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
        k<n;
        k+=gridDim.x*blockDim.x) 
    {
        const float v=b[k];        
        a[k]+=v*v;
    }
}

template<class Ta,class Tb>
__host__ void vaddsq_ip(size_t n, 
                      Ta * __restrict__ const a, 
                      const Tb * __restrict__ const b) {
    vaddsq_ip_k<Ta,Tb><<<(n+1023)/1024,1024>>>(n,a,b);
}

extern "C" void vaddsq_vf32_vi16_ip_gpu(size_t n, float *a,const int16_t* b) {
    vaddsq_ip(n,a,b);
}
extern "C" void vaddsq_vf32_vf32_ip_gpu(size_t n, float *a,const float* b) {
    vaddsq_ip(n,a,b);
}

// vdiv

template<class Tz,class Tx,class Ty>
__global__ void vdiv_k(
    int n,
    Tz * __restrict__ const z,
    const Tx * __restrict__ const x,
    const Ty y) 
{
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
        k<n;
        k+=gridDim.x*blockDim.x) {
        z[k]=x[k]/y;
    }
}

template<class Tz,class Tx,class Ty>
__host__ void vdiv(size_t n, 
                              Tz * __restrict__ const z, 
                        const Tx * __restrict__ const x,
                        const Ty y) {
    vdiv_k<Tz,Tx,Ty><<<(n+1023)/1024,1024>>>(n,z,x,y);
}

extern "C" void vdiv_vf32_vf32_f32_gpu(size_t n, float *z,const float *x, float y) {
    vdiv<float,float,float>(n,z,x,y);
}

// vmul 

template<class Tz,class Tx>
__global__ void vmul_k(
	int n,
	Tz *z,
	const Tx *x,
	const Tx y) 
{
	for(int k=blockIdx.x*blockDim.x+threadIdx.x;
		k<n;
		k+=gridDim.x*blockDim.x) {
		z[k]=x[k]*y;
	}
}

template<class Tz,class Tx>
__host__ void vmul(size_t n, 
				   Tz *z, 
				   const Tx *x,
				   const Tx y) {
	vmul_k<Tz,Tx><<<(n+1023)/1024,1024>>>(n,z,x,y);
}

extern "C" void vmul_vf32_vf32_f32_gpu(size_t n, float *z,const float *x, float y) {
	vmul<float,float>(n,z,x,y);
}


// complex multiplication
// Z,X,Y are n complex elements (eg float2)
// TODO: optimize to reduce cache loads

template<class Tz,class Tx>
__global__ void vcmul_k(int n, Tz *  const z, 
                        const Tx *  const x,
                        const Tx *  const y)
{
	for(int k=blockIdx.x*blockDim.x+threadIdx.x;
		k<n;
		k+=gridDim.x*blockDim.x) 
	{
		const auto
			x0=x[2*k],
			x1=x[2*k+1],
			y0=y[2*k],
			y1=y[2*k+1];

		z[2*k]  =x0*y0+x1*y1;
		z[2*k+1]=x1*y0-x0*y1;
	}
}

template<class Tz,class Tx>
__host__ void vcmul(size_t n, 
				   Tz *  const z, 
				   const Tx *  const x,
				   const Tx *  const y) {
	vcmul_k<Tz,Tx><<<(n+1023)/1024,1024>>>(n,z,x,y);
}

extern "C" void vcmul_vf32_vf32_vf32_gpu(size_t n, float *z,const float *x, const float *y) {
	vcmul(n,z,x,y);
} 

// phase normalization

template<class Tz>
__global__ void vcnorm_ip_k(int n,Tz * z)
{
	for(int k=blockIdx.x*blockDim.x+threadIdx.x;
		k<n;
		k+=gridDim.x*blockDim.x)
	{		
		const Tz
			z0=z[2*k],
			z1=z[2*k+1];
		const float norm=sqrtf(z0*z0+z1*z1+1.0e-5f);

		z[2*k]  =z0/norm;
		z[2*k+1]=z1/norm;
	}
}

template<class Tz>
__host__ void vcnorm_ip(size_t n, Tz * z) {
	vcnorm_ip_k<Tz><<<n/1024,1024>>>(n,z);
}

extern "C" void vcnorm_ip_gpu(size_t n, float *z) {
	vcnorm_ip(n,z);
} 

// normalize to range

template<class Tz,class Tx>
__global__ void vnorm_to_range_ip_k(int n,Tz *z,const Tx *mn,const Tx *mx,Tx low,Tx range) {
    const Tx r(range),l(low);
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
        k<n;
        k+=gridDim.x*blockDim.x)
    {
        z[k]=Tz(r*(Tx(z[k])-mn[k])/(mx[k]-mn[k]+1)+l);
    }
}

template<class Tz,class Tx>
__host__ void vnorm_to_range_ip(size_t n,Tz *z,const Tx *mn,const Tx *mx,Tx low,Tx range) {
    vnorm_to_range_ip_k<Tz,Tx><<<n/1024,1024>>>(n,z,mn,mx,low,range);
}

extern "C" void vnorm_to_range_ip_i16_f32_f32_gpu(size_t n,int16_t *z,const float *mn,const float *mx) {
    vnorm_to_range_ip<int16_t,float>(n,z,mn,mx,SHRT_MIN,65535);
}

// standardize

template<class Tz,class Tx>
__global__ void vstandardize_ip_k(int n,Tz *z,const Tx *offset,const Tx *norm,Tx low,Tx range) {    
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
        k<n;
        k+=gridDim.x*blockDim.x)
    {
        const Tx v=(z[k]-offset[k])/norm[k];
        z[k]=Tz(range*v+low);
    }
}

template<class Tz,class Tx>
__host__ void vstandardize_ip(size_t n,Tz *z,const Tx *offset,const Tx *norm,Tx low,Tx range) {
    vstandardize_ip_k<Tz,Tx><<<(n+1023)/1024,1024>>>(n,z,offset,norm,low,range);
}

extern "C" void vstandardize_ip_i16_f32_f32_gpu(size_t n,int16_t *z,const float *offset,const float *norm) {
    //vstandardize_ip<int16_t,float>(n,z,offset,norm,SHRT_MIN,65535);
    vstandardize_ip<int16_t,float>(n,z,offset,norm,0,32767/100);
}

// -- out-of-place

__global__ void vstandardize_gpu_k(size_t n,
                                 float * __restrict__ dst,
                                 const int16_t* __restrict__ src,
                                 const float* __restrict__ mean,
                                 const float* __restrict__ stdev ) 
{
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
        k<n;
        k+=gridDim.x*blockDim.x)
    {
        dst[k]=(float(src[k])-mean[k])/stdev[k];
    }
}

extern "C" void vstandardize_gpu(size_t n,float *dst,const int16_t *src, const float *mean, const float* stdev ) {
    if(mean && stdev)
        vstandardize_gpu_k<<<(n+1023)/1024,1024>>>(n,dst,src,mean,stdev);
    else
        vcpy_vf32_vi16_ip_gpu(n,dst,src);
}