#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
    void vadd_vf32_vi16_ip_gpu(size_t n,float *a,const int16_t* b);
    void vadd_vf32_vf32_ip_gpu(size_t n,float *a,const float* b);

    void vsub_vf32_vi16_ip_gpu(size_t n,float *a,const int16_t* b);
    void vsub_vf32_vf32_ip_gpu(size_t n,float *a,const float* b);

    void vcpy_vf32_vi16_ip_gpu(size_t n,float *a,const int16_t* b);
    void vcpy_vf32_vf32_ip_gpu(size_t n,float *a,const float* b);

    void vaddsq_vf32_vi16_ip_gpu(size_t n,float *a,const int16_t* b);
    void vaddsq_vf32_vf32_ip_gpu(size_t n,float *a,const float* b);

    void vdiv_vf32_vf32_f32_gpu(size_t n,float *z,const float *x,float y);

	void vmul_vf32_vf32_f32_gpu(size_t n,float *z,const float *x,float y);

	void vcmul_vf32_vf32_vf32_gpu(size_t n,float *z,const float *x,const float *y);

	void vcnorm_ip_gpu(size_t n,float *z);

    void vnorm_to_range_ip_i16_f32_f32_gpu(size_t n,int16_t *z,const float *mn,const float *mx);

    void vstandardize_ip_i16_f32_f32_gpu(size_t n,int16_t *z,const float *offset,const float *norm);

    // if mean or stdev are NULL, this will just casting copy from src to dst.
    void vstandardize_gpu(size_t n,float *dst,const int16_t *src,const float *mean,const float* stdev);
#ifdef __cplusplus
}
#endif
 