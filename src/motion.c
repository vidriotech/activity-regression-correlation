#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <cufft.h>
#include <vops.h>
#include "nd.h"
#include "maximum.h"
#include <string.h>


#define LOG(str) puts(str);
#define CHECK(e) do{if(!((e))) {LOG(#e); abort(); goto Error;}}while(0)
#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        printf("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        abort(); \
        goto Error; \
    }\
}while(0)


#define CUFFTCHK(expr) do{ \
    const char* estr=cufft_error_string(expr); \
    if(estr) { \
        printf("%s\n\t%s\n",#expr,estr); \
        abort(); \
    }\
}while(0)

static const char* cufft_error_string(enum cufftResult_t ecode) {
	switch(ecode){
		case CUFFT_SUCCESS: return 0;
#define CASE(e) case e: return #e
		CASE(CUFFT_INVALID_PLAN);
		CASE(CUFFT_ALLOC_FAILED);
		CASE(CUFFT_INVALID_TYPE);
		CASE(CUFFT_INVALID_VALUE);
		CASE(CUFFT_INTERNAL_ERROR);
		CASE(CUFFT_EXEC_FAILED);
		CASE(CUFFT_SETUP_FAILED);
		CASE(CUFFT_INVALID_SIZE);
		CASE(CUFFT_UNALIGNED_DATA);
		CASE(CUFFT_INCOMPLETE_PARAMETER_LIST);
		CASE(CUFFT_INVALID_DEVICE);
		CASE(CUFFT_PARSE_ERROR);
		CASE(CUFFT_NO_WORKSPACE);
		CASE(CUFFT_NOT_IMPLEMENTED);
		CASE(CUFFT_LICENSE_ERROR);
//		CASE(CUFFT_NOT_SUPPORTED);
		default: return "Unknown cufft error code.";
#undef CASE
	}
}



static size_t nbytes(const struct nd shape) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return shape.strides[shape.ndim]*bpp[shape.type];
}

// shape for batched transforms
static struct nd batched_fft_shape(struct nd src_shape) {
    struct nd out={0};
    out.type=nd_f32;
    out.ndim=4;
    out.dims[0]=2; // complex numbers
    out.dims[1]=src_shape.dims[1];
    out.dims[2]=(src_shape.dims[0]/2)+1;
    out.dims[3]=(src_shape.ndim>=3)?src_shape.dims[2]:1;
    out.strides[0]=1;
    out.strides[1]=out.dims[0];
    out.strides[2]=out.dims[1]*out.strides[1];
    out.strides[3]=out.dims[2]*out.strides[2];
    out.strides[4]=out.dims[3]*out.strides[3];
Error:
    return out;
}

#if 1
#define save_gpu(...)
static int call_count=0; // used for debug output in the "save_gpu" block
#else
    #include "tiff.write.h"
    #include <config.h>
    #include <stdarg.h>

	static int call_count=0;

    static int save_gpu(void *p,struct nd shape,const char* filename_fmt,...) {
        void *out=0;
        CHECK(out=malloc(nbytes(shape)));
        CUDACHK(cudaMemcpy(out,p,nbytes(shape),cudaMemcpyDeviceToHost));
        {
            char fname[1024]={0},buf[1024]={0};
            va_list args;
            va_start(args,filename_fmt);
            vsprintf_s(buf,sizeof(buf),filename_fmt,args);
            va_end(args);
            sprintf_s(fname,sizeof(fname),"%s/%d-%s",Configuration.output.folder,call_count,buf);
            switch(shape.type)
            {
                case nd_i16: write_tiff_i16(fname,shape.ndim,shape.dims,(int16_t*)out); break;
                case nd_f32: write_tiff_f32(fname,shape.ndim,shape.dims,(float*)out); break;
                default:
                    goto Error;
            }
        }
        free(out);
        return 1;
    Error:
        return 0;
    }
#endif

static void restride(struct nd* shape) {
    shape->strides[0]=1;
    for(int i=1;i<=shape->ndim;++i)
        shape->strides[i]=shape->strides[i-1]*shape->dims[i-1];
}

static struct vol* make_displacements_1(int ntimepoints,int ndims) {
    struct vol *v;
    struct nd shape={0};
    shape.ndim=3;
    shape.type=nd_f32;
    shape.dims[0]=2;
    shape.dims[1]=ntimepoints;
    shape.dims[2]=ndims;
    restride(&shape);
    CHECK(v=malloc(sizeof(struct vol)+nbytes(shape)));
    v->shape=shape;
    return v;
Error:
    return 0;
}

static struct vol* make_displacements_2(int ntimepoints) {
    struct vol *v;
    struct nd shape={0};
    shape.ndim=2;
    shape.type=nd_f32;
    shape.dims[0]=3;
    shape.dims[1]=ntimepoints;
    restride(&shape);
    CHECK(v=malloc(sizeof(struct vol)+nbytes(shape)));
    v->shape=shape;
    return v;
Error:
    return 0;
}

static int measure_displacements__centroid(void* gpu_ptr,struct nd shape, int idim, const int search_radius_px, struct vol* out) {
    float *p=0;
    CHECK(p=(float*)malloc(nbytes(shape)));
    CUDACHK(cudaMemcpy(p,gpu_ptr,nbytes(shape),cudaMemcpyDeviceToHost));
    for(unsigned t=0;t<shape.dims[2];++t){
        float sx=0.0f,sy=0.0f,norm=0.0f;
        for(int i=-search_radius_px;i<=search_radius_px;++i){
            for(int j=-search_radius_px;j<=search_radius_px;++j){
                const int
                    ix=j%shape.dims[0],
                    iy=i%shape.dims[1];
                const float v=p[ix*shape.strides[0]+iy*shape.strides[1]+t*shape.strides[2]];
                sx+=v*j;
                sy+=v*i;
                norm+=v;
            }
        }
        float *r
            =(float*)out->data
            +out->shape.strides[1]*t
            +out->shape.strides[2]*idim;
        r[0]=sx/norm;
        r[1]=sy/norm;
    }
    free(p);
    return 1;
Error:
    free(p);
    return 0;
}


static int measure_displacements__meanshift(void* gpu_ptr,struct nd shape, int idim, const int search_radius_px, struct vol* out) {
    float *p=0;
    CHECK(p=(float*)malloc(nbytes(shape)));
    CUDACHK(cudaMemcpy(p,gpu_ptr,nbytes(shape),cudaMemcpyDeviceToHost));
    for(unsigned t=0;t<shape.dims[2];++t){
        float *r
            =(float*)out->data
            +out->shape.strides[1]*t
            +out->shape.strides[2]*idim;
        r[0]=r[1]=0.0f;
        for(int k=0;k<5;++k) {
            float sx=0.0f,sy=0.0f,norm=0.0f;
            for(int i=-search_radius_px;i<=search_radius_px;++i){
                for(int j=-search_radius_px;j<=search_radius_px;++j){
                    const int
                        ix=(j+(int)r[0])%shape.dims[0],
                        iy=(i+(int)r[1])%shape.dims[1];
                    const float v=p[ix*shape.strides[0]+iy*shape.strides[1]+t*shape.strides[2]];
                    sx+=v*(j+(int)r[0]);
                    sy+=v*(i+(int)r[1]);
                    norm+=v;
                }
            }
            r[0]=sx/norm;
            r[1]=sy/norm;
        }
    }
    free(p);
    return 1;
Error:
    free(p);
    return 0;
}

#include <float.h>
static int measure_displacements__max(void* gpu_ptr,struct nd shape, int idim, const int search_radius_px, struct vol* out) {
    float *p=0;
    CHECK(p=(float*)malloc(nbytes(shape)));
    CUDACHK(cudaMemcpy(p,gpu_ptr,nbytes(shape),cudaMemcpyDeviceToHost));
    for(unsigned t=0;t<shape.dims[2];++t){
        int sx=0,sy=0;
        float mx=-FLT_MAX;
#if 0 // use search radius
        for(int i=-search_radius_px;i<=search_radius_px;++i){
            for(int j=-search_radius_px;j<=search_radius_px;++j){
                const int
                    ix=j%shape.dims[0],
                    iy=i%shape.dims[1];
                const float v=p[ix*shape.strides[0]+iy*shape.strides[1]+t*shape.strides[2]];
                if(v>mx) {
                    mx=v;
                    sx=j;
                    sy=i;
                }
            }
        }
#else // search whole image
        for(int iy=0;iy<shape.dims[1];++iy){
            for(int ix=0;ix<shape.dims[0];++ix){
                const float v=p[ix*shape.strides[0]+iy*shape.strides[1]+t*shape.strides[2]];
                if(v>mx) {
                    mx=v;
                    sx=ix;
                    sy=iy;
                }
            }
        }

#endif
        float *r
            =(float*)out->data
            +out->shape.strides[1]*t
            +out->shape.strides[2]*idim;
        int hx=shape.dims[0]/2,
            hy=shape.dims[1]/2;
        r[0]=((sx+hx)%(int)shape.dims[0])-hx; // wrap 0-nx to -nx/2..nx/2
        r[1]=((sy+hy)%(int)shape.dims[1])-hy;
    }
    free(p);
    return 1;
Error:
    free(p);
    return 0;
}

//

struct Motion {
	void *fft[3]; // fft's of the target max projections
	cufftHandle forward[3],inverse[3];
	int batch_size;
	struct nd shape;
	void(*onerror)(const char* file,int line,const char* function,const char *fmt,...);
};

void Motion_CleanupContext(struct Motion *ctx) {
    for(int i=0;i<3;++i) {
        cudaFree(ctx->fft[i]);
        CUFFTCHK(cufftDestroy(ctx->forward[i]));
		CUFFTCHK(cufftDestroy(ctx->inverse[i]));
    }
}

/// \param [in] shape	Shape of the 3d target array.
///
/// Caller must call Motion_CleanupContext() on the returned pointer, and then
/// deallocate it.  The returned pointer points to the base address
/// of memory allocated with the caller-supplied alloc() function.
struct Motion *Motion_CreateContext(
	void* target,
	struct nd shape,
	void* (*alloc)(size_t nbytes),
	void (*onerror)(const char* file, int line, const char* function, const char *fmt, ...))
{
	struct Motion* ctx=(struct Motion*)alloc(sizeof(*ctx));
	if(!ctx) return 0;
	memset(ctx,0,sizeof(*ctx));
	ctx->shape=shape;
	ctx->onerror=onerror;

	for(unsigned IDIM=0;IDIM<3;++IDIM) {
		struct nd dst_shape=get_output_shape__maximum(shape,IDIM);
		void *proj;

		dst_shape.type=nd_f32;
		struct nd fft_shape=batched_fft_shape(dst_shape);
		struct nd ref_shape=fft_shape; // the reference is one timepoint from the batched fft dataset
		ref_shape.ndim=3;
		CUDACHK(cudaMalloc(&ctx->fft[IDIM],2*nbytes(ref_shape)));

		{
			int dims[2]={dst_shape.dims[1],dst_shape.dims[0]};
			ctx->batch_size=dst_shape.dims[2];
			CUFFTCHK(cufftPlanMany(&ctx->forward[IDIM],2,dims,NULL,0,0,NULL,0,0,CUFFT_R2C,dst_shape.dims[2]));
			CUFFTCHK(cufftPlanMany(&ctx->inverse[IDIM],2,dims,NULL,0,0,NULL,0,0,CUFFT_C2R,dst_shape.dims[2]));
		}

		CUDACHK(cudaMalloc(&proj,nbytes(dst_shape)));
		CHECK(maximum(proj,dst_shape,target,shape,IDIM));
		save_gpu(proj,dst_shape,"reference-max4d.%d.tif",IDIM);
		CUDACHK(cudaPeekAtLastError());
		CUDACHK(cudaDeviceSynchronize());
		{
			cufftHandle plan;
			int dims[2]={dst_shape.dims[1],dst_shape.dims[0]};
			CUFFTCHK(cufftPlan2d(&plan,dims[0],dims[1],CUFFT_R2C));
			//CUDACHK(cufftPlanMany(&plan,2,dims,NULL,0,0,NULL,0,0,CUFFT_R2C,1));
			CUFFTCHK(cufftExecR2C(plan,proj,ctx->fft[IDIM]));
			CUDACHK(cudaPeekAtLastError());
			CUDACHK(cudaDeviceSynchronize());

			CUFFTCHK(cufftDestroy(plan));
		}
		CUFFTCHK(cudaFree(proj));
	}

	return ctx;
Error:
	return 0;
}

/// Registers a 3d+t array of source volumes to the target volume.  Produces
/// an array of displacements for each time point.  "time" is the last dimension.
///
/// \param [in] shape    Shape of the 4d source array.  The first 3 dimensions should
///                      be the same as the target volume.
/// \param [in] source   Pointer to the buffer holding the 3d+t volume of
///                      data to register.  Must be allocated on the gpu.
///
/// \returns a 2d array of size (3,N) where N is shape.dims[3].  Caller is
///          responsible for de-allocating the returned pointer.
///
/// \todo FIXME: returned value is allocated with malloc.  Should use user-supplied allocater.
struct vol *Motion_Estimate(struct Motion *ctx,void *src_gpu, struct nd shape) {
    struct dev{ void *dst,*fft; } dev={0};
    struct vol* deltas_raw  =make_displacements_1(shape.dims[3],3);
    struct vol* deltas_final=make_displacements_2(shape.dims[3]);

	call_count++; // Used for debug output.

    for(unsigned IDIM=0;IDIM<3;++IDIM)
    {
        cufftHandle forward=ctx->forward[IDIM],
                    inverse=ctx->inverse[IDIM];


        struct nd dst_shape=get_output_shape__maximum(shape,IDIM);
        dst_shape.type=nd_f32;
        struct nd fft_shape=batched_fft_shape(dst_shape);

		if(ctx->batch_size!=dst_shape.dims[2]) {
			const int dims[2]={dst_shape.dims[1],dst_shape.dims[0]};
			CUDACHK(cufftPlanMany(&forward,2,dims,NULL,0,0,NULL,0,0,CUFFT_R2C,dst_shape.dims[2]));
			CUDACHK(cufftPlanMany(&inverse,2,dims,NULL,0,0,NULL,0,0,CUFFT_C2R,dst_shape.dims[2]));
		}


        CUDACHK(cudaMalloc(&dev.dst,nbytes(dst_shape)));
        CUDACHK(cudaMalloc(&dev.fft,nbytes(fft_shape)));

        // max projection
        CHECK(maximum(dev.dst,dst_shape,src_gpu,shape,IDIM));
        save_gpu(dev.dst,dst_shape,"max4d.%d.tif",IDIM);
        {
            CUDACHK(cufftExecR2C(forward,dev.dst,dev.fft));

            for(unsigned i=0;i<fft_shape.dims[3];++i){
                vcmul_vf32_vf32_vf32_gpu(
                    fft_shape.strides[3]/2,
                    (float*)dev.fft+i*fft_shape.strides[3],
                    (float*)dev.fft+i*fft_shape.strides[3],
                    (float*)ctx->fft[IDIM]);
            }
        	vcnorm_ip_gpu(fft_shape.strides[4]/2,dev.fft); // phase correlation

        	CUDACHK(cufftExecC2R(inverse,dev.fft,dev.dst)); // in-place inverse fft
            save_gpu(dev.dst,dst_shape,"xcorr.%d.tif",IDIM);

            //CHECK(measure_displacements__centroid(dev.dst,dst_shape,IDIM,10,deltas_raw));
            CHECK(measure_displacements__max(dev.dst,dst_shape,IDIM,
                  (dst_shape.dims[IDIM]-1)/2, // search radius...ignored?
                  deltas_raw));
            //CHECK(measure_displacements__meanshift(dev.dst,dst_shape,IDIM,5,deltas_raw));
        }

		if(ctx->batch_size!=dst_shape.dims[2]){
			cufftDestroy(forward);
			cufftDestroy(inverse);
		}

        cudaFree(dev.fft);
        cudaFree(dev.dst);
    } // end loop over each projection dimension

    #if 0
        {
            // save raw deltas
            const char fname[1024]={0};
            sprintf_s(fname,sizeof(fname),
                      "delta.raw.%dx%dx%d.f32",
                      deltas_raw->shape.dims[0],
                      deltas_raw->shape.dims[1],
                      deltas_raw->shape.dims[2]);
            FILE *fp=fopen(fname,"wb");
            fwrite(deltas_raw->data,nbytes(deltas_raw->shape),1,fp);
            fclose(fp);
        }
    #endif

        {
            // aggregate translations
            for(int t=0;t<deltas_final->shape.dims[1];++t) {
                float
                    *r=(float*)deltas_final->data+t*deltas_final->shape.strides[1];
                const float
                    *r_xp=(float*)deltas_raw->data+t*deltas_raw->shape.strides[1],
                    *r_yp=(float*)deltas_raw->data+t*deltas_raw->shape.strides[1]+1*deltas_raw->shape.strides[2],
                    *r_zp=(float*)deltas_raw->data+t*deltas_raw->shape.strides[1]+2*deltas_raw->shape.strides[2];
                r[0]=-r_zp[0]; // trust the z projection the most for (x,y) displacements
                r[1]=-r_zp[1];
                r[2]=-round(0.5f*(r_xp[1]+r_yp[1])); // round(average of z's) from x,y projections.
            }
        }
        free(deltas_raw);
        return deltas_final;
    Error:
        return 0;
}


// TODO: remove motion() function in favor of newer interface
// TODO: fix allocator pattern in newer interface
