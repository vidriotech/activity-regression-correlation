#include "resampler.h"
#include <host_defines.h>
#include <cuda_runtime.h>

#define log(...) resampler->onerror(__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)

#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        log("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        goto Error; \
        }\
}while(0)

struct Resampler {
    void *tmp;
    void(*onerror)(const char *file,int line,const char *function,const char *fmt,...);
};

static int dot_int_i64_f32(size_t n,const int64_t * __restrict__ a,const float* __restrict__ b) {
    int out=0;
    for(size_t i=0;i<n;++i)
        out+=(int)(a[i]*b[i]);
    return out;
}

template<class Ta,class Tb>
__global__ void vcpy_k(
    int n,
    Ta * __restrict__ const a,
    const Tb * __restrict__ const b) {
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
        k<n;
        k+=gridDim.x*blockDim.x) 
    {
        a[k]=b[k];
    }
}

template<class Ta,class Tb>
__host__ void vcpy(size_t n,
                   Ta * __restrict__ const a,
                   const Tb * __restrict__ const b) {
    vcpy_k<<<(n+1023)/1024,1024>>>(n,a,b);
}

static size_t nbytes(const struct nd shape) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return shape.strides[shape.ndim]*bpp[shape.type];
}

static int maybe_alloc_tmp(
    struct Resampler* resampler,
    const struct nd shape) {
    if(!resampler->tmp) {
        CUDACHK(cudaMalloc(&resampler->tmp,nbytes(shape)));
    }
    return 1;
Error:    
    return 0;
}

// Interface

struct Resampler *CreateTranslationInPlaceResampler(
    void* (*alloc)(size_t nbytes),
    void (*onerror)(const char *file, int line, const char *function, const char *fmt,...)) 
{
    struct Resampler* ctx=(struct Resampler*)alloc(sizeof(*ctx));
    if(!ctx){
        onerror(__FILE__,__LINE__,__FUNCTION__,"Could not allocate memory.");
        goto Error;
    }
    ctx->tmp=0;
    ctx->onerror=onerror;
Error:    
    return ctx;
}

void CleanupTranslationInPlaceResampler(struct Resampler *resampler) {
    if(resampler)
        cudaFree(resampler->tmp);
}

int ResampleInPlace(
    struct Resampler *resampler,
    void *src,struct nd shape,
    const float *dr) 
{
    int offset=dot_int_i64_f32(3,shape.strides,dr);
    const size_t
        od=(offset>0)?offset:0,
        os=(offset<0)?(-offset):0,
        nelem=shape.strides[3]-((offset<0)?(-offset):offset);

	shape.ndim=3; // only work on first three dims
    maybe_alloc_tmp(resampler,shape);
    switch(shape.type){
#define CASE(TID,T) case TID: \
            vcpy(nelem,((T*)resampler->tmp)+od,((T*)src)+os); \
            vcpy(shape.strides[3],(T*)src,(T*)resampler->tmp); \
            break; 
        CASE(nd_u8 ,uint8_t);
        CASE(nd_u16,uint16_t);
        CASE(nd_u32,uint32_t);
        CASE(nd_u64,uint64_t);
        CASE(nd_i8 , int8_t);
        CASE(nd_i16, int16_t);
        CASE(nd_i32, int32_t);
        CASE(nd_i64, int64_t);
        CASE(nd_f32, float);
        CASE(nd_f64, double);
        default:
            resampler->onerror(__FILE__,__LINE__,__FUNCTION__,"Unrecognized type");
            goto Error;
#undef CASE
    }

    return 1;
Error:
    return 0;
}