#define _CRT_SECURE_NO_WARNINGS
#include <nd.h>
#include <tiff.reader.api.h>
#include <stdlib.h>
#include <stdio.h>
#include <correlation.h>
#include <motion.h>
#include <maximum.h> // normalization eneds a time projection
#include <cuda_runtime.h>
#include <vops.h>
#include <stdarg.h>
#include <median.h>
#include "config.h"
#include "tiff.write.h"
#include "tictoc.h"
#include "resampler.h"

#include <sys/stat.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>  // for ProgramName() -- I think this is the only platform-specific dependency

#define STR(X) #X

static void log(const char* fmt,...);

static const char* AUTHOR="Nathan Clack <nathan@vidriotech.com>";

const char* ProgramName();
static void headline() {
    log("%s %s-%s by %s\n",ProgramName(),STR(GIT_TAG),STR(GIT_HASH),AUTHOR);
}
static void usage() {
    headline();
    log("Usage:\n"
        "\tcat <config text> | %s\tRead config from stdin\n"
        "\t%s <config file name>\n"
        "\n\tExample:\n\t\t%s path/to/expt.config\n"
        "\n\tDescription:\n"
        "\n\t\tThe configuration file contains information about where to\n"
        "\t\tread input data, write output, and which parameters should be\n"
        "\t\tused.  The syntax is Lua 5.3 (Lua is a scripting language).\n"
        "\n\tExample configuration file:\n"
        "\n\t\tinput={"
        "\n\t\t    timepoint=\"path/to/input/%%05d.tif\","
        "\n\t\t    stimulus=\"path/to/input/STIM.txt\","
        "\n\t\t    first=1,"
        "\n\t\t    last=350,"
        "\n\t\t    chunk_size=100,"
        "\n\t\t}"
        "\n\t\toutput={"
        "\n\t\t    folder=\"path/to/results\","
        "\n\t\t    correlation=\"r2.tif\","
        "\n\t\t    log=\"log.txt\","
        "\n\t\t    stimdt=\"stimdt.i32\","
        "\n\t\t}"
        "\n\t\tparameters={"
        "\n\t\t    strimulus_threshold=6,"
        "\n\t\t    window=12,"
        "\n\t\t    median_radius=2,"
        "\n\t\t    gpuid=0,"
        "\n\t\t}"
        "\n\t\toptions={"
        "\n\t\t    extract_stim_times_from_images=true,"
        "\n\t\t    write_median=false,"
        "\n\t\t    write_stimdt=false,"
        "\n\t\t    write_motion_vectors=false,"
        "\n\t\t}\n\n"
        ,ProgramName(),ProgramName(),ProgramName());
}

static FILE*  fp_log;
char* backlog=0;
int nbacklog=0;


static void log(const char* fmt,...) {
    va_list ap;
    va_start(ap,fmt);
    vprintf(fmt,ap);
    va_end(ap);
    if(fp_log){
        if(backlog) { // now that the log file is open, flush the backlog
            fputs(backlog,fp_log);
            free(backlog);
            backlog=0;
        }
        va_start(ap,fmt);
        vfprintf(fp_log,fmt,ap);
        va_end(ap);
        fflush(fp_log);
    } else {
        // File hasn't been opened yet, so append to a string buffer
        // 1. render the string
        char buf[1024]={0};
        va_start(ap,fmt);
        vsnprintf(buf,sizeof(buf),fmt,ap);
        va_end(ap);
        // 2. append to a resizable buffer
        size_t a=strlen(buf),
               b=backlog?strlen(backlog):0;
        while(nbacklog<(a+b)) {    // ensure sufficient size
            nbacklog=(int)(1.2*(a+b)+50); // geometrically expand w a little padding
            backlog=(char*)realloc(backlog,nbacklog);
            memset(backlog+b,0,nbacklog-b); // null out the end
        }
        strcat(backlog,buf);
    }
}
static void log_puts(const char* str) {
    log("%s\n",str);
}

static struct fileseries {
  int first, last, step;
  const char* filename_template;
  const char* output_filename;
} DATASET;

#define LOG(str) log_puts(str);
#define CHECK(e) do{if(!((e))) {log("ASSERT FAILED: " #e "\n\tmain(line: %d)\n",__LINE__); goto Error;}}while(0)
#define SICHECK(...) __VA_ARGS__; do{if(h.log) {log("ScanImageTiffReader: %s\n\tmain(line: %d)\n",h.log,__LINE__); goto Error;}}while(0)
#define CUDACHK(expr) do{ \
    cudaError_t e=(expr); \
    if(e!=cudaSuccess) { \
        log("%s\n\t%s\n",#expr,cudaGetErrorString(e)); \
        abort(); \
        goto Error; \
        }\
}while(0)


static void onerror(const char *file,int line,const char *function,const char *fmt,...){
    char buf[1024]={0};
    va_list ap;
    va_start(ap,fmt);
    _vsnprintf(buf,sizeof(buf),fmt,ap);
    va_end(ap);
    log_puts(buf);
}


static size_t nbytes(const struct nd shape) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return shape.strides[shape.ndim]*bpp[shape.type];
}

static void restride(struct nd* shape) {
    shape->strides[0]=1;
    for(int i=1;i<=shape->ndim;++i)
        shape->strides[i]=shape->strides[i-1]*shape->dims[i-1];
}

static void* offset(void * p, const nd shape, int idim, int i) {
    switch(shape.type) {
        #define CASE(tid,T) case tid: return ((T*)p)+shape.strides[idim]*i
        CASE(nd_u8 ,uint8_t);
        CASE(nd_u16,uint16_t);
        CASE(nd_u32,uint32_t);
        CASE(nd_u64,uint64_t);
        CASE(nd_i8 , int8_t);
        CASE(nd_i16, int16_t);
        CASE(nd_i32, int32_t);
        CASE(nd_i64, int64_t);
        CASE(nd_f32,float);
        CASE(nd_f64,double);
        #undef CASE
        default:
            LOG("Unrecognized type.\n");
    }
    return 0;
}

static const void* const_offset(const void * p, const nd shape, int idim, int i) {
    switch(shape.type) {
#define CASE(tid,T) case tid: return ((T*)p)+shape.strides[idim]*i
        CASE(nd_u8 ,uint8_t);
        CASE(nd_u16,uint16_t);
        CASE(nd_u32,uint32_t);
        CASE(nd_u64,uint64_t);
        CASE(nd_i8 , int8_t);
        CASE(nd_i16, int16_t);
        CASE(nd_i32, int32_t);
        CASE(nd_i64, int64_t);
        CASE(nd_f32,float);
        CASE(nd_f64,double);
#undef CASE
        default:
            LOG("Unrecognized type.\n");
    }
    return 0;
}

static struct vol* load_file_series(const struct fileseries t) {
    char fname[1024]={0};
    struct ScanImageTiffReader h={0};
    struct nd shape={0};
    struct vol *vol=0;
    size_t st=0; // the number of bytes loaded from an individual file

    // can use the timepoint up to 4 times in the filename pattern
    sprintf_s(fname,sizeof(fname),t.filename_template,t.first,t.first,t.first,t.first);
    SICHECK(h=ScanImageTiffReader_Open(fname));
    SICHECK(shape=ScanImageTiffReader_GetShape(&h));
    shape.dims[shape.ndim]=(t.last-t.first+1)/t.step;
    shape.strides[shape.ndim+1]=shape.dims[shape.ndim]*shape.strides[shape.ndim];
    st=nbytes(shape);
    shape.ndim+=(t.last!=t.first); // only increment if file series adds a dimension
    CHECK(vol=(struct vol*)malloc(sizeof(*vol)+nbytes(shape)));
    vol->shape=shape;

    // read data from first file since we've already got it open
    SICHECK(ScanImageTiffReader_GetData(&h,vol->data,nbytes(shape)));
    SICHECK(ScanImageTiffReader_Close(&h));

    char *cur=vol->data+st;
    for(int i=t.first+t.step;i<=t.last;i+=t.step) {
        // can use the timepoint up to 4 times in the filename pattern
        sprintf_s(fname,sizeof(fname),t.filename_template,i,i,i,i);
        SICHECK(h=ScanImageTiffReader_Open(fname));
        SICHECK(ScanImageTiffReader_GetData(&h,cur,st));
        SICHECK(ScanImageTiffReader_Close(&h));
        cur+=st;
    }
    CHECK(shape.ndim==4);
    return vol;
Error:
    log("\t%s\n",fname);
    return 0;
}

static struct vol* load_single_file(const struct fileseries t) {
    char fname[1024]={0};
    struct ScanImageTiffReader h={0};
    struct nd shape={0};
    struct vol *vol=0;
    SICHECK(h=ScanImageTiffReader_Open(t.filename_template));
    SICHECK(shape=ScanImageTiffReader_GetShape(&h));
    CHECK(vol=(struct vol*)malloc(sizeof(*vol)+nbytes(shape)));
    SICHECK(ScanImageTiffReader_GetData(&h,vol->data,nbytes(shape)));
    SICHECK(ScanImageTiffReader_Close(&h));

    // reshape
    CHECK(shape.ndim==3);
    {
        int ntimepoints = t.last-t.first+1;
        int nz = shape.dims[2]/ntimepoints;
        CHECK(nz==shape.dims[2]/(float)ntimepoints); /* want shape.dims[2] to be divisible by ntimepoints */
        shape.ndim=4;
        shape.dims[2]=nz;
        shape.dims[3]=ntimepoints;
        restride(&shape);
    }

    CHECK(shape.ndim==4);
    vol->shape=shape;
    return vol;
Error:
    log("\t%s\n",t.filename_template);
    return 0;
}

static struct vol* load(const struct fileseries t) {
    if(Configuration.options.use_single_file)
        return load_single_file(t);
    else
        return load_file_series(t);
}

static size_t size(const struct vol* v,int idim) {
    return v->shape.dims[idim];
}

static size_t bytestride(const struct vol* v, int idim) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return v->shape.strides[idim]*bpp[v->shape.type];
}

static struct nd subvol(const struct vol* v, int idim) {
    struct nd shape=v->shape;
    shape.ndim=idim;
    return shape;
}

static float mean(const struct nd* shape,char* data) {
    CHECK(shape->type==nd_i16);
    int16_t
        *v  =(int16_t*)data,
        *end=v+shape->strides[2];
    float acc=0.0f;
    while(v<end) acc+=*v++;
    return acc/(float)shape->strides[2];
Error:
    return 0.0f;
}

static int32_t vget_i32(struct vol* v,size_t i) {
    CHECK(v->shape.type=nd_i32);
    int32_t* d=(int32_t*)v->data;
    return d[i];
Error:
    return 0;
}

static const char* ProgramName() {
    char buf[1024]={0};
    static char out[1024]={0};
    // get the fully qualified path name to the program
    DWORD n=GetModuleFileName(GetModuleHandle(0),buf,sizeof(buf));
    // get rid of the path and the extension
    unsigned ext=n;
    while(ext>0 && buf[--ext]!='.');
    if(ext==0) ext=n;
    buf[ext]='\0';
    unsigned pathsep=ext;
    while(pathsep>0 && buf[--pathsep]!='\\');
    memset(out,0,sizeof(out));
    memcpy(out,buf+pathsep+1,ext-pathsep-1);
    return out;
}


static int parseargs(int argc, const char** argv) {
    char *reason="I don't know what happened";
    switch(argc) {
        case 1:{ // Load config from stdin
            static char buf[4096]={0};
            size_t c=0;
            while(fgets(buf+c,sizeof(buf)-c-1,stdin)) {
                c=strlen(buf);
            }
            if(!LoadConfigurationFromString(buf,log)) {
                reason="Problem loading the configuration from stdin";
                goto Error;
            }
        } break;
        default:{ // Load config from file -- pass additional args to the lua script
            if(!LoadConfiguration(argv[1],argc-2,argv+2,log)) {
                reason="Problem loading the configuration file";
                goto Error;
            }
        } break;
    }
    /* Historical note:
    It's a little strange that the DATASET gets populated from the Configuration.
    They're redundant.  Initially, I used command line arguments to populate DATASET,
    but later I moved to using a configuration file that supported more options.
    Rather than rewrite the DATASET dependent stuff, I just populate it here.
    */
    DATASET.step=1;
    DATASET.output_filename=Configuration.output.correlation_filename;
    DATASET.filename_template=Configuration.input.timepoint_filename_pattern;
    DATASET.first=Configuration.input.first_timepoint;
    DATASET.last=Configuration.input.last_timepoint;
    return 1;
Error:
    log("%s\n\n",reason);
    //usage();
    return 0;
}

/**
Allocates and returns the stimdt array by analyzing volumes in the timeseries.
Caller is responsible for free'ing the returned pointer.

    Assumes

        The last plane of each timepoint can be used to detect the stimulus by
        comparing the average intensity to a threshold.

        Input volume has uint16 values.

    Output

        `stimdt` counts the number of frames since the last stimulus.
        Before the first stimulus, timepoints get a dt of -1.
*/
static struct vol* extract_stim_dt(struct vol *timeseries,int thresh) {
    const size_t n=timeseries->shape.dims[3];
    struct vol *stimdt=(struct vol*)malloc(n*sizeof(int32_t)+sizeof(*stimdt));
    stimdt->shape.ndim=1;
    stimdt->shape.type=nd_i32;
    stimdt->shape.dims[0]=n;
    stimdt->shape.strides[0]=1;
    stimdt->shape.strides[0]=n;
    int32_t *dt=(int32_t*)stimdt->data;

    const size_t iplane=size(timeseries,2)-1;
    const size_t lastplane_offset=iplane*bytestride(timeseries,2);
    const size_t timepoint_offset=bytestride(timeseries,3);
    char* cur=timeseries->data+lastplane_offset;
    struct nd shape=subvol(timeseries,3);
    int counter=-1;
    for(size_t i=0;i<timeseries->shape.dims[3];++i,++dt) {
        float avg=mean(&shape,cur+i*timepoint_offset);
        if(avg>thresh)
            counter=0;
        *dt=counter;
        if(counter>=0)
            ++counter;
    }
    return stimdt;
}

/**
Allocates and returns the stimdt array loaded from a file.
Caller is responsible for free'ing the returned pointer.

Input stimulus file

    A raw file with 1 byte for each timepoint in the timeseries.
    If a stimulus occured at time t, then byte t should be
    non-zero.  Otherwise, the byte should be zero.

Output

    `stimdt` counts the number of frames since the last stimulus.
    Before the first stimulus, timepoints get a dt of -1.
*/
static struct vol* load_stimdt_from_binary(struct vol *timeseries) {
    const size_t n=timeseries->shape.dims[3];
    struct vol *stimdt=(struct vol*)malloc(n*sizeof(int32_t)+sizeof(*stimdt));
    char *stim=(char*)malloc(n);
    stimdt->shape.ndim=1;
    stimdt->shape.type=nd_i32;
    stimdt->shape.dims[0]=n;
    stimdt->shape.strides[0]=1;
    stimdt->shape.strides[0]=n;

    /* read stimulus file */
    {
        FILE *fp=0;
        if(!strlen(Configuration.input.stimulus_filename))
        fp=fopen(Configuration.input.stimulus_filename,"rb");
        if(!fp) {
            log("Could not open stimulus file for reading: %s\n",Configuration.input.stimulus_filename);
            goto Error;
        }

        fread(stim,1,n,fp);
        fclose(fp);
    }

    /* convert from stim to stimdt */
    {
        int32_t *dt=(int32_t*)stimdt->data;
        int32_t counter=-1;
        for(size_t i=0;i<n;++i,++dt){
            if(stim[i])
                counter=0;
            *dt=counter;
            if(counter>=0)
                ++counter;
        }
    }
    free(stim);

    return stimdt;
Error:
    return 0;
}

/**
Allocates and returns the stimdt array loaded from a file.
Caller is responsible for free'ing the returned pointer.

Input stimulus file

    A text file with whitespace_-delimeted list of integers.  Each integer
    should correspond to a timepoint when a stimulus occured.

Output

    `stimdt` counts the number of frames since the last stimulus.
    Before the first stimulus, timepoints get a dt of -1.
*/
static struct vol* load_stimdt_from_textfile() {
    const size_t n=Configuration.input.last_timepoint-Configuration.input.first_timepoint+1;
    struct vol *stimdt=(struct vol*)malloc(n*sizeof(int32_t)+sizeof(*stimdt));
    char *txt=0;
    int ntxt=0;
    stimdt->shape.ndim=1;
    stimdt->shape.type=nd_i32;
    stimdt->shape.dims[0]=n;
    stimdt->shape.strides[0]=1;
    stimdt->shape.strides[0]=n;

    /* read stimulus file */
    {
        FILE *fp=0;
        const char* filename=Configuration.input.stimulus_filename;
        if(strlen(filename)) {
            struct __stat64 st;
            if(_stat64(filename,&st)) {
                log("Could not open stimulus file: %s\n",filename);
                goto Error;
            }
            if(!(txt=(char*)malloc(st.st_size))) {
                log("Could not allocate buffer for stimulus file: %s\n",filename);
                goto Error;
            }
            ntxt=st.st_size;

            fp=fopen(filename,"r");
            if(!fp) {
                log("Could not open stimulus file for reading: %s\n",filename);
                goto Error;
            }
            fread(txt,1,ntxt,fp);
            fclose(fp);
        }
    }

    /* parse */
    {
        int32_t *dt=(int32_t*)stimdt->data;
        for(int i=0;i<n;++i)
            dt[i]=-1;
        for(char *c=txt;c<(txt+ntxt);) {
            int t;
            char *end=c;
            t=strtol(c,&end,10);
            if(t==0 && (c==end)) { // couldn't parse
                ++c;               // just try to move on
                continue;          // will eventually terminate at end of buffer
            }
            c=end;
            if(errno) {
                LOG("Parsing error for stimulus file.\n");
                goto Error;
            }
            t=t-Configuration.input.first_timepoint; /* time point t will be the t-t0'th loaded frame */
            for(int i=t;i<n;++i)
                if(i>=0) // adjusting for first timepoint could fall off of edge
                    dt[i]=i-t;
        }
    }
    free(txt);

    return stimdt;
Error:
    return 0;
}

static void maybe_write_stimdt(struct vol* stimdt) {
    char fname[1024]={0};
    FILE *fp;
    if(!Configuration.options.write_stimdt)
        return;
    sprintf_s(fname,sizeof(fname),"%s/%s",Configuration.output.folder,Configuration.output.stimdt_filename);
    fp=fopen(fname,"wb");
    fwrite(stimdt->data,sizeof(int32_t),stimdt->shape.dims[0],fp);
    fclose(fp);
}

static float * alloc_correlation(const accumulator* a) {
    return (float*)malloc(a->bytesof_correlation());
}

static void write(const char* fname,const struct nd shape,const float* data) {
    char filename[1024]={0};
    sprintf_s(filename,sizeof(filename),"%s/%s",Configuration.output.folder,fname);
    log("Writing %s\n",fname);
    write_tiff_f32(filename,shape.ndim,shape.dims,data);
}

static void* alloc_gpu(struct nd shape, int ntimepoints) {
    void *p;
    shape.dims[3]=ntimepoints;
    restride(&shape);
    size_t n=nbytes(shape)+1ULL; // HAX: On some computers(?), the npp median filter implementation needs a bit of extra padding (one byte?) to avoid overflow
    log("GPU allocating %f GB\n",1e-9*n);
    CUDACHK(cudaMalloc(&p,n));
    return p;
Error:
    return 0;
}

static void* move_to_gpu(void *gpu, void* buf,struct nd shape,int time_beg,int ntimepoints) {
    shape.dims[3]=ntimepoints;
    restride(&shape);
    CUDACHK(cudaMemcpy(gpu,offset(buf,shape,3,time_beg),nbytes(shape),cudaMemcpyHostToDevice));
    return gpu;
Error:
    return 0;
}

static void* move_back_to_ram(void *buf,void *gpu,struct nd shape,int time_beg,int ntimepoints) {
    shape.dims[3]=ntimepoints;
    restride(&shape);
    CUDACHK(cudaMemcpy(offset(buf,shape,3,time_beg),gpu,nbytes(shape),cudaMemcpyDeviceToHost));
    return buf;
Error:
    return 0;
}

static int v_save_cpu(void *out,struct nd shape,const char* filename_fmt,va_list args) {
    char fname[1024]={0},buf[1024]={0};
    vsprintf_s(buf,sizeof(buf),filename_fmt,args);
    sprintf_s(fname,sizeof(fname),"%s/%s",Configuration.output.folder,buf);
    switch(shape.type)
    {
        case nd_i16: write_tiff_i16(fname,shape.ndim,shape.dims,(int16_t*)out); break;
        case nd_f32: write_tiff_f32(fname,shape.ndim,shape.dims,(float*)out); break;
        default:
            throw "TODO";
    }
    return 1;
Error:
    return 0;
}


static int save_cpu(void *out,struct nd shape,const char* filename_fmt,...) {
    char fname[1024]={0},buf[1024]={0};
    va_list args;
    va_start(args,filename_fmt);
    CHECK(v_save_cpu(out,shape,filename_fmt,args));
    va_end(args);
    return 1;
Error:
    return 0;
}

static int save_gpu(const void *p,struct nd shape,const char* filename_fmt,...) {
    void *out=0;
    CHECK(out=malloc(nbytes(shape)));
    CUDACHK(cudaMemcpy(out,p,nbytes(shape),cudaMemcpyDeviceToHost));
    {
        char fname[1024]={0},buf[1024]={0};
        va_list args;
        va_start(args,filename_fmt);
        CHECK(v_save_cpu(out,shape,filename_fmt,args));
        va_end(args);
    }
    free(out);
    return 1;
Error:
    return 0;
}

static int dot_int_i64_f32(size_t n,int64_t *a,float* b) {
    int out=0;
    for(size_t i=0;i<n;++i)
        out+=(int)(a[i]*b[i]);
    return out;
}

static void maybe_create_output_folder() {
    char *c;
    const char * const end=Configuration.output.folder+strlen(Configuration.output.folder);
    for(c=Configuration.output.folder;c<end;++c) {
        if(*c=='/' || *c== '\\') {
            char t=*c;
            *c=0;
            CreateDirectoryA(Configuration.output.folder,NULL);
            *c=t;
        }
    }
    CreateDirectoryA(Configuration.output.folder,NULL);
}

static void maybe_init_log() {
    char fname[1024]={0};
    sprintf_s(fname,sizeof(fname),"%s/%s",Configuration.output.folder,Configuration.output.log_filename);
    if(!(fp_log=fopen(fname,"w"))) {
        log("Could not open log file for writing.\n");
        return;
    }
}

static const char* win32_get_last_error_string(void) {
    static char buf[1024]={0};
    DWORD ecode=GetLastError();
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,NULL,ecode,0,buf,sizeof(buf),0);
    return buf;
}

#define CHECK_HANDLE(expr) do{ \
    if(INVALID_HANDLE_VALUE==(expr)) { \
        log("Windows error: %s\n\tAt %s(%d): %s()\n",win32_get_last_error_string(),__FILE__,__LINE__,__FUNCTION__); \
        goto Error; \
    }}while(0)
#define CHECK_WIN32(expr) do{ \
    if(!(expr)) { \
        log("Windows error: %s\n\tAt %s(%d): %s()\n",win32_get_last_error_string(),__FILE__,__LINE__,__FUNCTION__); \
        goto Error; \
    }}while(0)


static void write_config_to_results_folder() {
    switch(Configuration.source_config_type) {
        case SourceConfigType_File: {
            char fname[1024]={0};
            const char *s=Configuration.source_config;
            if(!s) return;
            s+=strlen(s)-1; // start at the end
            while(*s!='/' && *s!='\\' && s>=Configuration.source_config)
                --s; // back up to a path seperator
            s++;     // move one past the path seperator
            sprintf_s(fname,sizeof(fname),"%s/%s",Configuration.output.folder,s);
            CHECK_WIN32(CopyFileA(Configuration.source_config,fname,FALSE));
        } break;
        case SourceConfigType_String: {
            char fname[1024]={0};
            HANDLE h;
            DWORD nwritten=0;
            sprintf_s(fname,sizeof(fname),"%s/%s",Configuration.output.folder,"config.lua");
            CHECK_HANDLE( h=CreateFile(fname,GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ,0,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,0) );
            CHECK_WIN32(WriteFile(h,Configuration.source_config,strlen(Configuration.source_config),&nwritten,0));
            CloseHandle(h);
        } break;
        default:;
    }
Error:;
}

static void medfilt_onerror(const char* file,int line,const char* function,const char* fmt,...){
    char buf[1024]={0};
    va_list ap;
    va_start(ap,fmt);
    vsnprintf(buf,sizeof(buf),fmt,ap);
    va_end(ap);
    log("MEDIAN FILTER: Error - %s\n",buf);
}

static struct vol* make_displacements(int ntimepoints) {
    struct vol *v;
    struct nd shape={0};
    shape.ndim=2;
    shape.type=nd_f32;
    shape.dims[0]=3;
    shape.dims[1]=ntimepoints;
    restride(&shape);
    CHECK(v=(struct vol*)malloc(sizeof(struct vol)+nbytes(shape)));
    v->shape=shape;
    return v;
Error:
    return 0;
}

static void write_mean_volume(const struct vol *v, const struct vol *dr, void *gpu_buf) {
    tic();

    struct Resampler* resampler=CreateTranslationInPlaceResampler(malloc,onerror);
    accumulator accumulator(v->shape,Configuration.parameters.window); // HAX: this is overkill, but it's convenient to reuse the code
    CHECK(dr->shape.type==nd_f32);

    // estimate mean and sigma for standardizing inputs
    for(int tm=0;tm<v->shape.dims[3];tm+=Configuration.input.chunk_size){
        const int nchunk=min(v->shape.dims[3]-tm,Configuration.input.chunk_size);

        struct nd chunk_shape=v->shape;
        chunk_shape.dims[3]=nchunk;
        restride(&chunk_shape);

        CHECK(move_to_gpu(gpu_buf,(void*)v->data,chunk_shape,tm,nchunk));

        for(unsigned i=0;i<nchunk;++i){ // for each timepoint...
            void *src_tm=offset(gpu_buf,chunk_shape,3,i);
            CHECK(ResampleInPlace(resampler,src_tm,chunk_shape,
                (float*)offset((void*)dr->data,dr->shape,1,tm+i)));
            accumulator.update_standardizer(src_tm,chunk_shape.type,0);
        }

    } // end loop over chunks
    accumulator.finalize_standardizer();
    {
        struct nd shape=v->shape;
        shape.type=nd_f32;
        shape.ndim=3;
        save_gpu(accumulator.mean(),shape,Configuration.output.mean_volume_filename);
    }
Error:
    CleanupTranslationInPlaceResampler(resampler);

    {
        double dt=toc(0);
        log("MEAN VOLUME: Elapsed %4.2f s\n"
            "\t%6.3g B/s\n"
            "\t%4.2f timepoints/s\n",
            dt,nbytes(v->shape)/dt,v->shape.dims[3]/dt);
    }
}

static void write_z_projection(const struct vol *v, const struct vol *dr, void *gpu_buf) {
    tic();

    struct Resampler* resampler=CreateTranslationInPlaceResampler(malloc,onerror);
    void * proj_gpu=0;
    struct nd dst_shape=get_output_shape__maximum(v->shape,2);
    CUDACHK(cudaMalloc(&proj_gpu,nbytes(dst_shape)));

    for(int tm=0;tm<v->shape.dims[3];tm+=Configuration.input.chunk_size) {
        const int nchunk=min(v->shape.dims[3]-tm,Configuration.input.chunk_size);

        struct nd chunk_shape=v->shape;
        chunk_shape.dims[3]=nchunk;
        restride(&chunk_shape);

        struct nd dst_chunk_shape=get_output_shape__maximum(chunk_shape,2);

        CHECK(move_to_gpu(gpu_buf,(void*)v->data,v->shape,tm,nchunk));
        for(unsigned i=0;i<nchunk;++i){ // for each timepoint...
            CHECK(ResampleInPlace(resampler,
                offset(gpu_buf,chunk_shape,3,i),
                chunk_shape,
                (float*)const_offset(dr->data,dr->shape,1,tm+i)));
        }        
        /*
            For the output projected shape, time is the 2nd dimension.
        */
        maximum(offset(proj_gpu,dst_shape,2,tm),dst_chunk_shape,gpu_buf,chunk_shape,2);
    }
    save_gpu(proj_gpu,dst_shape,"%s",Configuration.output.z_projection_filename);
    CUDACHK(cudaFree(proj_gpu));

Error:
    CleanupTranslationInPlaceResampler(resampler);

    {
        double dt=toc(0);
        log("Z PROJECTED TIMESERIES: Elapsed %4.2f s\n"
            "\t%6.3g B/s\n"
            "\t%4.2f timepoints/s\n",
            dt,nbytes(v->shape)/dt,v->shape.dims[3]/dt);
    }
}

int main(int argc,const char *argv[]) {
    struct vol *vol=0;
    struct vol *stimdt=0;
    struct vol *dr=0; // motion
    float *r2;
    struct dev { void *src,*mean,*stdev,*dst; } dev={0};
    TicTocTimer clock=tic();
    fp_log=0;
    if(!parseargs(argc,argv)) goto Error;
    maybe_create_output_folder();
    maybe_init_log();
    tiff_writer_set_reporter(log);

    cudaSetDevice(Configuration.parameters.gpu);
    {
        cudaDeviceProp prop;
        cudaGetDeviceProperties(&prop,Configuration.parameters.gpu);
        log("%s - %f GB Global Memory\n",prop.name,1e-9*prop.totalGlobalMem);
    }

    if(Configuration.options.extract_stim_times_from_images){
        CHECK(stimdt=extract_stim_dt(vol,Configuration.parameters.stimulus_threshold));
    } else {
        //CHECK(stimdt=load_stimdt_from_binary());
        CHECK(stimdt=load_stimdt_from_textfile());
    }
    maybe_write_stimdt(stimdt);

    {
        TicTocTimer t=tic();
        CHECK(vol=load(DATASET));
        double dt=toc(&t);
        log("LOAD: Elapsed %4.2f s\n"
                "\t%6.3g B/s\n"
                "\t%4.2f timepoints/s\n",
                dt,nbytes(vol->shape)/dt,vol->shape.dims[3]/dt);
    }

    //CHECK(dev.src=move_to_gpu(vol->data,vol->shape));
    dev.src=alloc_gpu(vol->shape,Configuration.input.chunk_size);
    //save_gpu(dev.src,vol->shape,"orig.tif");

    if(Configuration.options.disable_motion_correction) {
        // just use a displacement vector initialized to zero
        // Ideally, we'd skip resampling altogether, but this works too
        CHECK(dr=make_displacements(vol->shape.dims[3]));
        memset(dr->data,0,nbytes(dr->shape));
    } else {
        // estimation motion
        TicTocTimer t=tic();
        struct Motion *motion;
        {
            void *tgt=0;
            struct nd shape=vol->shape;
            shape.dims[3]=Configuration.input.chunk_size;

            shape.ndim=3;
            CHECK(tgt=alloc_gpu(shape,1));
            CHECK(move_to_gpu(tgt,vol->data,vol->shape,0,1));
            CHECK(motion=Motion_CreateContext(tgt,shape,malloc,onerror));
            CUDACHK(cudaFree(tgt));
        }
        CHECK(dr=make_displacements(vol->shape.dims[3]));

        for(int tm=0;tm<vol->shape.dims[3];tm+=Configuration.input.chunk_size){
            struct vol* dr_chunk=0;
            float tmp[3];
            const int nchunk=min(vol->shape.dims[3]-tm,Configuration.input.chunk_size);
            struct nd chunk_shape=vol->shape;
            chunk_shape.dims[3]=nchunk;
            restride(&chunk_shape);
            CHECK(move_to_gpu(dev.src,vol->data,vol->shape,tm,nchunk));
            CHECK(dr_chunk=Motion_Estimate(motion,dev.src,chunk_shape));

            memcpy(&tmp,offset(dr->data,dr->shape,1,tm),sizeof(tmp));
            memcpy(offset(dr->data,dr->shape,1,tm),dr_chunk->data,nbytes(dr_chunk->shape));
            free(dr_chunk);
        } // end loop over chunks

        Motion_CleanupContext(motion);
        free(motion);

        double dt=toc(&t);
        log("MOTION: Elapsed %4.2f s\n"
            "\t%6.3g B/s\n"
            "\t%4.2f timepoints/s\n",
            dt,nbytes(vol->shape)/dt,vol->shape.dims[3]/dt);
        if(Configuration.options.write_motion_vectors){
            // save final deltas
            char fname[1024]={0};
            sprintf_s(fname,sizeof(fname),
                        "%s/dr.%dx%d.f32",
                        Configuration.output.folder,
                        dr->shape.dims[0],
                        dr->shape.dims[1]);
            FILE *fp=fopen(fname,"wb");
            fwrite(dr->data,nbytes(dr->shape),1,fp);
            fclose(fp);
        }
    }

    if(Configuration.options.write_mean_volume)
        write_mean_volume(vol,dr,dev.src);
    if(Configuration.options.write_z_projection)
        write_z_projection(vol,dr,dev.src);

    // median filter
    if(Configuration.parameters.median_radius>0) {
        TicTocTimer t=tic();
        struct MedianFilter* ctx=MedianFilterGPU_NPP_Init(vol->shape.dims[0],vol->shape.dims[1],Configuration.parameters.median_radius,malloc,medfilt_onerror);
        const int stride=vol->shape.strides[2];
        for(int tm=0;tm<vol->shape.dims[3];tm+=Configuration.input.chunk_size) {
            const int nchunk=min(vol->shape.dims[3]-tm,Configuration.input.chunk_size);
            const int nplanes=vol->shape.dims[2]*nchunk;
            int16_t *data=(int16_t*)dev.src;
            CHECK(move_to_gpu(dev.src,vol->data,vol->shape,tm,nchunk));
            for(int i=0;i<nplanes;++i)
                MedianFilterGPU_NPP_Compute(ctx,data+i*stride,data+i*stride);
            CHECK(move_back_to_ram(vol->data,dev.src,vol->shape,tm,nchunk));
        }
        free(ctx);
        double dt=toc(&t);
        log("MEDIAN FILTER: Elapsed %4.2f s\n"
            "\t%6.3g B/s\n"
            "\t%4.2f timepoints/s\n",
            dt,nbytes(vol->shape)/dt,vol->shape.dims[3]/dt);

        if(Configuration.options.write_median)
            save_cpu(vol->data,vol->shape,"median_rad%d.tif",Configuration.parameters.median_radius);
    }

    {
        struct Resampler* resampler=CreateTranslationInPlaceResampler(malloc,onerror);
        accumulator accumulator(vol->shape,Configuration.parameters.window);
        CHECK(r2=alloc_correlation(&accumulator));
        CHECK(dr->shape.type==nd_f32);



        // estimate mean and sigma for standardizing inputs
        for(int tm=0;tm<vol->shape.dims[3];tm+=Configuration.input.chunk_size){
            const int nchunk=min(vol->shape.dims[3]-tm,Configuration.input.chunk_size);

            struct nd chunk_shape=vol->shape;
            chunk_shape.dims[3]=nchunk;
            restride(&chunk_shape);

            CHECK(move_to_gpu(dev.src,vol->data,chunk_shape,tm,nchunk));

            for(unsigned i=0;i<nchunk;++i){ // for each timepoint...
                void *src_tm=offset(dev.src,chunk_shape,3,i);
                CHECK(ResampleInPlace(resampler,src_tm,chunk_shape,(float*)offset(dr->data,dr->shape,1,tm+i)));
                accumulator.update_standardizer(src_tm,chunk_shape.type,0);
            }

        } // end loop over chunks
        accumulator.finalize_standardizer();

        // estimate kernels
        for(int tm=0;tm<vol->shape.dims[3];tm+=Configuration.input.chunk_size) {
            const int nchunk=min(vol->shape.dims[3]-tm,Configuration.input.chunk_size);

            struct nd chunk_shape=vol->shape;
            chunk_shape.dims[3]=nchunk;
            restride(&chunk_shape);

            CHECK(move_to_gpu(dev.src,vol->data,chunk_shape,tm,nchunk));
            for(unsigned i=0;i<nchunk;++i){ // for each timepoint...
                void *src_tm=offset(dev.src,chunk_shape,3,i);
                const int dt=vget_i32(stimdt,tm+i);
                CHECK(ResampleInPlace(resampler,src_tm,chunk_shape,(float*)offset(dr->data,dr->shape,1,tm+i)));
                accumulator.update_kernel(dt,src_tm,chunk_shape.type,0);
            }
        }
        if(Configuration.options.write_kernel_estimate){
            // save kernel estimate
            struct nd shape;
            shape.type=nd_f32;
            shape.ndim=4;
            memcpy(shape.dims,vol->shape.dims,sizeof(shape.dims[0])*3);
            shape.dims[3]=accumulator.window;
            restride(&shape);
            save_gpu(accumulator.K,shape,"K.tif");
        }

        // estimate error
        for(int tm=0;tm<vol->shape.dims[3];tm+=Configuration.input.chunk_size) {
            const int nchunk=min(vol->shape.dims[3]-tm,Configuration.input.chunk_size);

            struct nd chunk_shape=vol->shape;
            chunk_shape.dims[3]=nchunk;
            restride(&chunk_shape);

            CHECK(move_to_gpu(dev.src,vol->data,vol->shape,tm,nchunk));
            for(unsigned i=0;i<nchunk;++i){ // for each timepoint...
                void *src_tm=offset(dev.src,chunk_shape,3,i);
                const int dt=vget_i32(stimdt,tm+i);
                CHECK(ResampleInPlace(resampler,src_tm,chunk_shape,(float*)offset(dr->data,dr->shape,1,tm+i)));
                accumulator.update_error(dt,src_tm,chunk_shape.type,0);
            }
        }

        CleanupTranslationInPlaceResampler(resampler);
#if 0
        if(Configuration.options.write_accumulators){
            // save accumulators
            struct nd shape;
            shape.type=nd_f32;
            shape.ndim=3;
            memcpy(shape.dims,vol->shape.dims,sizeof(shape.dims[0])*3);
            restride(&shape);
            save_gpu(accumulator.y,shape ,"y.tif");
            save_gpu(accumulator.y2,shape,"y2.tif");
            save_gpu(accumulator.e,shape ,"e.tif");
            save_gpu(accumulator.e2,shape,"e2.tif");
        }
#endif
        accumulator.correlation(r2);
        write(Configuration.output.correlation_filename,subvol(vol,3),r2);
    }
    log("DONE: Elapsed %f s\n",toc(&clock));
    write_config_to_results_folder();
    return 0;
Error:
    write_config_to_results_folder();
    return 1;
}
