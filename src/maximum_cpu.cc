#include <nd.h>
#include <iostream>
#include <stdexcept>

#define CHECK(e) do{if(!(e)){throw std::runtime_error(#e);}}while(0)

static size_t prod(int n,const size_t *v) {
	size_t a=1;
	const auto *e=v+n;
    while(v<e) a*=*v++;
    return a;
}

/* Reduce the n-dimensional space to a 3d problem */
static nd problem(nd shape, unsigned idim) {
	nd out={0};
	out.ndim=3;
	out.dims[0]=prod(idim,shape.dims);
	out.dims[1]=shape.dims[idim];
	out.dims[2]=prod(shape.ndim-idim-1,&shape.dims[0]+idim+1);
	out.strides[0]=shape.strides[0];
	out.strides[1]=shape.strides[idim];
	out.strides[2]=shape.strides[idim+1];
	out.strides[3]=shape.strides[shape.ndim];
	out.type=shape.type;
	return out;
}

template<typename T>
static void maximum_(      void* dst_, const nd dst_shape,
					 const void* src_, const nd src_shape) {
	T *src=(T*)src_,
	  *dst=(T*)dst_;
	for(auto i=0;i<src_shape.dims[2];++i) {
		for(auto j=0;j<src_shape.dims[0];++j) {
			const T* v=src+j*src_shape.strides[0]+i*src_shape.strides[2];
			T mx=v[0];
			for(auto k=1;k<src_shape.dims[1];++k) {
				T e=v[k*src_shape.strides[1]];
				mx=mx<e?e:mx;
			}
			*dst=mx;
			dst+=dst_shape.strides[0];
		}
	}
}

extern "C" struct nd get_output_shape__maximum(struct nd src_shape,unsigned idim) {
	for(auto i=idim;i<src_shape.ndim;++i) {
		src_shape.dims[i]=src_shape.dims[i+1];		
	}	
	src_shape.ndim--;
	src_shape.strides[0]=1;
	for(unsigned i=1;i<=src_shape.ndim;++i)
		src_shape.strides[i]=src_shape.dims[i-1]*src_shape.strides[i-1];
	return src_shape;
}

static bool dst_shape_fits(const nd dst,const nd src_shape,int idim){
	nd sh=get_output_shape__maximum(src_shape,idim);
	return (dst.dims[0]<=sh.dims[0])
		&& (dst.dims[1]<=sh.dims[1])
		&& (dst.type==sh.type)
		&& (dst.ndim>=sh.ndim);
}

extern "C" int maximum(       void *dst,const struct nd dst_shape, 
						const void *src,const struct nd src_shape,
						unsigned idim) 
{
	try{
		CHECK(idim<src_shape.ndim);
		CHECK(dst_shape_fits(dst_shape,src_shape,idim));
		nd p=problem(src_shape,idim);
		switch(src_shape.type){
			case nd_i16: maximum_<int16_t >(dst,dst_shape,src,p); break;
			case nd_u16: maximum_<uint16_t>(dst,dst_shape,src,p); break;
			default:
				throw std::runtime_error("TODO");
		}
		return 1;
	} catch(std::runtime_error e) {
		std::cerr<<e.what()<<std::endl;
		return 0;
	}
}
