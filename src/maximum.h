#pragma once

#include "nd.h"

#ifdef __cplusplus
extern "C" {
#endif

struct nd get_output_shape__maximum(struct nd src_shape,unsigned idim);
int maximum(void *dst,const struct nd dst_shape,
            const void *src,const struct nd src_shape,
            unsigned idim);
int minimum(void *dst,const struct nd dst_shape,
            const void *src,const struct nd src_shape,
            unsigned idim);
int mean(void *dst,const struct nd dst_shape,
         const void *src,const struct nd src_shape,
         unsigned idim);
int stdev(void *dst,const struct nd dst_shape,
         const void *src,const struct nd src_shape,
         unsigned idim);
#ifdef __cplusplus
}
#endif
