#pragma once
#ifndef H_NGC_MOTION_V0
#define H_NGC_MOTION_V0

#include "nd.h"

#ifdef __cplusplus
extern "C" {
#endif

    struct Motion;
    struct Motion *Motion_CreateContext(void* target,
                                        struct nd shape,
                                        void* (*alloc)(size_t nbytes),
                                        void (*onerror)(const char* file, int line, const char* function, const char *fmt, ...));
    void  Motion_CleanupContext(struct Motion *ctx);
    struct vol *Motion_Estimate(struct Motion *ctx,void *source,struct nd shape);

#ifdef __cplusplus
}
#endif

#endif /* end of include guard: H_NGC_MOTION_V0 */
