#pragma once

class accumulator;

class accumulator {
public:
	float *N,*K,*r2;
	struct error_accumulator_t {
		float y,y2,e,e2;
	} *error_accumulators;
	float *standardizer;
    int16_t *h; // history buffer

    nd shape;
    int window;
    int count;
	int n_std;


    accumulator(const nd shape,int windowsize);
    ~accumulator();

	void update_standardizer(const void *data, nd_type type, int offset);
	void finalize_standardizer();
    

    void update_kernel(int dt,const void *data, nd_type type, int offset);
    void update_error (int dt,const void *data, nd_type type, int offset);
    void correlation(float *dst);

    size_t bytesof_correlation() const;

    void reset();

    // Copies the mean to a temporary gpu buffer
    // The standardizer is used to compute the mean so
    // it needs to have been updated and finalized for this to 
    // work properly.  Re-uses memory for r2.
    const float* mean(); 
};