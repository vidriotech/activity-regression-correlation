#include "median.h"
#include <string.h>

struct hist {
    short course[16];
    short fine[16][16];
};

struct MedianFilter {
    struct hist *hists,*kernel;
	int shape[2];
    int nthreads;
    void(*createthread)(void *thread,void(*f)(void*));
    void(*destroythread)(void *thread);
};

struct MedianFilter* MedianFilter_Init(
    int w, int h,
    int nthreads,
    void* (*alloc)(size_t nbytes),
    void  (*createthread)(void *thread,void(*f)(void*)),
    void  (*destroythread)(void *thread))
{
	unsigned char* buf=(unsigned char*)alloc(
        sizeof(struct MedianFilter)+
        (w+1)*sizeof(struct hist));
	struct MedianFilter* ctx=(struct MedianFilter*)buf;
	if(ctx){
		ctx->hists =(struct hist*)(buf+sizeof(struct MedianFilter));
		ctx->kernel=(struct hist*)(buf+sizeof(struct MedianFilter)+w*sizeof(struct hist));
		ctx->shape[0]=w;
		ctx->shape[1]=h;
        ctx->nthreads=nthreads;
        ctx->createthread=createthread;
        ctx->destroythread=destroythread;
	}
	return ctx;
}


static void hinc(struct hist* h, unsigned char v) {
    const unsigned char high=v>>4,
                         low=v&0xf;
    ++h->course[high];
    ++h->fine[high][low];
}

static void hdec(struct hist* h, unsigned char v) {
    const unsigned char high=v>>4,
        low=v&0xf;
    --h->course[high];
    --h->fine[high][low];
}

static void hadd(struct hist* __restrict a,const struct hist* __restrict b) {
    for(int c=0;c<16;++c) {
        short bc=b->course[c];
        if(bc){
            const short * const end=a->fine[c]+16;
            const short * __restrict bf=b->fine[c];
            a->course[c]+=bc;
            for(short * __restrict af=a->fine[c];af<end;++af,++bf)
                *af+=*bf;
        }
    }
}

static void hsub(struct hist* __restrict a,const struct hist* __restrict b) {
    for(int c=0;c<16;++c) {
        short bc=b->course[c];
        if(bc){
            const short * const end=a->fine[c]+16;
            const short * __restrict bf=b->fine[c];
            a->course[c]-=bc;
            for(short * __restrict af=a->fine[c];af<end;++af)
                *af-=*bf;
        }
    }
}

static int ordn(const struct hist* h, int n) {
    int f,c,acc=0;
    const short* hs=h->course;
    for(c=0;acc<=n&&c<16;++c,++hs)
        acc+=*hs;    
    acc-=hs[-1];
    hs=h->fine[--c];
    for(f=0;acc<=n&&f<16;++f,++hs)
        acc+=*hs;
    return (c<<4)|(f-1);
}

static void addrow(struct hist * __restrict hists,int nx, const unsigned char * __restrict v) {
    const unsigned char * const end=v+nx;
    for(;v<end;++v,++hists)
        hinc(hists,*v);
}

static void subrow(struct hist * __restrict hists,int nx, const unsigned char * __restrict v) {
    const unsigned char * const end=v+nx;
    for(;v<end;++v,++hists)
        hdec(hists,*v);
}

#if 0
static void hadd(int * __restrict a, const int* __restrict b) {
	const int* end=a+NHIST;
	for(;a<end;++a,++b)
		*a+=*b;	
}

static void hsub(int * __restrict a, const int* __restrict b) {
	const int* end=a+NHIST;
	for(;a<end;++a,++b)
		*a-=*b;
}

static int median(const int * a,int n) {
	int c,b;
	/* There's a choice of whether to use
	   c<n/2 or c<=n/2.  Matlab's median
	   appears to use <= so I'm sticking
	   with that here.
	*/
	for(c=0,b=0;c<=n/2;)
		c+=a[b++];
	return b-1;
}

static void addrow(int * __restrict hists,int nx, const unsigned char * __restrict v) {
	const unsigned char * const end=v+nx;
	for(;v<end;++v,hists+=NHIST){
		++hists[*v];
	}
}

static void subrow(int * __restrict hists,int nx, const unsigned char * __restrict v) {
	const unsigned char * const end=v+nx;
	for(;v<end;++v,hists+=NHIST){
		--hists[*v];
	}
}
              
#endif

void MedianFilter_Compute(struct MedianFilter *ctx,unsigned char* out,unsigned char* im, int r){
    const int 
        kh=2*r+1,            /* kernel height */
        nx=ctx->shape[0],
        ny=ctx->shape[1];
    struct hist 
        *hists=ctx->hists,
        *k=ctx->kernel;
    
    /* Initialize colulmn histograms
       Add the first r rows
    */
    memset(hists,0,nx*sizeof(*hists));
	{		
		for(int j=0;j<r && j<ny;++j)
			addrow(hists,nx,im+nx*j);
	}


    /* Filter image */
	{
		for(int j=0;j<ny;++j){
			int i,
				top=j-r-1,
				bot=j+r;

			/* update column histograms */
            if(bot<ny)
                addrow(hists,nx,im+nx*bot);
            else
                bot=ny-1;
				
            if(top>=0)
                subrow(hists,nx,im+nx*top);
            else
                top=-1;

			/* initialize the kernel - O(r) */
			memset(k,0,sizeof(*k));
			for(i=0;i<r && i<nx;++i)      /* add the right r column histograms */
				hadd(k,hists+i);

			/* update kernel histogram - scan columns */
			for(i=0;i<nx;++i){
				int
					left=i-r-1,
					right=i+r;
                if(right<nx)
                    hadd(k,hists+right);
                else
                    right=nx-1;
                if(left>=0)
                    hsub(k,hists+left);
                else
                    left=-1;
                            
                out[i+j*nx]=ordn(k,(right-left)*(bot-top)/2);
			}
		}
    }
}
