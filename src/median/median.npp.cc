#include <cuda_runtime.h>
#include <nppi.h>
#include <cstdint>

#define CUDACHK(e) do { cudaError_t cudachk_ecode=(e);if(cudaSuccess!=cudachk_ecode){ if(ctx->onerror) ctx->onerror(__FILE__,__LINE__,__FUNCTION__,"Error - CUDA:\n\t%s\n\t%s\n",#e,cudaGetErrorString(cudachk_ecode)); goto Error; }} while(0)
#define NPPCHK(e) do{ NppStatus npp_ecode=(e); if(NPP_SUCCESS!=npp_ecode){if(ctx->onerror) ctx->onerror(__FILE__,__LINE__,__FUNCTION__,"Error - NPP:\n\t%s\n\t%s\n",#e,NppStatusToString(npp_ecode)); goto Error; }}while(0)

static const char* NppStatusToString(NppStatus code);

struct MedianFilter {
	int w,h;	
	void(*onerror)(const char* file,int line,const char* function,const char* fmt,...);	

	// device pointers
	int16_t *out,*im; 
	Npp8u *scratch;

	// nppi stuff
	NppiSize sz;
	NppiSize k;
	NppiPoint anchor;
};

extern "C" struct MedianFilter* MedianFilterGPU_NPP_Init(int w,
										             int h,
													 int r,
										             void* (*alloc)(size_t nbytes),
										             void  (*onerror)(const char* file,int line,const char* function,const char* fmt,...)) 
{
	unsigned char* data=(unsigned char*)alloc(sizeof(struct MedianFilter));
	struct MedianFilter* ctx=(struct MedianFilter*)data;
	if(ctx) {
		ctx->w=w;
		ctx->h=h;
		ctx->onerror=onerror;
		CUDACHK(cudaMalloc(&ctx->out,w*h*sizeof(*ctx->out)));
		CUDACHK(cudaMalloc(&ctx->im,w*h*sizeof(*ctx->im)));		


		NppiSize sz{ctx->w,ctx->h};
		NppiSize k{2*r+1,2*r+1};
		NppiPoint anchor{r,r};
		Npp32u nbytes;
		NPPCHK(nppiFilterMedianGetBufferSize_16s_C1R(sz,k,&nbytes));
		CUDACHK(cudaMalloc(&ctx->scratch,nbytes));
		ctx->sz=sz;
		ctx->k=k;
		ctx->anchor=anchor;

	}
	return ctx;
Error:
	return 0;
}

extern "C" void MedianFilterGPU_NPP_Compute(struct MedianFilter *ctx,int16_t* out,int16_t* im) {	
	const size_t nbytes=ctx->w*ctx->h*sizeof(*out);
    const size_t stride=ctx->w*sizeof(*im);
#if 0 // im and out are in ram
	CUDACHK(cudaMemcpy(ctx->im,im,nbytes,cudaMemcpyHostToDevice));
	NPPCHK(nppiFilterMedian_8u_C1R(ctx->im,ctx->w,ctx->out,ctx->w,ctx->sz,ctx->k,ctx->anchor,ctx->scratch));
	CUDACHK(cudaMemcpy(out,ctx->out,nbytes,cudaMemcpyDeviceToHost));
#else // im and out are in gpu
    if(im==out) {  // pseudo in-place
        NPPCHK(nppiFilterMedian_16s_C1R(im,stride,ctx->out,stride,ctx->sz,ctx->k,ctx->anchor,ctx->scratch));
        CUDACHK(cudaMemcpy(out,ctx->out,nbytes,cudaMemcpyDeviceToDevice));
    } else { // out of place
        NPPCHK(nppiFilterMedian_16s_C1R(im,stride,out,stride,ctx->sz,ctx->k,ctx->anchor,ctx->scratch));
    }    
#endif
Error:;
}

static const char* NppStatusToString(NppStatus code) {
	switch(code) {
#define XXX(e) case e: return #e
		XXX(NPP_NOT_SUPPORTED_MODE_ERROR            );
		XXX(NPP_INVALID_HOST_POINTER_ERROR          );
		XXX(NPP_INVALID_DEVICE_POINTER_ERROR        );
		XXX(NPP_LUT_PALETTE_BITSIZE_ERROR           );
		XXX(NPP_ZC_MODE_NOT_SUPPORTED_ERROR         );
		XXX(NPP_NOT_SUFFICIENT_COMPUTE_CAPABILITY   );
		XXX(NPP_TEXTURE_BIND_ERROR                  );
		XXX(NPP_WRONG_INTERSECTION_ROI_ERROR        );
		XXX(NPP_HAAR_CLASSIFIER_PIXEL_MATCH_ERROR   );
		XXX(NPP_MEMFREE_ERROR                       );
		XXX(NPP_MEMSET_ERROR                        );
		XXX(NPP_MEMCPY_ERROR                        );
		XXX(NPP_ALIGNMENT_ERROR                     );
		XXX(NPP_CUDA_KERNEL_EXECUTION_ERROR         );
		XXX(NPP_ROUND_MODE_NOT_SUPPORTED_ERROR      );
		XXX(NPP_QUALITY_INDEX_ERROR                 );
		XXX(NPP_RESIZE_NO_OPERATION_ERROR           );
		XXX(NPP_OVERFLOW_ERROR                      );
		XXX(NPP_NOT_EVEN_STEP_ERROR                 );
		XXX(NPP_HISTOGRAM_NUMBER_OF_LEVELS_ERROR    );
		XXX(NPP_LUT_NUMBER_OF_LEVELS_ERROR          );
		XXX(NPP_CORRUPTED_DATA_ERROR                );
		XXX(NPP_CHANNEL_ORDER_ERROR                 );
		XXX(NPP_ZERO_MASK_VALUE_ERROR               );
		XXX(NPP_QUADRANGLE_ERROR                    );
		XXX(NPP_RECTANGLE_ERROR                     );
		XXX(NPP_COEFFICIENT_ERROR                   );
		XXX(NPP_NUMBER_OF_CHANNELS_ERROR            );
		XXX(NPP_COI_ERROR                           );
		XXX(NPP_DIVISOR_ERROR                       );
		XXX(NPP_CHANNEL_ERROR                       );
		XXX(NPP_STRIDE_ERROR                        );
		XXX(NPP_ANCHOR_ERROR                        );
		XXX(NPP_MASK_SIZE_ERROR                     );
		XXX(NPP_RESIZE_FACTOR_ERROR                 );
		XXX(NPP_INTERPOLATION_ERROR                 );
		XXX(NPP_MIRROR_FLIP_ERROR                   );
		XXX(NPP_MOMENT_00_ZERO_ERROR                );
		XXX(NPP_THRESHOLD_NEGATIVE_LEVEL_ERROR      );
		XXX(NPP_THRESHOLD_ERROR                     );
		XXX(NPP_CONTEXT_MATCH_ERROR                 );
		XXX(NPP_FFT_FLAG_ERROR                      );
		XXX(NPP_FFT_ORDER_ERROR                     );
		XXX(NPP_STEP_ERROR                          );
		XXX(NPP_SCALE_RANGE_ERROR                   );
		XXX(NPP_DATA_TYPE_ERROR                     );
		XXX(NPP_OUT_OFF_RANGE_ERROR                 );
		XXX(NPP_DIVIDE_BY_ZERO_ERROR                );
		XXX(NPP_MEMORY_ALLOCATION_ERR               );
		XXX(NPP_NULL_POINTER_ERROR                  );
		XXX(NPP_RANGE_ERROR                         );
		XXX(NPP_SIZE_ERROR                          );
		XXX(NPP_BAD_ARGUMENT_ERROR                  );
		XXX(NPP_NO_MEMORY_ERROR                     );
		XXX(NPP_NOT_IMPLEMENTED_ERROR               );
		XXX(NPP_ERROR                               );
		XXX(NPP_ERROR_RESERVED                      );
		XXX(NPP_SUCCESS);
		XXX(NPP_NO_OPERATION_WARNING                );
		XXX(NPP_DIVIDE_BY_ZERO_WARNING              );
		XXX(NPP_AFFINE_QUAD_INCORRECT_WARNING       );
		XXX(NPP_WRONG_INTERSECTION_ROI_WARNING      );
		XXX(NPP_WRONG_INTERSECTION_QUAD_WARNING     );
		XXX(NPP_DOUBLE_SIZE_WARNING                 );
		XXX(NPP_MISALIGNED_DST_ROI_WARNING          );

#undef XXX		
		default:
			return "Unknown";
	}
	
}