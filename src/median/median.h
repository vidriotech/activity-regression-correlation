#pragma once

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

struct MedianFilter;

struct MedianFilter* MedianFilter_Init(int w,int h,int nthreads,
                                       void* (*alloc)(size_t nbytes),
                                       void  (*createthread)(void *thread,void(*f)(void*)),
                                       void  (*destroythread)(void *thread));
void MedianFilter_Compute(struct MedianFilter *ctx,unsigned char* out,unsigned char* im, int radius);

struct MedianFilter* MedianFilterGPU_NPP_Init(int w,int h,
                                              int radius,
                                              void* (*alloc)(size_t nbytes),
                                              void  (*onerror)(const char* file,int line,const char* function,const char* fmt,...));
void MedianFilterGPU_NPP_Compute(struct MedianFilter *ctx,
                                 int16_t* out,
                                 int16_t* im);

#ifdef __cplusplus
}
#endif

/* NOTES

1.  Getting rid of the onerror() callback in MedianFilterGPU_Init:

    a. Make MedianFilterGPU_Compute require out and im are gpu pointers.
       Then Compute is just a kernel call and we can check that later with
       cudaDeviceSynchronize().

    Wouldn't work, some device memory needs to be allocated in Init and that could
    fail.

2. Allow for non-unit stride in x.
*/
