#pragma once

#ifndef __VIDRIO_RESAMPLER_H
#define __VIDRIO_RESAMPLER_H
#endif

#include "nd.h"

#ifdef __cplusplus
extern "C" {
#endif

struct Resampler;

struct Resampler *CreateTranslationInPlaceResampler(
    void* (*alloc)(size_t nbytes),
    void (*onerror)(const char *file, int line, const char *function, const char *fmt,...));

void CleanupTranslationInPlaceResampler(struct Resampler *resampler);

int ResampleInPlace(
    struct Resampler *resampler,
    void *src,struct nd src_shape,
    const float *coeffs);


#ifdef __cplusplus
} //extern "C" 
#endif