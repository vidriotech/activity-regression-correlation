#pragma once

#ifdef __cplusplus
extern "C"{
#endif

    enum SourceConfigType {
        SourceConfigType_File,
        SourceConfigType_String,
    };

    struct config{
        struct input{
            char timepoint_filename_pattern[1024];
            char stimulus_filename[1024];
            int first_timepoint;
            int last_timepoint;
            int chunk_size;
        } input;

        struct output{
            char folder[1024];
            char correlation_filename[1024];
            char log_filename[1024];
            char stimdt_filename[1024];
            char z_projection_filename[1024];
            char mean_volume_filename[1024];
        } output;

        struct parameters{
            int gpu;
            int window;
            int stimulus_threshold;
            int median_radius;
        } parameters;

        struct options{
            int extract_stim_times_from_images;
            int write_median;
            int write_stimdt;
            int write_motion_vectors;
            int write_mean_volume;
            int write_z_projection;
            int write_kernel_estimate;
            int disable_motion_correction;
            int use_single_file;
        } options;

        enum SourceConfigType source_config_type;
        const char* source_config;
        int is_valid;
    };

    extern struct config Configuration;

    int LoadConfiguration(const char* filename,int nargs, const char** args, void (*logger)(const char*fmt,...));
    int LoadConfigurationFromString(const char* str,void(*logger)(const char*fmt,...));
#ifdef __cplusplus
}
#endif
