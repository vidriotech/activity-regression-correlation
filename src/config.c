#include <lauxlib.h>
#include <lualib.h>
#include <string.h>
#include "config.h"

#define _STR(e) #e
#define STR(e) _STR(e)
#define CHECK(expr) do{int ecode=(expr);if(ecode) {puts(lua_tostring(L,-1)); goto Error;}} while(0)

//#define OK(expr) do{if(!(expr)) goto Error;}while(0)
#define OK(expr) do{if(!(expr)) Configuration.is_valid=0;}while(0)

struct config Configuration;

static const char* current_section_name;
static void(*Logger)(const char*fmt,...);

static const struct messages{
    const char
        *missing_section,
        *not_found,
        *not_string,
        *not_int,
        *not_bool,
        *missing_input_stimulus_file,
        *missing_first_or_last;
} messages = {
    "Configuration file:  Could not find required section: %s.\n",
    "Configuration file:  Not specified - %s.%s\n",
    "Configuration file:  %s.%s must be a string.\n",
    "Configuration file:  %s.%s must be an integer.\n",
    "Configuration file:  %s.%s must be true or false.\n",
    "Configuration file:  %s.%s must refer to a file providing stimulus times.\n",
};

#define get_string(L,K,S) get_string_(L,K,S,sizeof(*S))

static int get_string_(lua_State *L,const char* key,char* dst,size_t nbytes_dst)
{
    const char* str;
    if(lua_type(L,-1)!=LUA_TTABLE)
        return 0;
    lua_pushstring(L,key);
    if(lua_gettable(L,-2)==LUA_TNIL){
        Logger(messages.not_found,current_section_name,key);
        goto Error;
    }
    if(!lua_isstring(L,-1)){
        Logger(messages.not_string,current_section_name,key);
        goto Error;
    }
    str=lua_tostring(L,-1);
    strcpy_s(dst,nbytes_dst,str);
    lua_pop(L,1);
    return 1;
Error:
    lua_pop(L,1);
    return 0;
}

static int get_int(lua_State *L,const char* key,int* dst)
{
    if(lua_type(L,-1)!=LUA_TTABLE)
        return 0;
    lua_pushstring(L,key);
    if(lua_gettable(L,-2)==LUA_TNIL){
        Logger(messages.not_found,current_section_name,key);
        goto Error;
    }
    if(!lua_isinteger(L,-1)){
        Logger(messages.not_int,current_section_name,key);
        goto Error;
    }
    *dst=lua_tointeger(L,-1);
    lua_pop(L,1);
    return 1;
Error:
    lua_pop(L,1);
    return 0;
}

static int get_bool(lua_State *L,const char* key,int* dst)
{
    if(lua_type(L,-1)!=LUA_TTABLE)
        return 0;
    lua_pushstring(L,key);
    if(lua_gettable(L,-2)==LUA_TNIL){
        Logger(messages.not_found,current_section_name,key);
        goto Error;
    }
    if(!lua_isboolean(L,-1)){
        Logger(messages.not_bool,current_section_name,key);
        goto Error;
    }
    *dst=lua_toboolean(L,-1);
    lua_pop(L,1);
    return 1;
Error:
    lua_pop(L,1);
    return 0;
}

static int push_section(lua_State *L,const char* name)
{
    switch(lua_getglobal(L,name)) {
        case LUA_TTABLE: break;
        default:
            Logger(messages.missing_section,name);
            return 0;
    }
    current_section_name=name;
    return 1;
}

static int input_section(lua_State *L)
{
    OK(push_section(L,"input"));
    OK(get_string(L,"timepoint",&Configuration.input.timepoint_filename_pattern));
    if(!Configuration.options.extract_stim_times_from_images){
        OK(get_string(L,"stimulus",&Configuration.input.stimulus_filename));
        OK(strlen(Configuration.input.stimulus_filename)>0);
        if(!Configuration.is_valid)
            Logger(messages.missing_input_stimulus_file,"input","stimulus");
    }
    
    OK(get_int(L,"first",&Configuration.input.first_timepoint));
    OK(get_int(L,"last",&Configuration.input.last_timepoint));
    if(!get_int(L,"chunk_size",&Configuration.input.chunk_size)) {
        // not found, use default
        Configuration.input.chunk_size=Configuration.input.last_timepoint-Configuration.input.first_timepoint+1;
    }
    lua_pop(L,1);
    return 1;
Error:
    Configuration.is_valid=0;
    lua_pop(L,1);
    return 0;
}

static int output_section(lua_State *L) {
    OK(push_section(L,"output"));
    OK(get_string(L,"folder",&Configuration.output.folder));
    OK(get_string(L,"correlation",&Configuration.output.correlation_filename));
    OK(get_string(L,"log",&Configuration.output.log_filename));
    if(Configuration.options.write_stimdt)
        OK(get_string(L,"stimdt",&Configuration.output.stimdt_filename));
    if(get_string(L,"projection",&Configuration.output.z_projection_filename))
        Configuration.options.write_z_projection=1;
    if(get_string(L,"mean",&Configuration.output.mean_volume_filename))
        Configuration.options.write_mean_volume=1;


    /* strip path seperator from end of folder if present */
    {
        char *path=Configuration.output.folder;
        int n=strlen(path);
        char *r=path+n-1;
        while( (*r=='/'||*r=='\\') && r>path)
            --r;
        r[1]='\0';
        if(r==path)
            r[0]='.';
    }

    lua_pop(L,1);
    return 1;
Error:
    lua_pop(L,1);
    return 0;
}

static int parameters_section(lua_State *L) {
    OK(push_section(L,"parameters"));
    OK(get_int(L,"window",&Configuration.parameters.window));
    if(Configuration.options.extract_stim_times_from_images)
        OK(get_int(L,"stimulus_threshold",&Configuration.parameters.stimulus_threshold));
    if(!get_int(L,"median_radius",&Configuration.parameters.median_radius))
        Configuration.parameters.median_radius=0;
    if(!get_int(L,"gpuid",&Configuration.parameters.gpu))
        Configuration.parameters.gpu=0;
    lua_pop(L,1);
    return 1;
Error:
    lua_pop(L,1);
    return 0;
}

static int options_section(lua_State *L)
{
    push_section(L,"options");
    OK(get_bool(L,"extract_stim_times_from_images",&Configuration.options.extract_stim_times_from_images));
    if(!get_bool(L,"write_kernel_estimate",&Configuration.options.write_kernel_estimate))
        Configuration.options.write_kernel_estimate=0;
    OK(get_bool(L,"write_median",&Configuration.options.write_median));
    OK(get_bool(L,"write_motion_vectors",&Configuration.options.write_motion_vectors));
    OK(get_bool(L,"write_stimdt",&Configuration.options.write_stimdt));
    if(!get_bool(L,"disable_motion_correction",&Configuration.options.disable_motion_correction))
        Configuration.options.disable_motion_correction=0;
    if(!get_bool(L,"use_single_file",&Configuration.options.use_single_file))
        Configuration.options.use_single_file=0;
    lua_pop(L,1);
    return 1;
Error:
    lua_pop(L,1);
    return 0;
}

int LoadConfiguration(const char* filename,int nargs, const char** args, void (*logger)(const char*fmt,...)) {
    lua_State *L=luaL_newstate();
    luaL_openlibs(L);

    Logger=logger;
    Configuration.is_valid=1;
    Configuration.source_config_type=SourceConfigType_File;
    Configuration.source_config=filename;
    CHECK(luaL_loadfile(L,filename));
    for(int i=0;i<nargs;++i)
        lua_pushstring(L,args[i]);
    CHECK(lua_pcall(L,nargs,LUA_MULTRET,0));
    OK(options_section(L)); /* do options first because they enabled/disables requirements for other fields */
    OK(input_section(L));
    OK(output_section(L));
    OK(parameters_section(L));
    lua_close(L);
    return Configuration.is_valid;
Error:
    Configuration.is_valid=0;
    lua_close(L);
    return 0;
}

int LoadConfigurationFromString(const char* str,void (*logger)(const char*fmt,...)) {
    lua_State *L=luaL_newstate();
    luaL_openlibs(L);

    Logger=logger;
    Configuration.is_valid=1;
    Configuration.source_config_type=SourceConfigType_String;
    Configuration.source_config=str;
    CHECK(luaL_loadstring(L,str));
    CHECK(lua_pcall(L,0,LUA_MULTRET,0));
    OK(options_section(L)); /* do options first because they enabled/disables requirements for other fields */
    OK(input_section(L));
    OK(output_section(L));
    OK(parameters_section(L));
    lua_close(L);
    return Configuration.is_valid;
Error:
    Configuration.is_valid=0;
    lua_close(L);
    return 0;
}