#include "nd.h"
#include <iostream>
#include <stdexcept>
#include <host_defines.h>
#include <cuda_runtime.h>

#define CHECK(e) do{if(!(e)){throw std::runtime_error(#e);}}while(0)
#define CUDACHK(expr) do{ \
    cudaError_t eeeeeeee=(expr); \
    if(eeeeeeee!=cudaSuccess) \
        throw std::runtime_error(cudaGetErrorString(eeeeeeee)); \
}while(0)


static size_t prod(int n,const size_t *v) {
	size_t a=1;
	const auto *e=v+n;
	while(v<e) a*=*v++;
	return a;
}
//
//static size_t nbytes(const struct nd &shape) {
//	const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
//	return shape.strides[shape.ndim]*bpp[shape.type];
//}

/* Reduce the n-dimensional space to a 3d problem */
static nd problem(nd shape, unsigned idim) {
	nd out={0};
	out.ndim=3;
	out.dims[0]=prod(idim,shape.dims);
	out.dims[1]=shape.dims[idim];
	out.dims[2]=prod(shape.ndim-idim-1,&shape.dims[0]+idim+1);
	out.strides[0]=shape.strides[0];
	out.strides[1]=shape.strides[idim];
	out.strides[2]=shape.strides[idim+1];
	out.strides[3]=shape.strides[shape.ndim];
	out.type=shape.type;
	return out;
}

/// parallel max scan over a warp
template<typename T> // T needs to be 4 bytes
__inline__ __device__ T warp_max(T v) {
	for(int o=16;o>0;o>>=1) {
		auto t=__shfl_down(v,o);
		v=t>v?t:v;
	}
	return v;
}

/// parallel max scan over a block
template<typename T> // T needs to be 4 bytes
__inline__ __device__ T block_max(T v) {
	__shared__ T vs[32];
	int lane=threadIdx.y & 0x1f; // mod 32
	int warp=threadIdx.y>>5;
	v=warp_max(v);
	if(lane==0)
		vs[warp]=v;
	__syncthreads();
	// read from shared for valid threads
	// otherwise init with something that won't
	// effect the max
	v=(threadIdx.y<(blockDim.y>>5))? vs[lane] : (-FLT_MAX);
	if(warp==0)
		v=warp_max(v);
	return v;
}

template<typename T>
__global__ void max_try1_k(      T* dst_, const nd dst_shape, 
					  const T* src_, const nd src_shape) {
	float mx=(-FLT_MAX);

	const T* src=src_+blockIdx.x*src_shape.strides[0]+blockIdx.z*src_shape.strides[2];
	T* dst=dst_+(blockIdx.x+blockIdx.z*src_shape.dims[0])*dst_shape.strides[0];
	for(int i=blockIdx.y*blockDim.y+threadIdx.y;
		i<src_shape.dims[1];
		i+=blockDim.y*gridDim.y) {
		const float v=src[i*src_shape.strides[1]];
		mx=v>mx?v:mx;
	}
	mx=block_max(mx);
	if(threadIdx.y==0)		
		dst[blockIdx.y]=mx;

	// atomicMax(dst+blockIdx.y,mx);  
	// NOTE: would like to use atomicMax, which, I think, would allow for reductions > 1024 elems 
	// but requries a 4-byte dst pointer.
	// I could force the dst type or something, but it's not that important
}


/* 
	TODO: measure
		  Is this a retarded way of using parallelism since we're usually working with
		  higher dimensional arrays?

	TODO: watch for max block dim of 65k.
	      inner is fine, it's just outer that we have to
		  worry about.

	TODO: is there a way to get the max projection on each dimension at the same time?

	TODO: look into Interleaving Multiple Reductions to use instruction level parallelism 
		__inline__ __device__
			int3 warpReduceSumTriple(int3 val) {
			for (int offset = warpSize / 2; offset > 0; offset /= 2) {
				val.x += __shfl_down(val.x, offset);
				val.y += __shfl_down(val.y, offset);
				val.z += __shfl_down(val.z, offset);
			}
			return val; 
		}

*/


template<typename T>
static void maximum_try1_(void* dst_,const nd dst_shape,
						   const void* src_,const nd src_shape){
	T *src=(T*)src_,
	  *dst=(T*)dst_;
	CHECK(src_shape.dims[1]<1024);
	// block reduction
	dim3 threads{1,unsigned(src_shape.dims[1]),1};
	dim3 grid{
		unsigned(src_shape.dims[0]),
		1, //unsigned(src_shape.dims[1]+1023)/1024,
		unsigned(src_shape.dims[2])};
	max_try1_k<T><<<grid,threads>>>(dst,dst_shape,src,src_shape);
}

/* TRY 2 -- naiive */

template<typename Tdst,typename Tsrc>
__global__ void max_k(        Tdst* dst_, const nd dst_shape, 
						const Tsrc* src_, const nd src_shape) {
	int j0=threadIdx.y+blockIdx.y*blockDim.y;
	int i0=threadIdx.x+blockIdx.x*blockDim.x;
	for(int j=j0;j<src_shape.dims[2];j+=blockDim.y*gridDim.y){
		for(int i=i0;i<src_shape.dims[0];i+=blockDim.x*gridDim.x){
			const Tsrc* src=src_+i*src_shape.strides[0]+j*src_shape.strides[2];
			Tdst* dst=dst_+(i+j*src_shape.dims[0])*dst_shape.strides[0];
			float mx=float(*src);
			for(int k=1;k<src_shape.dims[1];++k) {
				float v=float(src[k*src_shape.strides[1]]);
				mx=(v<mx)?mx:v;
			}
			*dst=Tdst(mx);
		}
	}
}

template<typename Tdst,typename Tsrc>
static void maximum_(void* dst_,const nd dst_shape,
					 const void* src_,const nd src_shape){
	Tsrc *src=(Tsrc*)src_;
	Tdst *dst=(Tdst*)dst_;
	dim3 grid,threads;
	if(src_shape.dims[0]==1) {
		threads=dim3(1,1024,1);
		grid=dim3(1,unsigned(src_shape.dims[2]+1023)/1024,1);
	} else if(src_shape.dims[2]==1) {
		threads=dim3(1024,1,1);
		grid=dim3(unsigned(src_shape.dims[0]+1023)/1024,1,1);
	} else {
		threads=dim3(32,32,1);
		grid=dim3(unsigned(src_shape.dims[0]+31)/32,unsigned(src_shape.dims[2]+31)/32,1);
	}
	max_k<Tdst,Tsrc><<<grid,threads>>>(dst,dst_shape,src,src_shape);
}

extern "C" struct nd get_output_shape__maximum(struct nd src_shape,unsigned idim) {
	for(auto i=idim;i<src_shape.ndim;++i) {
		src_shape.dims[i]=src_shape.dims[i+1];		
	}	
	src_shape.ndim--;
	src_shape.strides[0]=1;
	for(unsigned i=1;i<=src_shape.ndim;++i)
		src_shape.strides[i]=src_shape.dims[i-1]*src_shape.strides[i-1];
	return src_shape;
}

static bool dst_shape_fits(const nd dst,const nd src_shape,int idim){
	nd sh=get_output_shape__maximum(src_shape,idim);
	return (dst.dims[0]<=sh.dims[0])
		&& (dst.dims[1]<=sh.dims[1])
		&& (dst.ndim>=sh.ndim);
}

template<class Tdst>
void maximum_tdst(      void *dst,const struct nd dst_shape, 
				  const void *src,const struct nd src_shape) {
	switch(src_shape.type){
		case nd_i16: maximum_<Tdst,int16_t >(dst,dst_shape,src,src_shape); break;
		case nd_u16: maximum_<Tdst,uint16_t>(dst,dst_shape,src,src_shape); break;
		default:
			throw std::runtime_error("TODO");
	}
}

extern "C" int maximum(      void *dst,const struct nd dst_shape, 
					   const void *src,const struct nd src_shape,
					   unsigned idim) 
{
	try{
		CHECK(idim<src_shape.ndim);
		CHECK(dst_shape_fits(dst_shape,src_shape,idim));
		nd p=problem(src_shape,idim);
		switch(dst_shape.type) {
			case nd_i16: maximum_tdst<int16_t >(dst,dst_shape,src,p); break;
			case nd_i32: maximum_tdst<int32_t >(dst,dst_shape,src,p); break;
			case nd_u16: maximum_tdst<uint16_t>(dst,dst_shape,src,p); break;
			case nd_u32: maximum_tdst<uint32_t>(dst,dst_shape,src,p); break;
			case nd_f32: maximum_tdst<float>(dst,dst_shape,src,p); break;
			default:
				throw std::runtime_error("TODO");
		}
		
		return 1;
	} catch(std::runtime_error e) {
		std::cerr<<e.what()<<std::endl;
		return 0;
	}
}

// MINIMUM

template<typename Tdst,typename Tsrc>
__global__ void min_k(Tdst* dst_,const nd dst_shape,
                      const Tsrc* src_,const nd src_shape) {
    int j0=threadIdx.y+blockIdx.y*blockDim.y;
    int i0=threadIdx.x+blockIdx.x*blockDim.x;
    for(int j=j0;j<src_shape.dims[2];j+=blockDim.y*gridDim.y) {
        for(int i=i0;i<src_shape.dims[0];i+=blockDim.x*gridDim.x) {
            const Tsrc* src=src_+i*src_shape.strides[0]+j*src_shape.strides[2];
            Tdst* dst=dst_+(i+j*src_shape.dims[0])*dst_shape.strides[0];
            float mn=float(*src);
            for(int k=1;k<src_shape.dims[1];++k) {
                float v=float(src[k*src_shape.strides[1]]);
                mn=(v>mn)?mn:v;
            }
            *dst=Tdst(mn);
        }
    }
}

template<typename Tdst,typename Tsrc>
static void minimum_(void* dst_,const nd dst_shape,
                     const void* src_,const nd src_shape) {
    Tsrc *src=(Tsrc*)src_;
    Tdst *dst=(Tdst*)dst_;
    dim3 grid,threads;
    if(src_shape.dims[0]==1) {
        threads=dim3(1,1024,1);
        grid=dim3(1,unsigned(src_shape.dims[2]+1023)/1024,1);
    } else if(src_shape.dims[2]==1) {
        threads=dim3(1024,1,1);
        grid=dim3(unsigned(src_shape.dims[0]+1023)/1024,1,1);
    } else {
        threads=dim3(32,32,1);
        grid=dim3(unsigned(src_shape.dims[0]+31)/32,unsigned(src_shape.dims[2]+31)/32,1);
    }
    min_k<Tdst,Tsrc><<<grid,threads>>>(dst,dst_shape,src,src_shape);
}

template<class Tdst>
void minimum_tdst(void *dst,const struct nd dst_shape,
                  const void *src,const struct nd src_shape) {
    switch(src_shape.type) {
        case nd_i16: minimum_<Tdst,int16_t >(dst,dst_shape,src,src_shape); break;
        case nd_u16: minimum_<Tdst,uint16_t>(dst,dst_shape,src,src_shape); break;
        default:
            throw std::runtime_error("TODO");
    }
}

extern "C" int minimum(void *dst,const struct nd dst_shape,
                       const void *src,const struct nd src_shape,
                       unsigned idim) {
    try {
        CHECK(idim<src_shape.ndim);
        CHECK(dst_shape_fits(dst_shape,src_shape,idim));
        nd p=problem(src_shape,idim);
        switch(dst_shape.type) {
            case nd_i16: minimum_tdst<int16_t >(dst,dst_shape,src,p); break;
            case nd_i32: minimum_tdst<int32_t >(dst,dst_shape,src,p); break;
            case nd_u16: minimum_tdst<uint16_t>(dst,dst_shape,src,p); break;
            case nd_u32: minimum_tdst<uint32_t>(dst,dst_shape,src,p); break;
            case nd_f32: minimum_tdst<float>(dst,dst_shape,src,p); break;
            default:
                throw std::runtime_error("TODO");
        }

        return 1;
    } catch(std::runtime_error e) {
        std::cerr<<e.what()<<std::endl;
        return 0;
    }
}

// MEAN

template<typename Tdst,typename Tsrc>
__global__ void mean_k(Tdst* dst_,const nd dst_shape,
                      const Tsrc* src_,const nd src_shape) {
    int j0=threadIdx.y+blockIdx.y*blockDim.y;
    int i0=threadIdx.x+blockIdx.x*blockDim.x;
    for(int j=j0;j<src_shape.dims[2];j+=blockDim.y*gridDim.y) {
        for(int i=i0;i<src_shape.dims[0];i+=blockDim.x*gridDim.x) {
            const Tsrc* src=src_+i*src_shape.strides[0]+j*src_shape.strides[2];
            Tdst* dst=dst_+(i+j*src_shape.dims[0])*dst_shape.strides[0];
            float acc=0.0f;
            for(int k=1;k<src_shape.dims[1];++k)
                acc+=float(src[k*src_shape.strides[1]]);
            *dst=Tdst(acc/src_shape.dims[1]);
        }
    }
}

template<typename Tdst,typename Tsrc>
static void mean_(void* dst_,const nd dst_shape,
                     const void* src_,const nd src_shape) {
    Tsrc *src=(Tsrc*)src_;
    Tdst *dst=(Tdst*)dst_;
    dim3 grid,threads;
    if(src_shape.dims[0]==1) {
        threads=dim3(1,1024,1);
        grid=dim3(1,unsigned(src_shape.dims[2]+1023)/1024,1);
    } else if(src_shape.dims[2]==1) {
        threads=dim3(1024,1,1);
        grid=dim3(unsigned(src_shape.dims[0]+1023)/1024,1,1);
    } else {
        threads=dim3(32,32,1);
        grid=dim3(unsigned(src_shape.dims[0]+31)/32,unsigned(src_shape.dims[2]+31)/32,1);
    }
    mean_k<Tdst,Tsrc><<<grid,threads>>>(dst,dst_shape,src,src_shape);
}

template<class Tdst>
void mean_tdst(void *dst,const struct nd dst_shape,
                  const void *src,const struct nd src_shape) {
    switch(src_shape.type) {
        case nd_i16: mean_<Tdst,int16_t >(dst,dst_shape,src,src_shape); break;
        case nd_u16: mean_<Tdst,uint16_t>(dst,dst_shape,src,src_shape); break;
        default:
            throw std::runtime_error("TODO");
    }
}

extern "C" int mean(void *dst,const struct nd dst_shape,
                    const void *src,const struct nd src_shape,
                    unsigned idim) {
    try {
        CHECK(idim<src_shape.ndim);
        CHECK(dst_shape_fits(dst_shape,src_shape,idim));
        nd p=problem(src_shape,idim);
        switch(dst_shape.type) {
            case nd_i16: mean_tdst<int16_t >(dst,dst_shape,src,p); break;
            case nd_i32: mean_tdst<int32_t >(dst,dst_shape,src,p); break;
            case nd_u16: mean_tdst<uint16_t>(dst,dst_shape,src,p); break;
            case nd_u32: mean_tdst<uint32_t>(dst,dst_shape,src,p); break;
            case nd_f32: mean_tdst<float>(dst,dst_shape,src,p); break;
            default:
                throw std::runtime_error("TODO");
        }

        return 1;
    } catch(std::runtime_error e) {
        std::cerr<<e.what()<<std::endl;
        return 0;
    }
}

// STDEV
template<typename Tdst,typename Tsrc>
__global__ void stdev_k(Tdst* dst_,const nd dst_shape,
                       const Tsrc* src_,const nd src_shape) {
    int j0=threadIdx.y+blockIdx.y*blockDim.y;
    int i0=threadIdx.x+blockIdx.x*blockDim.x;
    for(int j=j0;j<src_shape.dims[2];j+=blockDim.y*gridDim.y) {
        for(int i=i0;i<src_shape.dims[0];i+=blockDim.x*gridDim.x) {
            const Tsrc* src=src_+i*src_shape.strides[0]+j*src_shape.strides[2];
            Tdst* dst=dst_+(i+j*src_shape.dims[0])*dst_shape.strides[0];
            float acc=0.0f,acc2=0.0;
            for(int k=1;k<src_shape.dims[1];++k){
                float v(src[k*src_shape.strides[1]]);
                acc+=v;
                acc2+=v*v;
            }
            const float
                m=acc/src_shape.dims[1],
                m2=acc2/src_shape.dims[1],
                v=m2-m*m;

            *dst=(v>0)?Tdst(sqrtf(m2-m*m)):Tdst(1);
        }
    }
}

template<typename Tdst,typename Tsrc>
static void stdev_(void* dst_,const nd dst_shape,
                  const void* src_,const nd src_shape) {
    Tsrc *src=(Tsrc*)src_;
    Tdst *dst=(Tdst*)dst_;
    dim3 grid,threads;
    if(src_shape.dims[0]==1) {
        threads=dim3(1,1024,1);
        grid=dim3(1,unsigned(src_shape.dims[2]+1023)/1024,1);
    } else if(src_shape.dims[2]==1) {
        threads=dim3(1024,1,1);
        grid=dim3(unsigned(src_shape.dims[0]+1023)/1024,1,1);
    } else {
        threads=dim3(32,32,1);
        grid=dim3(unsigned(src_shape.dims[0]+31)/32,unsigned(src_shape.dims[2]+31)/32,1);
    }
    stdev_k<Tdst,Tsrc><<<grid,threads>>>(dst,dst_shape,src,src_shape);
}

template<class Tdst>
void stdev_tdst(void *dst,const struct nd dst_shape,
               const void *src,const struct nd src_shape) {
    switch(src_shape.type) {
        case nd_i16: stdev_<Tdst,int16_t >(dst,dst_shape,src,src_shape); break;
        case nd_u16: stdev_<Tdst,uint16_t>(dst,dst_shape,src,src_shape); break;
        default:
            throw std::runtime_error("TODO");
    }
}

extern "C" int stdev(void *dst,const struct nd dst_shape,
                    const void *src,const struct nd src_shape,
                    unsigned idim) {
    try {
        CHECK(idim<src_shape.ndim);
        CHECK(dst_shape_fits(dst_shape,src_shape,idim));
        nd p=problem(src_shape,idim);
        switch(dst_shape.type) {
            case nd_i16: stdev_tdst<int16_t >(dst,dst_shape,src,p); break;
            case nd_i32: stdev_tdst<int32_t >(dst,dst_shape,src,p); break;
            case nd_u16: stdev_tdst<uint16_t>(dst,dst_shape,src,p); break;
            case nd_u32: stdev_tdst<uint32_t>(dst,dst_shape,src,p); break;
            case nd_f32: stdev_tdst<float>(dst,dst_shape,src,p); break;
            default:
                throw std::runtime_error("TODO");
        }

        return 1;
    } catch(std::runtime_error e) {
        std::cerr<<e.what()<<std::endl;
        return 0;
    }
}