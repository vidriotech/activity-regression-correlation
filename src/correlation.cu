#include <vector>
#include <stdexcept>
#include <cuda_runtime_api.h>
#include "nd.h"
#include "vops.h"
#include "correlation.h"
#include <sstream>
#include <iostream>
#include "maximum.h"

using namespace std;

#define countof(e) (sizeof(e)/sizeof(*(e)))

static size_t nbytes(const struct nd shape) {
	const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
	return shape.strides[shape.ndim]*bpp[shape.type];
}

#define LOG(str) throw runtime_error(str);
#define CHECK(e) do{if(!((e))) {LOG(#e);}}while(0)
#define SICHECK(...) __VA_ARGS__; do{if(h.log) {LOG(h.log);}}while(0)

#define CUDACHECK(expr) do{ \
    cudaError_t eeeeeeee=(expr); \
    if(eeeeeeee!=cudaSuccess) \
        throw runtime_error(cudaGetErrorString(eeeeeeee)); \
}while(0)

accumulator::accumulator(const nd shape,int windowsize) 
    : shape(shape)
    , window(windowsize)
    , count(0)
{
    const size_t nelem=shape.strides[3];    
    CUDACHECK(cudaMalloc((void**)&error_accumulators ,nelem*sizeof(error_accumulator_t)));
    CUDACHECK(cudaMalloc((void**)&K ,nelem*window*sizeof(float)));
	CUDACHECK(cudaMalloc((void**)&r2 ,nelem*sizeof(float)));
    CHECK(N=(float*)malloc(window*sizeof(float)));

    {	// standardization
		CUDACHECK(cudaMalloc((void**)&standardizer,2*nelem*sizeof(float)));
    }
    reset();
}

accumulator::~accumulator() {
    CUDACHECK(cudaFree((void*)error_accumulators ));
    free(N);
    CUDACHECK(cudaFree((void*)K ));
	CUDACHECK(cudaFree((void*)r2 ));
	CUDACHECK(cudaFree((void*)standardizer ));
}

void accumulator::reset() {
    const size_t nelem=shape.strides[3];
    CUDACHECK(cudaMemset(error_accumulators ,0,nelem*sizeof(error_accumulator_t)));
    memset(N,0,window*sizeof(float));
    CUDACHECK(cudaMemset(K ,0,nelem*window*sizeof(float)));
    count=0;

	{	// standardization
		CUDACHECK(cudaMemset(standardizer,0,2*nelem*sizeof(float)));
		n_std=0;
	}
}

template<class T>
__global__
void accumulate_standardizer_k(
		int n,
		float2 * __restrict__ std,
		const T * __restrict__ data)
{
	for(int k=blockIdx.x*blockDim.x+threadIdx.x;
		k<n;
		k+=gridDim.x*blockDim.x) {
		const float v=data[k];
		std[k].x+=v;
		std[k].y+=v*v;
	}	
}

void accumulator::update_standardizer(const void *data, nd_type type,int offset) {
	const auto nelem=shape.strides[3]; // just update one timepoint at a time.  assumes data point to the timepoint
	const size_t 
		od=(offset>0)?offset:0,
		os=(offset<0)?(-offset):0;
	offset=(offset<0)?(-offset):offset;
	const unsigned grid=(nelem-offset+1023)/1024;
	switch(type) {

#define CASE(tid,T) case tid: \
		accumulate_standardizer_k<T><<<grid,1024>>>( \
			nelem-offset, \
		    (float2*)(standardizer+od*2), \
			((T*)data)+os); break;

		CASE(nd_u8,uint8_t);
		CASE(nd_u16,uint16_t);
		CASE(nd_u32,uint32_t);
		CASE(nd_u64,uint64_t);
		CASE(nd_i8 ,int8_t);
		CASE(nd_i16,int16_t);
		CASE(nd_i32,int32_t);
		CASE(nd_i64,int64_t);
		CASE(nd_f32,float);
		CASE(nd_f64,double);
		default:;
#undef CASE
	}
	
	++n_std;
}

__global__
void finalize_standardizer_k(
	int n,
	float * __restrict__ std,
	int n_std)
{
	for(int k=blockIdx.x*blockDim.x+threadIdx.x;
		k<n;
		k+=gridDim.x*blockDim.x) {		
		std[k]/=n_std;
	}	
}

void accumulator::finalize_standardizer() {
	const auto nelem=shape.strides[3]*2;
	const unsigned grid=(nelem+1023)/1024;
	finalize_standardizer_k<<<grid,1024>>>(nelem,standardizer,n_std);	
}

__global__ 
void _mean_cpy_k(
    int n,
    float * __restrict__ out,
    const float * __restrict__ std
){
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
        k<n;
        k+=gridDim.x*blockDim.x){
        out[k]=std[2*k];
    }
}

const float* accumulator::mean() {
    /* Just need to copy down the data */
    const auto nelem=shape.strides[3];
    const unsigned grid=(nelem+1023)/1024;
    _mean_cpy_k<<<grid,1024>>>(nelem,r2,standardizer);
    return r2;
}

template<class T>
__global__
void update_kernel_k(
	int n,
	float* __restrict__ K,
	const T* __restrict__ in,
	const float2 * __restrict__ std) 
{
	for(int k=blockIdx.x*blockDim.x+threadIdx.x;
		k<n;
		k+=gridDim.x*blockDim.x) {		
		K[k]+=(float(in[k])-std[k].x)/std[k].y; // (in-mean)/sigma
	}	
}

void accumulator::update_kernel(int dt,const void *data,nd_type type,int offset) {
    const auto nelem=shape.strides[3];
    const size_t 
        od=(offset>0)?offset:0,
        os=(offset<0)?(-offset):0;
	offset=(offset<0)?(-offset):offset;
	const unsigned grid=(nelem-offset+1023)/1024;

    if(0<=dt && dt<window) {
        N[dt]++;                                                   // number of samples at bin dt
																   // sum of observations at dt        
		switch(type) {
#define CASE(tid,T) case tid: update_kernel_k<T><<<grid,1024>>>(nelem-offset,K+dt*nelem+od,((T*)data)+os,(float2*)(standardizer+2*os)); break;

			CASE(nd_u8,uint8_t);
			CASE(nd_u16,uint16_t);
			CASE(nd_u32,uint32_t);
			CASE(nd_u64,uint64_t);
			CASE(nd_i8 ,int8_t);
			CASE(nd_i16,int16_t);
			CASE(nd_i32,int32_t);
			CASE(nd_i64,int64_t);
			CASE(nd_f32,float);
			CASE(nd_f64,double);
			default:;
#undef CASE
		}
        
    }
}

template<class T>
__global__ 
void update_error_k(
	int n,
	accumulator::error_accumulator_t * __restrict__ error_accumulators,
	const float * __restrict__ K, // null if not in window
	float norm,
	const T * __restrict__ data,
	const float2* std) {
	for(int k=blockIdx.x*blockDim.x+threadIdx.x;
		k<n;
		k+=gridDim.x*blockDim.x) {		

		const float v=(data[k]-std[k].x)/std[k].y;		
		error_accumulators[k].y +=v;    // shouldn't this always be zero mean?
		error_accumulators[k].y2+=v*v;  // shouldn't this always be unit variance?

		// prediction error
		// 1. e = prediction;
		// 2. e -= observed; <-- result is predicted-observed
		const float e=K?((K[k]*norm)-v):(-v);
		error_accumulators[k].e +=std[k].x; // e isn't really used so I repurpose it 
		error_accumulators[k].e2+=e*e;
	}		
}

void accumulator::update_error(int dt, const void* data, nd_type type,int offset) {
    const auto nelem=shape.strides[3];
    const size_t
        od=(offset>0)?offset:0,
        os=(offset<0)?(-offset):0;
	offset=(offset<0)?(-offset):offset;
	const unsigned grid=(nelem-offset+1023)/1024;
	float *K_dt=0;
	float norm=1.0f;

		
	if(0<=dt && dt<window){
		K_dt=K+dt*nelem;
		norm=1.0f/N[dt];
	}
		
	switch(type) {
#define CASE(tid,T) case tid: update_error_k<T><<<grid,1024>>>(nelem-offset,error_accumulators,K_dt,norm,((T*)data)+os,(float2*)(standardizer+2*od)); break
		CASE(nd_u8,uint8_t);
		CASE(nd_u16,uint16_t);
		CASE(nd_u32,uint32_t);
		CASE(nd_u64,uint64_t);
		CASE(nd_i8 ,int8_t);
		CASE(nd_i16,int16_t);
		CASE(nd_i32,int32_t);
		CASE(nd_i64,int64_t);
		CASE(nd_f32,float);
		CASE(nd_f64,double);
		default:;
#undef CASE
	}
    count++;
}



__global__ 
void correlation_k(
    int n,
    float * __restrict__ dst,
	const accumulator::error_accumulator_t * __restrict__ es,
    float  count)
{
    for(int k=blockIdx.x*blockDim.x+threadIdx.x;
        k<n;
        k+=gridDim.x*blockDim.x)
    {
		const float
			y=es[k].y,
			y2=es[k].y2,
			e2=es[k].e2;
        const float sst=y2-y*y/count; // ??  if mean y is 0 and variance is 1, what should this be
		dst[k]=1.0f-e2/sst;
		//dst[k]=es[k].e;
    }
}

size_t accumulator::bytesof_correlation() const {
    return shape.strides[3]*sizeof(float);
}

void accumulator::correlation(float *dst) {
    auto n=shape.strides[3];
    correlation_k<<<(n+1023)/1024,1024>>>(n,r2,error_accumulators,count); // reusing e as output
    CUDACHECK( cudaPeekAtLastError() );
    CUDACHECK( cudaDeviceSynchronize() );
    CUDACHECK(cudaMemcpy(dst,r2,shape.strides[3]*sizeof(float),cudaMemcpyDeviceToHost));
}

/* NOTES
 * 
 *  - Could reuse K for r2 instead of allocating a whole r2 buffer
 *  
 * FIXME
 *  
 *  - accumulating y and y2 is identical to the standardizer accumulators I think?
 *		Should elimate/reuse
 */