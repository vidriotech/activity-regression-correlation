// Tiff Output

// I'm going to assemble the file in a resizable buffer and then output that to a file.

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#ifdef _MSC_VER
#include <windows.h>
#endif

static void(*g_reporter)(const char* fmt,...)=nullptr;

struct buf_t {
    char *data;
    size_t end,cap;
};

static int resize(struct buf_t * b, size_t request) {
    b->cap=request;
    b->data=(char*)realloc(b->data,b->cap);
    return b->data!=NULL;
}

static int bufwrite(struct buf_t * __restrict b,size_t offset,size_t nbytes, const void* __restrict data) {
    size_t last=offset+nbytes;
    if(last>b->cap)
        if(!resize(b,last))
            return 0;
    b->end=(last>b->end)?last:b->end;
    memcpy(b->data+offset,data,nbytes);
    return 1;
}

static size_t prod(int n,const size_t *v) {
	size_t a=*v++;
	const auto *e=v+n-1;
    while(v<e) a*=*v++;
    return a;
}

/// Keep interesting tiff tags here.
enum TiffTag {
	TIFFTAG_NEWSUBFILETYPE            =254,
    TIFFTAG_IMAGEWIDTH                =256,   ///< The number of columns in the image, i.e., the number of pixels per scanline.
    TIFFTAG_IMAGELENGTH               =257,   ///< The number of rows (sometimes described as scanlines) in the image
    TIFFTAG_BITSPERSAMPLE             =258,
    TIFFTAG_COMPRESSION               =259,
    TIFFTAG_PHOTOMETRICINTERPRETATION =262,
    TIFFTAG_STRIPOFFSETS              =273,
	TIFFTAG_ORIENTATION               =274,
	TIFFTAG_SAMPLESPERPIXEL           =277,
    TIFFTAG_ROWSPERSTRIP              =278,
    TIFFTAG_STRIPBYTECOUNTS           =279,
    TIFFTAG_XRESOLUTION               =282,
    TIFFTAG_YRESOLUTION               =283,
    TIFFTAG_RESOLUTIONUNIT            =296,
    TIFFTAG_SAMPLEFORMAT              =339,   ///< This field specifies how to interpret each data sample in a pixel. See enum TiffSampleFormat
};
// NTAGS -- MUST be equal to the number of tags used for output
#define NTAGS (12) //(15) -- not outputing resolution tags for the time being....

enum TiffSubFileTypeFlag
{
	TiffSubFileTypeFlag_ReducedResolution=0x1,
	TiffSubFileTypeFlag_MultiPage        =0x2,
	TiffSubFileTypeFlag_TransparencyMask =0x4,
};

enum TiffType {
    TIFFTYPE_BYTE=1,
    TIFFTYPE_ASCII,
    TIFFTYPE_SHORT,
    TIFFTYPE_LONG,
    TIFFTYPE_RATIONAL,
    TIFFTYPE_SBYTE,
    TIFFTYPE_UNDEFINED,
    TIFFTYPE_SSHORT,
    TIFFTYPE_SLONG,
    TIFFTYPE_SRATIONAL,
    TIFFTYPE_FLOAT,
    TIFFTYPE_DOUBLE,
    TIFFTYPE_LONG8=16,
    TIFFTYPE_SLONG8,
    TIFFTYPE_IFD8,
};

enum TiffSampleFormat {
    TIFFSAMPLEFORMAT_UNSIGNED=1,
    TIFFSAMPLEFORMAT_SIGNED,
    TIFFSAMPLEFORMAT_FLOAT,
    TIFFSAMPLEFORMAT_UNDEFINED,
};

#pragma pack(push,1)
struct liltiff_header {
	uint16_t fmt;
	uint16_t ver;
	uint32_t first_ifd;
};
struct liltiff_tag {
	uint16_t tag,type;
	uint32_t count,value;
};
struct liltiff_ifd {
	uint16_t ntags;
	struct liltiff_tag tags[NTAGS];
	uint32_t next;
};

struct bigtiff_header {
    uint16_t fmt;
    uint16_t ver;
    uint16_t sizeof_offset;
    uint16_t zero;
    uint64_t first_ifd;
};
struct bigtiff_tag {
    uint16_t tag,type;
    uint64_t count,value;
};
struct bigtiff_ifd {
    uint64_t ntags;
    struct bigtiff_tag tags[NTAGS];
    uint64_t next;
};
#pragma pop

template<typename Ttag>
static Ttag subfiletype() {
	return Ttag{TIFFTAG_NEWSUBFILETYPE,TIFFTYPE_LONG,1,TiffSubFileTypeFlag_MultiPage};
}

template<typename Ttag>
static Ttag imagewidth(size_t w) {
    return Ttag{TIFFTAG_IMAGEWIDTH,TIFFTYPE_LONG,1,w};   
}

template<typename Ttag>
static Ttag imagelength(size_t h) {
    return Ttag{TIFFTAG_IMAGELENGTH,TIFFTYPE_LONG,1,h};    
}

template<typename Ttag>
static Ttag bitspersample(unsigned b) {
    return Ttag{TIFFTAG_BITSPERSAMPLE,TIFFTYPE_SHORT,1,b};    
}

template<typename Ttag>
static Ttag uncompressed() {
	return Ttag{TIFFTAG_COMPRESSION,TIFFTYPE_SHORT,1,1};
}

template<typename Ttag>
static Ttag photometricinterpretation(unsigned v) {
    return Ttag{TIFFTAG_PHOTOMETRICINTERPRETATION,TIFFTYPE_SHORT,1,1};    
}

template<typename Ttag> static Ttag stripoffsets(size_t v);
template<> static liltiff_tag stripoffsets(size_t v) {
	return liltiff_tag{TIFFTAG_STRIPOFFSETS,TIFFTYPE_LONG,1,v};	
}
template<> static bigtiff_tag stripoffsets(size_t v) {
	return bigtiff_tag{TIFFTAG_STRIPOFFSETS,TIFFTYPE_LONG8,1,v};
}

template<typename Ttag>
static Ttag orientation() {
	return Ttag{TIFFTAG_ORIENTATION,TIFFTYPE_SHORT,1,1}; // top-left origin
}

template<typename Ttag>
static Ttag samplesperpixel(unsigned v) {
	return Ttag{TIFFTAG_SAMPLESPERPIXEL,TIFFTYPE_SHORT,1,v};
}

template<typename Ttag>
static Ttag rowsperstrip(size_t v) {
    return Ttag{TIFFTAG_ROWSPERSTRIP,TIFFTYPE_LONG,1,v};
}

template<typename Ttag> static Ttag stripbytecounts(size_t v);
template<> static liltiff_tag stripbytecounts(size_t v) {
	return liltiff_tag{TIFFTAG_STRIPBYTECOUNTS,TIFFTYPE_LONG,1,v};	
}
template<> static bigtiff_tag stripbytecounts(size_t v) {
	return bigtiff_tag{TIFFTAG_STRIPBYTECOUNTS,TIFFTYPE_LONG8,1,v};
}

template<typename Ttag>
static Ttag xresolution(size_t v) {
    struct rational{ uint32_t num,den; } rat={v,1};
    return Ttag{TIFFTAG_XRESOLUTION,TIFFTYPE_RATIONAL,1,*(uint64_t*)&rat};    
}

template<typename Ttag>
static Ttag yresolution(size_t v) {
    struct rational{ uint32_t num,den; } rat={v,1};
    return Ttag{TIFFTAG_YRESOLUTION,TIFFTYPE_RATIONAL,1,*(uint64_t*)&rat};    
}

template<typename Ttag>
static Ttag resolutionunit_inch() {
    return Ttag{TIFFTAG_RESOLUTIONUNIT,TIFFTYPE_SHORT,1,2};    
}

template<typename Ttag>
static Ttag sampleformat(TiffSampleFormat sample_format) {
    return Ttag{TIFFTAG_SAMPLEFORMAT,TIFFTYPE_SHORT,1,sample_format};    
}

#define CHK(e) do{if(!(e)) {if(g_reporter) g_reporter(#e "\n");	goto Error;}}while(0)

template<typename Theader> static Theader header(){ return Theader{}; }
template<> static bigtiff_header header(){ return bigtiff_header{0x4949,0x002B,8,0,sizeof(bigtiff_header)}; }
template<> static liltiff_header header(){ return liltiff_header{0x4949,0x002A,sizeof(liltiff_header)};     }

template<typename Tifd, typename Theader, typename Ttag>
static void write_greyscale(const char* filename,size_t ndim,const size_t* shape,const void *data, size_t Bpp,TiffSampleFormat sample_format) {
	const size_t 
		w=shape[0],
		h=shape[1],
		nifd=(ndim==2)?1:prod(ndim-2,shape+2),
		nbytes=prod(ndim,shape)*Bpp,
		bytesof_ifds=nifd*sizeof(Tifd);
	struct buf_t buf={0};
	CHK(resize(&buf,nbytes+bytesof_ifds+sizeof(Theader)));

	//write_header
	{
		const Theader hdr=header<Theader>();
		CHK(bufwrite(&buf,0,sizeof(hdr),&hdr));
	}
	// write_ifds
	{
		size_t i;
		const size_t start=sizeof(Theader);
		const size_t end=start+bytesof_ifds;
		for(i=0;i<nifd;++i) {
			Tifd ifd{NTAGS,{0},start+(i+1)*sizeof(ifd)};
			int j=0;
			ifd.tags[j++]=subfiletype<Ttag>();
			ifd.tags[j++]=imagewidth<Ttag>(w);
			ifd.tags[j++]=imagelength<Ttag>(h);
			ifd.tags[j++]=bitspersample<Ttag>(Bpp*8);
			ifd.tags[j++]=uncompressed<Ttag>();
			ifd.tags[j++]=samplesperpixel<Ttag>(1);
			ifd.tags[j++]=photometricinterpretation<Ttag>(0);
			ifd.tags[j++]=stripoffsets<Ttag>(end+i*w*h*Bpp);
			ifd.tags[j++]=orientation<Ttag>();
			ifd.tags[j++]=rowsperstrip<Ttag>(h);
			ifd.tags[j++]=stripbytecounts<Ttag>(w*h*Bpp);
#if 0
			// FIXME: handle storage for rationals when lil'tiff.
			//        For big tiff these work just fine.
			//        Not sure how necessary they are.
			ifd.tags[j++]=xresolution<Ttag>(72);
			ifd.tags[j++]=yresolution<Ttag>(72);
			ifd.tags[j++]=resolutionunit_inch<Ttag>();
#endif
			ifd.tags[j++]=sampleformat<Ttag>(sample_format);
			CHK(j==NTAGS);
			if(i+1==nifd)
				ifd.next=0;
			bufwrite(&buf,start+i*sizeof(ifd),sizeof(ifd),&ifd);
		}
		// write the data
		bufwrite(&buf,end,nbytes,data);
	}
#ifndef _MSC_VER
	{
		FILE *fp=fopen(filename,"wb");
		CHK(fp);
		fwrite(buf.data,1,buf.end,fp);
		fclose(fp);
	}
#else
    {
        int64_t remaining=buf.end;
        size_t total_written=0;        
        HANDLE h=CreateFile(filename,GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,0,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,0);
        while(remaining>0) {
            DWORD written;
            if(!WriteFile(h,buf.data+total_written,min(1LL<<20,remaining),&written,0))
                break;
            total_written+=written;
            remaining-=written;
        }        
        CloseHandle(h);
    }
#endif
	free(buf.data);
	return;
Error:
	if(g_reporter) g_reporter("Tiff writer: Couldn't write.  Something went wrong.\n");	
}

void write_tiff_greyscale(const char* filename,size_t ndim,const size_t* shape,const void *data, size_t Bpp,TiffSampleFormat sample_format) {
	const auto nbytes=prod(ndim,shape)*Bpp;
	if(!(nbytes>>30))
		write_greyscale<liltiff_ifd,liltiff_header,liltiff_tag>(filename,ndim,shape,data,Bpp,sample_format);
	else
		write_greyscale<bigtiff_ifd,bigtiff_header,bigtiff_tag>(filename,ndim,shape,data,Bpp,sample_format);
}

extern "C" {
    void tiff_writer_set_reporter(void (*reporter)(const char* fmt,...)) {
        g_reporter=reporter;
    }

	void write_tiff_u8(const char* filename,size_t ndim,const size_t* shape,const uint8_t *data){
		write_tiff_greyscale(filename,ndim,shape,data,sizeof(*data),TIFFSAMPLEFORMAT_UNSIGNED);
	}
	void write_tiff_i16(const char* filename,size_t ndim,const size_t* shape,const int16_t *data){
		write_tiff_greyscale(filename,ndim,shape,data,sizeof(*data),TIFFSAMPLEFORMAT_SIGNED);
	}
	void write_tiff_u16(const char* filename,size_t ndim,const size_t* shape,const uint16_t *data){
		write_tiff_greyscale(filename,ndim,shape,data,sizeof(*data),TIFFSAMPLEFORMAT_UNSIGNED);
	}
	void write_tiff_f32(const char* filename,size_t ndim,const size_t* shape,const float *data){
		write_tiff_greyscale(filename,ndim,shape,data,sizeof(*data),TIFFSAMPLEFORMAT_FLOAT);
	}
}

/*
	TODO: strided write of data buffer
*/